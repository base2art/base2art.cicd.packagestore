namespace Base2art.CiCd.PackageStore.Web.UI.Public.Resources
{
    using System;
    using System.Net.Mail;

    public class Space
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public PersonAudit Owner { get; set; }
        // public string ReadGroupName { get; set; }
        // public string WriteGroupName { get; set; }
        public Channel[] Channels { get; set; }

        public string[] ManagementAddresses { get; set; }
        public string[] ManagementDomains { get; set; }
        public string[] PushAddresses { get; set; }
        public string[] PushDomains { get; set; }

        // public string Host { get; set; }
        // public ChannelStyle ChannelStyle { get; set; }
    }
}