namespace Base2art.CiCd.PackageStore.Web.Public.Resources
{
    using System.Collections.Generic;

    public class SearchResults
    {
        public int totalHits { get; set; }
        public List<Dictionary<string, object>> data { get; set; }
    }
}