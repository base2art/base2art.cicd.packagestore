namespace Base2art.CiCd.PackageStore.Web.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Data;

    public class PackageService
    {
        private readonly IRequireAuthProvider authentication;

        private readonly IUrlProvider urlProvider;
        private readonly PackageRepo packageRepo;

        public PackageService(
            IUrlProvider urlProvider,
            IRequireAuthProvider authentication,
            PackageRepo packageRepo)
        {
            this.urlProvider = urlProvider;
            this.authentication = authentication;
            this.packageRepo = packageRepo;
        }

        public async Task<Dictionary<string, object>> Index(Guid spaceId, Guid channelId, Guid readPassword)
        {
            // await this.authentication.RequireAction(FileAccess.ReadWrite, spaceId, channelId, readPassword);
            // await this.repo.Setup(spaceId, channelId);
            var url = "COMMENT PLACEHOLDER";
            var resources = new List<Dictionary<string, string>>();


            var canRead = await this.authentication.Api().CanPerformForAccessLevel(FileAccess.Read, spaceId, channelId, readPassword);
            var canWrite = await this.authentication.Api().CanPerformForAccessLevel(FileAccess.Write, spaceId, channelId, readPassword);

            if (canRead || canWrite)
            {
                await this.packageRepo.TriggerScanForChanges(spaceId, channelId);
            }
            
            if (canRead)
            {
                resources.Add(new Dictionary<string, string>
                              {
                                  {"@id", $"{this.urlProvider.GetBaseUrl(spaceId, channelId, readPassword)}/query"},
                                  {"@type", "SearchQueryService"},
                                  {"comment", url}
                              });
                
                resources.Add(new Dictionary<string, string>
                              {
                                  {"@id", $"{this.urlProvider.GetBaseUrl(spaceId, channelId, readPassword)}/query"},
                                  {"@type", "SearchQueryService/3.0.0-rc"},
                                  {"comment", url}
                              });
                
                resources.Add(new Dictionary<string, string>
                              {
                                  {"@id", $"{this.urlProvider.GetBaseUrl(spaceId, channelId, readPassword)}/query"},
                                  {"@type", "SearchQueryService/3.5.0"},
                                  {"comment", url}
                              });

                resources.Add(new Dictionary<string, string>
                              {
                                  {"@id", this.urlProvider.RegistrationBase(spaceId, channelId, readPassword)},
                                  {"@type", "RegistrationsBaseUrl"},
                                  {"comment", url}
                              });
                
                resources.Add(new Dictionary<string, string>
                              {
                                  {"@id", this.urlProvider.RegistrationBase(spaceId, channelId, readPassword)},
                                  {"@type", "RegistrationsBaseUrl/3.0.0-rc"},
                                  {"comment", url}
                              });
                
                resources.Add(new Dictionary<string, string>
                              {
                                  {"@id", this.urlProvider.RegistrationBase(spaceId, channelId, readPassword)},
                                  {"@type", "RegistrationsBaseUrl/3.4.0"},
                                  {"comment", url}
                              });
                
                resources.Add(new Dictionary<string, string>
                              {
                                  {"@id", this.urlProvider.RegistrationBase(spaceId, channelId, readPassword)},
                                  {"@type", "RegistrationsBaseUrl/3.6.0"},
                                  {"comment", url}
                              });
                
                resources.Add(new Dictionary<string, string>
                              {
                                  {"@id", $"{this.urlProvider.GetBaseUrl(spaceId, channelId, readPassword)}/autocomplete"},
                                  {"@type", "SearchAutocompleteService"},
                                  {"comment", url}
                              });
                
                resources.Add(new Dictionary<string, string>
                              {
                                  {"@id", $"{this.urlProvider.GetBaseUrl(spaceId, channelId, readPassword)}/autocomplete"},
                                  {"@type", "SearchAutocompleteService/3.0.0-rc"},
                                  {"comment", url}
                              });
                
                resources.Add(new Dictionary<string, string>
                              {
                                  {"@id", $"{this.urlProvider.GetBaseUrl(spaceId, channelId, readPassword)}/autocomplete"},
                                  {"@type", "SearchAutocompleteService/3.0.0-beta"},
                                  {"comment", url}
                              });
                
                resources.Add(new Dictionary<string, string>
                              {
                                  {"@id", $"{this.urlProvider.GetBaseUrl(spaceId, channelId, readPassword)}/autocomplete"},
                                  {"@type", "SearchAutocompleteService/3.5.0"},
                                  {"comment", url}
                              });

                resources.Add(new Dictionary<string, string>
                              {
                                  {"@id", $"{this.urlProvider.GetBaseUrl(spaceId, channelId, readPassword)}/flatcontainer/"},
                                  {"@type", "PackageBaseAddress/3.0.0"},
                                  {"comment", url}
                              });
            }

            if (canWrite)
            {
                resources.Add(new Dictionary<string, string>
                              {
                                  {"@id", $"{this.urlProvider.GetBaseUrl(spaceId, channelId, readPassword)}/package"},
                                  {"@type", "PackagePublish/2.0.0"},
                                  {"comment", url}
                              });
            }

            var context = new Dictionary<string, string>
                          {
                              {"@vocab", "http://schema.nuget.org/services#"},
                              {"comment", "http://www.w3.org/2000/01/rdf-schema#comment"}
                          };

            var dict = new Dictionary<string, object>();
            dict["version"] = "3.0.0";
            dict["resources"] = resources;
            dict["@context"] = context;

            return dict;
        }
    }
}