namespace Base2art.CiCd.PackageStore.Web.UI.Public.Resources
{
    using System;

    public class AdminConfiguration
    {
        public string[] ManagementAddresses { get; set; }
        public string[] ManagementDomains { get; set; }
        public Guid Id { get; set; }
    }
}