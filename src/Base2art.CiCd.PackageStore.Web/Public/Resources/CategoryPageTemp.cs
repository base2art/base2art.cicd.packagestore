namespace Base2art.CiCd.PackageStore.Web.Public.Resources
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Endpoints;

    public class CategoryPageTemp : Dictionary<string, object>
    {
        public CategoryPageTemp(
            IUrlProvider urlProvider,
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            CategoryItem[] items)
        {
            var categoryItem = items.Last();
            var lower = categoryItem.catalogEntry.version;
            var upper = items.First().catalogEntry.version;

            this.Add("@id", urlProvider.RegistrationPage(spaceId, channelId, readPassword, categoryItem.catalogEntry.id, lower, upper));
            this.Add("@type", "catalog:CatalogPage");

            this.Add("commitId", categoryItem.commitId);
            this.Add("commitTimeStamp", categoryItem.commitTimeStamp);
            this.Add("count", items.Length);
            this.Add("lower", lower);
            this.Add("upper", upper);
        }
    }
}