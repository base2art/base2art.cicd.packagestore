namespace Base2art.CiCd.PackageStore.Web.Data
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Threading.Tasks;
    using Models;
    using Models.FileSystem;
    using Newtonsoft.Json;

    public class ChannelRepo
    {
        private readonly IDirectoryProvider directoryProvider;
        private readonly JsonSerializerSettings settings;

        public ChannelRepo(IDirectoryProvider directoryProvider)
        {
            this.directoryProvider = directoryProvider;
            this.settings = new JsonSerializerSettings {Formatting = Formatting.Indented};
            this.settings.Converters.Add(new MailAddressConvertor());
        }

        // public async Task

        public async Task<ISpace> AddSpace(
            string name,
            Guid ownerId,
            MailAddress[] managementAddresses,
            string[] managementDomains,
            MailAddress[] pushAddresses,
            string[] pushDomains)
        {
            var spaceDirRoot = this.directoryProvider.GetSpacesDir();

            var id = Guid.NewGuid();
            var indexDir = Directory.CreateDirectory(Path.Combine(spaceDirRoot.FullName, this.directoryProvider.ToName(id)));

            var indexPage = Path.Combine(indexDir.FullName, "index.json");

            var space = new Space
                        {
                            Id = id,
                            ManagementAddresses = managementAddresses ?? new MailAddress[0],
                            ManagementDomains = managementDomains ?? new string[0],
                            PushAddresses = pushAddresses ?? new MailAddress[0],
                            PushDomains = pushDomains ?? new string[0],
                            Name = name,
                            OwnerId = ownerId
                        };

            File.WriteAllText(indexPage, JsonConvert.SerializeObject(space, this.settings));

            return space;
        }

        public async Task<ISpace> UpdateSpace(
            Guid spaceId,
            string name,
            Guid ownerId,
            MailAddress[] managementAddresses,
            string[] managementDomains,
            MailAddress[] pushAddresses,
            string[] pushDomains)
        {
            var spaceDirRoot = this.directoryProvider.GetSpacesDir();

            var indexDir = Directory.CreateDirectory(Path.Combine(spaceDirRoot.FullName, this.directoryProvider.ToName(spaceId)));

            var indexPage = Path.Combine(indexDir.FullName, "index.json");

            var space = new Space
                        {
                            Id = spaceId,
                            ManagementAddresses = managementAddresses.Where(x => x != null).ToArray(),
                            ManagementDomains = managementDomains,
                            PushAddresses = pushAddresses.Where(x => x != null).ToArray(),
                            PushDomains = pushDomains,
                            Name = name,
                            OwnerId = ownerId
                        };

            File.WriteAllText(indexPage, JsonConvert.SerializeObject(space, this.settings));

            return space;
        }

        public async Task<ISpace[]> GetSpaces()
        {
            var spaceDirRoot = this.directoryProvider.GetSpacesDir();

            List<ISpace> spaces = new List<ISpace>();

            foreach (var spaceDir in spaceDirRoot.GetDirectories())
            {
                var indexPage = Path.Combine(spaceDir.FullName, "index.json");
                if (File.Exists(indexPage))
                {
                    var indexContent = File.ReadAllText(indexPage);
                    spaces.Add(JsonConvert.DeserializeObject<Space>(indexContent, this.settings));
                }
            }

            return spaces.ToArray();
        }

        public async Task<ISpace[]> GetSpacesByHost(MailAddress host)
        {
            var spaces = await this.GetSpaces();
            return spaces.Where(x => RequireAuthProviders.IsAllowed(x.ManagementAddresses, x.ManagementDomains, host)
                                     || RequireAuthProviders.IsAllowed(x.PushAddresses, x.PushDomains, host)).ToArray();
        }

        public async Task<IChannel> AddChannel(Guid spaceId, string name)
        {
            var channelsDir = this.directoryProvider.GetChannelsDir(spaceId);

            var id = Guid.NewGuid();

            var channelDir = Directory.CreateDirectory(Path.Combine(channelsDir.FullName, this.directoryProvider.ToName(id)));

            var indexPage = Path.Combine(channelDir.FullName, "index.json");
            if (File.Exists(indexPage))
            {
                throw new InvalidOperationException("Duplicate Id");
            }

            var channel = new Channel()
                          {
                              Id = id,
                              Name = name,
                              SpaceId = spaceId
                          };

            File.WriteAllText(indexPage, JsonConvert.SerializeObject(channel, this.settings));
            return channel;
        }

        public async Task<IChannel> UpdateChannel(Guid spaceId, Guid channelId, string channelName)
        {
            var channelDir = this.directoryProvider.GetPackagesDir(spaceId, channelId);

            var indexPage = Path.Combine(channelDir.FullName, "index.json");
            if (!File.Exists(indexPage))
            {
                throw new InvalidOperationException("Unknown Id");
            }

            var channel = new Channel()
                          {
                              Id = channelId,
                              Name = channelName,
                              SpaceId = spaceId
                          };

            File.WriteAllText(indexPage, JsonConvert.SerializeObject(channel, this.settings));
            return channel;
        }

        public async Task<IReadOnlyDictionary<Guid, IChannel[]>> GetChannelsBySpaces(Guid[] spaceIds)
        {
            var items = new Dictionary<Guid, IChannel[]>();
            foreach (var spaceId in spaceIds)
            {
                items[spaceId] = this.GetChannelsBySpaceInternal(spaceId).ToArray();
            }

            return items;
        }

        public async Task<IChannel[]> GetChannelsBySpace(Guid spaceId)
        {
            return (this.GetChannelsBySpaceInternal(spaceId)).ToArray();
        }

        public async Task<IChannel> GetChannelBySpace(Guid spaceId, Guid channelId)
        {
            var channelsDir = this.directoryProvider.GetChannelsDir(spaceId);
            var channelDir = Directory.CreateDirectory(Path.Combine(channelsDir.FullName, this.directoryProvider.ToName(channelId)));

            var indexPage = Path.Combine(channelDir.FullName, "index.json");

            var content = File.ReadAllText(indexPage);
            var obj = JsonConvert.DeserializeObject<Channel>(content, this.settings);
            return obj;
        }

        private IEnumerable<IChannel> GetChannelsBySpaceInternal(Guid spaceId)
        {
            var dir = this.directoryProvider.GetChannelsDir(spaceId);
            var dirs = dir.GetDirectories()
                          .Select(x => Path.Combine(x.FullName, "index.json"))
                          .Where(x => File.Exists(x));

            foreach (var file in dirs)
            {
                var content = File.ReadAllText(file);
                var obj = JsonConvert.DeserializeObject<Channel>(content, this.settings);
                yield return obj;
            }
        }

        public async Task<ISpace> GetSpaceById(Guid objectId)
        {
            var spaceDirRoot = this.directoryProvider.GetSpacesDir();
            var indexDir = Directory.CreateDirectory(Path.Combine(spaceDirRoot.FullName, this.directoryProvider.ToName(objectId)));

            var indexPage = Path.Combine(indexDir.FullName, "index.json");

            if (!File.Exists(indexPage))
            {
                return null;
            }

            var content = File.ReadAllText(indexPage);
            return JsonConvert.DeserializeObject<Space>(content, this.settings);
        }

        public async Task<IAdminConfiguration> GetAdminInformation()
        {
            var spaceDirRoot = this.directoryProvider.GetSpacesDir();
            var fullPath = Path.Combine(spaceDirRoot.FullName, "admin-config.json");
            if (!File.Exists(fullPath))
            {
                return new MyAdminConfiguration();
            }

            var content = File.ReadAllText(fullPath);
            return JsonConvert.DeserializeObject<MyAdminConfiguration>(content, this.settings);
        }

        public async Task SetAdminInformation(MailAddress[] managementAddresses, string[] managementDomains)
        {
            var spaceDirRoot = this.directoryProvider.GetSpacesDir();
            var fullPath = Path.Combine(spaceDirRoot.FullName, "admin-config.json");
            var configObj = new MyAdminConfiguration
                            {
                                Id = new Guid("72794301-724A-47C7-AA20-E0342D37D74D"),
                                ManagementAddresses = managementAddresses ?? new MailAddress[0],
                                ManagementDomains = managementDomains ?? new string[0]
                            };
            File.WriteAllText(fullPath, JsonConvert.SerializeObject(configObj, this.settings));
        }

        private class MyAdminConfiguration : IAdminConfiguration
        {
            public Guid Id { get; set; } = new Guid("72794301-724A-47C7-AA20-E0342D37D74D");
            public MailAddress[] ManagementAddresses { get; set; } = new MailAddress[0];
            public string[] ManagementDomains { get; set; } = new string[0];
        }
    }
}

//     IChannel[] Channels(Guid spaceId) => new[]
//                                          {
//                                              new Channel
//                                              {
//                                                  Id = Guid.NewGuid(),
//                                                  Name = "Dev",
//                                                  SpaceId = spaceId
//                                              },
//                                              new Channel
//                                              {
//                                                  Id = Guid.NewGuid(),
//                                                  Name = "Test",
//                                                  SpaceId = spaceId
//                                              },
//                                              new Channel
//                                              {
//                                                  Id = Guid.NewGuid(),
//                                                  Name = "Prod",
//                                                  SpaceId = spaceId
//                                              }
//                                          };
//
//     return spaces.SelectMany(x => Channels(x.Id)).ToArray();
// }
// var spaces = new ISpace[]
//              {
//                  new Space
//                  {
//                      Id = Guid.Parse("F886148F-F483-429E-83C8-C4FF01773720"),
//                      Name = "default",
//                      Owner = "(none)",
//                      ReadGroupName = "",
//                      WriteGroupName = "",
//                  },
//                  new Space
//                  {
//                      Id = Guid.Parse("001698D5-527A-40C0-9079-D86A7C012564"),
//                      Name = "base2art-public",
//                      Owner = "(none)",
//                      ReadGroupName = "",
//                      WriteGroupName = "",
//                  },
//                  new Space
//                  {
//                      Id = Guid.Parse("E6E61A27-EE0B-42EA-87A7-C10930DEBF9A"),
//                      Name = "base2art-private",
//                      Owner = "(none)",
//                      ReadGroupName = "",
//                      WriteGroupName = "",
//                  }
//              };
// return spaces;