namespace Base2art.CiCd.PackageStore.Web.UI.Converters
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Data;
    using Data.Models;
    using Public.Resources;

    public class KeyConverter
    {
        private readonly IPersonLookup lookup;

        public KeyConverter(IPersonLookup lookup)
        {
            this.lookup = lookup;
        }

        public async Task<Key[]> ConvertToModel(Task<IKey[]> keys, bool includePassword)
        {
            var keyValues = await keys;
            return keyValues.Select(x => this.ConvertToModel(x, includePassword))
                            .OrderByDescending(x=>x.Expires).ToArray();
        }

        // public Key[] ConvertToModel(IKey[] keys, bool includePassword)
        // {
        //     var keyValues = keys;
        //     return keyValues.Select(x => this.ConvertToModel(x, includePassword)).ToArray();
        // }

        public Key ConvertToModel(IKey key, bool includePassword)
        {
            var x = new Key
                    {
                        Id = key.Id,
                        Name = key.Name,
                        Password = includePassword ? key.Password : Guid.Empty,
                        CanDelete = key.CanDelete,
                        CanRead = key.CanRead,
                        CanWrite = key.CanWrite,
                        Expires = key.Expires,
                    };

            if (x.Expires < DateTime.UtcNow)
            {
                x.CanDelete = false;
                x.CanRead = false;
                x.CanWrite = false;
            }

            return x;
        }
    }
}