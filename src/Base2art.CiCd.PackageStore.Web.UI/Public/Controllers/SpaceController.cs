namespace Base2art.CiCd.PackageStore.Web.UI.Public.Controllers
{
    using System;
    using System.Linq;
    using System.Net.Mail;
    using System.Reflection;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Flows;
    using Base2art.Web.Http;
    using Base2art.Web.ObjectQualityManagement;
    using Base2art.Web.Pages;
    using Converters;
    using Data;
    using Data.Models;
    using Pages;
    using Pages.Spaces;
    using Resources;
    using Threading.Tasks;
    using Validation;
    using ViewModels;
    using ViewModels.Spaces;

    public class SpaceController
    {
        private readonly ChannelRepo spaces;
        private readonly IRequireAuthProvider auth;
        private readonly RestListOperationFactory listApi;
        private readonly RestCreateOperationFactory createApi;
        private readonly RestEditOperationFactory editApi;
        private readonly SpaceConverter spaceConverter;

        public SpaceController(MembershipRepo users, ChannelRepo spaces, IRequireAuthProvider auth, ILayoutProvider layout)
        {
            this.spaces = spaces;
            this.auth = auth;
            var userLookup = new AuthenticatedUserLookup(users);
            this.listApi = new RestListOperationFactory(layout, userLookup);
            this.createApi = new RestCreateOperationFactory(layout, userLookup);
            this.editApi = new RestEditOperationFactory(layout, userLookup);
            this.spaceConverter = new SpaceConverter(new PersonLookup(users.GetUser));
        }

        public async Task<WebPage> List(ClaimsPrincipal principal, IHttpRequest request)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionListSpaces(principal));
            return await this.listApi.Create()
                             .WithRequest(request)
                             .WithUser(principal)
                             .Build(
                                    () => new Index(),
                                    async delegate(IAuthenticatedUser user)
                                    {
                                        var spacesLocal = this.spaces.GetSpacesByHost(user.Email);
                                        var spaceValues = await spacesLocal;

                                        var channelsBySpaces = this.spaces.GetChannelsBySpaces(spaceValues.Select(x => x.Id).ToArray());
                                        return new IndexViewModel
                                               {
                                                   Spaces = await this.spaceConverter.ConvertToModel(spacesLocal, channelsBySpaces),
                                                   UserAddress = user.Email
                                               };
                                    });
        }

        public async Task<WebPage> GetAddItem(ClaimsPrincipal principal, IHttpRequest request)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionCreateSpace(principal));
            return await this.createApi.Create<ISpace>()
                             .WithRequest(request)
                             .WithUser(principal)
                             .Build(() => new Edit(), async (user, objectId, errors) => new EditViewModel
                                                                                        {
                                                                                            Data = null,
                                                                                            Errors = errors,
                                                                                            OperationType = OperationType.Creating,
                                                                                            DataValidation = new SpaceValidator(false)
                                                                                        });
        }

        public async Task<WebPage> AddItem(
            ClaimsPrincipal principal,
            IHttpRequest request,
            Space space)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionCreateSpace(principal));
            return await this.createApi.Create<ISpace>()
                             .WithRequest(request)
                             .WithValidation(() => this.ValidateItem(space))
                             .WithPersistence(x => x.Id, async (user) =>
                             {
                                 var spaceId = await this.spaces.AddSpace(
                                                                          space.Name,
                                                                          user.UserId,
                                                                          space.ManagementAddresses?.Select(x => TryGetMailAddress(x))?.ToArray(),
                                                                          space.ManagementDomains,
                                                                          space.PushAddresses?.Select(x => TryGetMailAddress(x)).ToArray(),
                                                                          space.PushDomains);

                                 await this.spaces.AddChannel(spaceId.Id, "default");

                                 return spaceId;
                             })
                             .WithUser(principal)
                             .WithEditUrl(objectId => $"/spaces/edit/{objectId}")
                             .Build(() => new Edit(), async (user, objectId, errors) =>
                                        new EditViewModel
                                        {
                                            Data = space,
                                            Errors = errors,
                                            OperationType = OperationType.Creating,
                                            DataValidation = new SpaceValidator(false)
                                        });
        }

        public async Task<WebPage> GetEditItem(ClaimsPrincipal principal, IHttpRequest request, Guid spaceId)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionEditSpace(principal, spaceId));
            return await this.editApi.Create<Space>()
                             .WithRequest(request)
                             .WithUser(principal)
                             // .WithLookup(() => )
                             .BuildForGet(() => new Edit(), async (user, errors) => new EditViewModel
                                                                                    {
                                                                                        Data = await spaceConverter.ConvertToModel(
                                                                                                this.spaces.GetSpaceById(spaceId),
                                                                                                this.spaces.GetChannelsBySpace(spaceId)),
                                                                                        Errors = errors,
                                                                                        OperationType = OperationType.Updating,
                                                                                        DataValidation = new SpaceValidator(true)
                                                                                    });
        }

        public async Task<WebPage> EditItem(ClaimsPrincipal principal, IHttpRequest request, Guid spaceId, Space space)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionEditSpace(principal, spaceId));

            return await this.editApi.Create<Space>()
                             .WithRequest(request)
                             .WithValidation(() => this.ValidateItem(space, true))
                             .WithPersistence((user) => this.spaces.UpdateSpace(spaceId,
                                                                                space.Name,
                                                                                user.UserId,
                                                                                space.ManagementAddresses?.Select(x => TryGetMailAddress(x))?.ToArray() ?? new MailAddress[0],
                                                                                space.ManagementDomains,
                                                                                space.PushAddresses?.Select(x => TryGetMailAddress(x))?.ToArray() ?? new MailAddress[0],
                                                                                space.PushDomains).Then().Return(x => x.Id))
                             .WithUser(principal)
                             // .WithLookup(() => this.spaceConverter.ConvertToModel(this.spaces.GetSpaceById(spaceId), this.spaces.GetChannelsBySpace(spaceId)))
                             .WithEditUrl(objectId => $"/spaces/edit/{objectId}")
                             .BuildForSubmit(() => new Edit(), async (user, errors) =>
                                                 new EditViewModel
                                                 {
                                                     Data = space,
                                                     Errors = errors,
                                                     OperationType = OperationType.Updating,
                                                     DataValidation = new SpaceValidator(true)
                                                 });
        }

        private MailAddress TryGetMailAddress(string p0)
        {
            try
            {
                return new MailAddress(p0);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<WebPage> DeleteItem(ClaimsPrincipal principal, IHttpRequest request, Guid spaceId)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionDeleteSpace(principal, spaceId));

            return WebPage.Create(new Layout(), new LayoutViewModel
                                                {
                                                    Body = null,
                                                    Author = "Base2art",
                                                    Title = "Nuget Space List",
                                                    Url = request.Uri.AbsolutePath,
                                                    User = principal
                                                });
        }

        private Task<IQualityManagementResult> ValidateItem(Space space, bool update = false)
        {
            IQualityManagementLookup lookup = new QualityManagementLookup<Space>().AddValidator(new SpaceValidator(update));

            return lookup.ValidateAndVerifyResult(space);
        }
    }
}