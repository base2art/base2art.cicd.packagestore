namespace Base2art.CiCd.PackageStore.Web.UI.MoveOuts
{
    using System.Text.RegularExpressions;
    using Base2art.Web.Razor.Forms.ViewModels;

    public class MultiAddInputModel : IValuesModel,
                                      ILabelModel,
                                      IReadOnlyModel,
                                      INamedAndIdedModel,
                                      IPatternedModel,
                                      ITypeModel

    {
        public string Label { get; set; }

        public bool IsReadOnly { get; set; }

        public string Id { get; set; }

        public string InputName { get; set; }

        public string[] Values { get; set; }
        
        public Regex Pattern { get; set; }
        
        public string Type { get; set; }
    }
}