namespace Base2art.CiCd.PackageStore.Web
{
    using System;
    using System.Net.Mail;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Exceptions;
    using Data;

    public class RequireUIAuthProvider : IRequireUIAuthProvider
    {
        private readonly KeyRepo keys;
        private readonly MembershipRepo users;
        private readonly ChannelRepo areas;
        private readonly string[] adminDomains;
        private readonly bool isAdminManagement;
        private readonly MailAddress[] adminAddresses;

        public RequireUIAuthProvider(
            KeyRepo keys,
            MembershipRepo users,
            ChannelRepo areas,
            MailAddress[] adminAddresses,
            string[] adminDomains,
            bool isAdminManagement)
        {
            this.keys = keys;
            this.users = users;
            this.areas = areas;
            this.adminDomains = adminDomains;
            this.isAdminManagement = isAdminManagement;
            this.adminAddresses = adminAddresses ?? new MailAddress[0];
        }

        // SPACES
        async Task IRequireUIAuthProvider.RequireActionManageAdmin(ClaimsPrincipal principal)
        {
            this.PerformAdminCheck(principal);

            var user = await this.users.GetUser(principal);
            var address = new MailAddress(user.Email);

            if (IsAdmin(address))
            {
                return;
            }

            if (this.adminAddresses.Length + this.adminDomains.Length == 0)
            {
                return;
            }

            this.Throw();
        }

        async Task IRequireUIAuthProvider.RequireActionListSpaces(ClaimsPrincipal principal)
        {
            this.PerformAdminCheck(principal);
            return;
        }

        async Task IRequireUIAuthProvider.RequireActionCreateSpace(ClaimsPrincipal principal)
        {
            this.PerformAdminCheck(principal);
            var user = await this.users.GetUser(principal);
            var address = new MailAddress(user.Email);

            if (IsAdmin(address))
            {
                return;
            }

            this.Throw();
        }

        async Task IRequireUIAuthProvider.RequireActionEditSpace(ClaimsPrincipal principal, Guid spaceId)
        {
            this.PerformAdminCheck(principal);
            var user = await this.users.GetUser(principal);
            var address = new MailAddress(user.Email);

            if (IsAdmin(address))
            {
                return;
            }

            var space = await this.areas.GetSpaceById(spaceId);

            if (RequireAuthProviders.IsAllowed(space.ManagementAddresses, space.ManagementDomains, address))
            {
                return;
            }

            this.Throw();
        }

        async Task IRequireUIAuthProvider.RequireActionDeleteSpace(ClaimsPrincipal principal, Guid spaceId)
        {
            this.PerformAdminCheck(principal);
            var user = await this.users.GetUser(principal);
            var address = new MailAddress(user.Email);

            if (IsAdmin(address))
            {
                return;
            }

            var space = await this.areas.GetSpaceById(spaceId);
            if (RequireAuthProviders.IsAllowed(space.ManagementAddresses, space.ManagementDomains, address))
            {
                return;
            }

            this.Throw();
        }

        // Keys
        Task IRequireUIAuthProvider.RequireActionListKeys(ClaimsPrincipal principal)
        {
            this.PerformAdminCheck(principal);
            return Task.CompletedTask;
        }

        Task IRequireUIAuthProvider.RequireActionCreateKey(ClaimsPrincipal principal)
        {
            this.PerformAdminCheck(principal);
            return Task.CompletedTask;
        }

        Task IRequireUIAuthProvider.RequireActionDeleteKey(ClaimsPrincipal principal, Guid keyId)
        {
            this.PerformAdminCheck(principal);
            // This check is done by the data later, the key intrinsically belongs to the user. it will be not found if the user doesn't own it.
            return Task.CompletedTask;
        }

        // Channels
        async Task IRequireUIAuthProvider.RequireActionCreateChannel(ClaimsPrincipal principal, Guid spaceId)
        {
            this.PerformAdminCheck(principal);
            var user = await this.users.GetUser(principal);
            var address = new MailAddress(user.Email);

            if (IsAdmin(address))
            {
                return;
            }

            var space = await this.areas.GetSpaceById(spaceId);
            if (RequireAuthProviders.IsAllowed(space.ManagementAddresses, space.ManagementDomains, address))
            {
                return;
            }

            this.Throw();
        }

        async Task IRequireUIAuthProvider.RequireActionEditChannel(ClaimsPrincipal principal, Guid spaceId, Guid channelId)
        {
            this.PerformAdminCheck(principal);
            var user = await this.users.GetUser(principal);
            var address = new MailAddress(user.Email);

            if (IsAdmin(address))
            {
                return;
            }

            var space = await this.areas.GetSpaceById(spaceId);
            if (RequireAuthProviders.IsAllowed(space.ManagementAddresses, space.ManagementDomains, address))
            {
                return;
            }

            this.Throw();
        }

        private bool IsAdmin(MailAddress address) => RequireAuthProviders.IsAllowed(this.adminAddresses, this.adminDomains, address);

        private void PerformAdminCheck(ClaimsPrincipal principal)
        {
            if (principal?.Identity?.IsAuthenticated != true)
            {
                throw new NotAuthenticatedException();
            }

            if (!this.isAdminManagement)
            {
                if (this.adminAddresses.Length + this.adminDomains.Length == 0)
                {
                    throw new RedirectException("/admin/management", false, false);
                }
            }
        }

        private void Throw()
        {
            throw new Base2art.Web.Exceptions.NotAuthorizedToResourceException();
            throw new AccessViolationException();
        }
    }
}