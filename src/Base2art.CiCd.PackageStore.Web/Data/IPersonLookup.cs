namespace Base2art.CiCd.PackageStore.Web.Data
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;

    public interface IPersonLookup
    {
        Task<IPersonData> GetPerson(Guid personId);
        Task<IReadOnlyDictionary<Guid, IPersonData>> GetPersons(IEnumerable<Guid> items);
    }
}