namespace Base2art.CiCd.PackageStore.Web.Public.Resources
{
    using System.Collections.Generic;

    public class AutoCompleteResult : Dictionary<string, object>
    {
        public AutoCompleteResult()
        {
            this.Add("@context", new Dictionary<string, string>
                                 {
                                     {"@vocab", "http://schema.nuget.org/schema#"}
                                 });
        }

        public int totalHits
        {
            get => (int) this[nameof(this.totalHits)];
            set => this[nameof(this.totalHits)] = value;
        }

        public string[] data
        {
            get => (string[]) this[nameof(this.data)];
            set => this[nameof(this.data)] = value;
        }
    }
}