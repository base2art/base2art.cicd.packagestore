namespace Base2art.CiCd.PackageStore.Web
{
    using System;

    public class UrlProvider : IUrlProvider
    {
        private readonly string value;

        public UrlProvider(string value) => this.value = value;

        public static string OverridingValue { get; set; }

        public string Value => string.IsNullOrWhiteSpace(OverridingValue) ? this.value : OverridingValue;

        public string GetBaseUrl(Guid spaceId, Guid channelId, Guid readKey)
        {
            return this.Value.EndsWith("/")
                       ? $"{this.Value}{spaceId}/{channelId}/{readKey:D}"
                       : $"{this.Value}/{spaceId}/{channelId}/{readKey:D}";
        }

        public Uri GetBaseUrl()
            => new Uri(this.Value);
    }
}