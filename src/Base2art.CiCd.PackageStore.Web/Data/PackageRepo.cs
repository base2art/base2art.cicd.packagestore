namespace Base2art.CiCd.PackageStore.Web.Data
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using NuGet.Versioning;

    public class PackageRepo
    {
        private readonly IDirectoryProvider directoryProvider;

        public PackageRepo(IDirectoryProvider directoryProvider) => this.directoryProvider = directoryProvider;

        public async Task Add(Guid spaceId, Guid channelId, byte[] toArray)
        {
            var dir = this.directoryProvider.GetPackagesDir(spaceId, channelId);

            var path = Path.Combine(dir.FullName, $"{Guid.NewGuid():N}.nupkg");
            File.WriteAllBytes(path, toArray);
        }

        public async Task<XDocument> GetSpecFile(Guid spaceId, Guid channelId, string packageNameAndVersion)
        {
            var versionDir = await this.VersionDir(spaceId, channelId, packageNameAndVersion);
            return SpecFile(versionDir);
        }

        public async Task<XDocument> GetSpecFile(Guid spaceId, Guid channelId, string packageName, string version)
        {
            var versionDir = await this.VersionDir(spaceId, channelId, packageName, version);
            return SpecFile(versionDir);
        }

        public async Task<FileInfo> GetSpecFileInfo(Guid spaceId, Guid channelId, string packageName, string version)
        {
            var versionDir = await this.VersionDir(spaceId, channelId, packageName, version);
            return SpecFileInfo(versionDir);
        }

        public async Task<(string, string[])[]> Search(Guid spaceId, Guid channelId, string s, bool prerelease) =>
            this.SearchEnumerable(spaceId, channelId, s, prerelease).ToArray();

        public async Task<string[]> GetVersionsForPackage(Guid spaceId, Guid channelId, string packageId, bool includePrerelease)
        {
            var dir = this.directoryProvider.GetPackagesDir(spaceId, channelId);

            var packageDir = dir.GetDirectories("*", SearchOption.TopDirectoryOnly);
            var matchingPackageDir = packageDir.FirstOrDefault(x => string.Equals(x.Name, packageId));
            if (matchingPackageDir == null)
            {
                return new string[0];
            }

            return this.GetVersions(matchingPackageDir, includePrerelease).ToArray();
        }

        public async Task<FileInfo> GetPackageFile(Guid spaceId, Guid channelId, string packageName, string packageVersion)
        {
            var versionDir = await VersionDir(spaceId, channelId, packageName, packageVersion);

            return ContentFileInfo(versionDir);
        }

        public async Task UnlistPackage(Guid spaceId, Guid channelId, string packageName, string packageVersion)
        {
            var versionDir = await this.VersionDir(spaceId, channelId, packageName, packageVersion);

            if (versionDir != null)
            {
                var path = Path.Combine(versionDir.FullName, "nuspec.ref");
                FileSystemScanner.TryDelete(path);
            }
        }

        public Task TriggerScanForChanges(Guid spaceId, Guid channelId)
            => this.directoryProvider.TriggerScanForChanges(spaceId, channelId);

        public Task ScanForChangesAsync(Guid spaceId, Guid channelId)
            => this.directoryProvider.ScanForChangesAsync(spaceId, channelId);

        private async Task<DirectoryInfo> VersionDir(Guid spaceId, Guid channelId, string packageName, string version)
        {
            var dir = this.directoryProvider.GetPackagesDir(spaceId, channelId);

            var packageDir = dir.GetDirectories("*", SearchOption.TopDirectoryOnly)
                                .FirstOrDefault(x => string.Equals(packageName, x.Name, StringComparison.OrdinalIgnoreCase));

            var versionDir = packageDir?.GetDirectories("*", SearchOption.TopDirectoryOnly)
                                       ?.FirstOrDefault(x => string.Equals(x.Name, version, StringComparison.OrdinalIgnoreCase));
            return versionDir;
        }

        private IEnumerable<(string, string[])> SearchEnumerable(Guid spaceId, Guid channelId, string s, bool prerelease)
        {
            var dir = this.directoryProvider.GetPackagesDir(spaceId, channelId);

            var packageDir = dir.GetDirectories();
            var matchingPackageDirs = packageDir.Where(x => x.Name.Contains(s?.ToLowerInvariant() ?? "")).ToArray();

            foreach (var matchingPackageDir in matchingPackageDirs)
            {
                yield return (matchingPackageDir.Name, this.GetVersions(matchingPackageDir, prerelease).ToArray());
            }
        }

        private IEnumerable<string> GetVersions(DirectoryInfo matchingPackageDir, bool includePrerelease)
        {
            return matchingPackageDir?.EnumerateDirectories()
                                     .Select(x => x.Name)
                                     .Where(x => NuGetVersion.TryParse(x, out _))
                                     .Where(x => includePrerelease || !NuGetVersion.Parse(x).HasMetadata)
                   ?? Array.Empty<string>();
        }

        private async Task<DirectoryInfo> VersionDir(Guid spaceId, Guid channelId, string packageNameAndVersion)
        {
            var dir = this.directoryProvider.GetPackagesDir(spaceId, channelId);

            var packageDir = dir.GetDirectories("*", SearchOption.TopDirectoryOnly)
                                .FirstOrDefault(x => packageNameAndVersion.StartsWith(x.Name + ".", StringComparison.OrdinalIgnoreCase));

            var versionDir = packageDir?.GetDirectories("*", SearchOption.TopDirectoryOnly)
                                       ?.FirstOrDefault(x => string.Equals($"{packageDir.Name}.{x.Name}", packageNameAndVersion,
                                                                           StringComparison.OrdinalIgnoreCase));
            return versionDir;
        }

        private static XDocument SpecFile(DirectoryInfo versionDir)
        {
            var info = SpecFileInfo(versionDir);
            if (info == null)
            {
                return null;
            }

            return XDocument.Load(info.FullName);
        }

        private static FileInfo SpecFileInfo(DirectoryInfo versionDir) => FileInfoFor(versionDir, @for: "nuspec.ref");
        private static FileInfo ContentFileInfo(DirectoryInfo versionDir) => FileInfoFor(versionDir, @for: "nupkg.ref");

        private static FileInfo FileInfoFor(DirectoryInfo versionDir, string @for)
        {
            if (versionDir == null)
            {
                return null;
            }

            var refPath = Path.Combine(versionDir.FullName, @for);
            if (!File.Exists(refPath))
            {
                return null;
            }

            var content = File.ReadAllText(refPath).Trim();
            var newPath = Path.Combine(versionDir.FullName, content);

            if (!File.Exists(newPath))
            {
                return null;
            }

            return new FileInfo(newPath);
        }
    }
}