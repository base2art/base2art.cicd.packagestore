namespace Base2art.CiCd.PackageStore.Web.Public.Resources
{
    using System;
    using System.Collections.Generic;

    public class CategoryItem : Dictionary<string, object>
    {
        public CategoryEntry catalogEntry
        {
            get => (CategoryEntry) this[nameof(this.catalogEntry)];
            set => this[nameof(this.catalogEntry)] = value;
        }

        public string packageContent
        {
            get => (string) this[nameof(this.packageContent)];
            set => this[nameof(this.packageContent)] = value;
        }

        public string registration
        {
            get => (string) this[nameof(this.registration)];
            set => this[nameof(this.registration)] = value;
        }

        public Guid commitId
        {
            get => (Guid) this[nameof(this.commitId)];
            set => this[nameof(this.commitId)] = value;
        }

        public string commitTimeStamp
        {
            get => (string) this[nameof(this.commitTimeStamp)];
            set => this[nameof(this.commitTimeStamp)] = value;
        }

        public void SetInternalId(string id)
        {
            this["@id"] = id;
        }
    }
}