namespace Base2art.CiCd.PackageStore.Web.UI.Public.Resources
{
    using System;

    public class PersonAudit
    {
        public Guid PersonId { get; set; }
        public string PersonName { get; set; }
    }
}