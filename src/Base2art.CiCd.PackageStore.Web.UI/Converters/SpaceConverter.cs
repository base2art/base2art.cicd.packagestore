namespace Base2art.CiCd.PackageStore.Web.UI.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Data;
    using Data.Models;
    using Public.Resources;

    public class SpaceConverter
    {
        private readonly IPersonLookup lookup;

        public SpaceConverter(IPersonLookup lookup)
        {
            this.lookup = lookup;
        }

        public async Task<Space> ConvertToModel(Task<ISpace> space, Task<IChannel[]> channels)
        {
            var spaceValue = await space;
            var channelsValue = await channels;
            var person = await this.lookup.GetPerson(spaceValue.OwnerId);

            return ConvertToModel(
                                  spaceValue,
                                  channelsValue.ToArray(),
                                  person);
        }

        public async Task<Space[]> ConvertToModel(Task<ISpace[]> spaces, Task<IReadOnlyDictionary<Guid, IChannel[]>> channels)
        {
            var spacesValue = await spaces;
            var channelsValue = await channels;

            var owners = spacesValue.Select(x => x.OwnerId);

            var persons = await this.lookup.GetPersons(owners);

            return spacesValue.Select(x => this.ConvertToModel(x, TryGet(channelsValue, x.Id).ToArray(), this.TryGet(persons, x.OwnerId)))
                              .ToArray();
        }

        public Space ConvertToModel(ISpace space, IChannel[] channels, IPersonData user)
        {
            if (space == null)
            {
                return null;
            }

            return new Space
                   {
                       Id = space.Id,
                       ManagementAddresses = space.ManagementAddresses?.Select(x => x?.ToString())?.ToArray() ?? new string[0],
                       ManagementDomains = space.ManagementDomains ?? new string[0],
                       PushAddresses = space.PushAddresses?.Select(x => x?.ToString())?.ToArray() ?? new string[0],
                       PushDomains = space.PushDomains ?? new string[0],
                       Name = space.Name,
                       Owner = user.ConvertToModel(),
                       Channels = channels.Select(x =>
                                                      new Channel
                                                      {
                                                          Id = x.Id,
                                                          Name = x.Name,
                                                      }).ToArray()
                   };
        }

        private IPersonData TryGet(IReadOnlyDictionary<Guid, IPersonData> persons, Guid personId)
        {
            return persons.ContainsKey(personId) ? persons[personId] : null;
        }

        private IChannel[] TryGet(IReadOnlyDictionary<Guid, IChannel[]> channels, Guid spaceId)
        {
            return channels.ContainsKey(spaceId) ? channels[spaceId] : new IChannel[0];
        }
    }
}