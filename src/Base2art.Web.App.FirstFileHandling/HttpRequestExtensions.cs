namespace Base2art.Web.App.FirstFileHandling
{
    using System;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;

    public static class HttpRequestExtensions
    {
        //https://github.com/loic-sharma/BaGet/blob/e60e91618d8654727c93df60db346d3c3cb2fa07/src/BaGet.Hosting/Extensions/HttpRequestExtensions.cs#L13
        public static async Task<Stream> GetUploadStreamOrNullAsync(this HttpRequest request, CancellationToken? cancellationToken = null)
        {
            // Try to get the nupkg from the multipart/form-data. If that's empty,
            // fallback to the request's body.
            Stream rawUploadStream = null;
            try
            {
                if (request.HasFormContentType && request.Form.Files.Count > 0)
                {
                    rawUploadStream = request.Form.Files[0].OpenReadStream();
                }
                else
                {
                    rawUploadStream = request.Body;
                }

                // Convert the upload stream into a temporary file stream to
                // minimize memory usage.
                if (cancellationToken.HasValue)
                {
                    return await rawUploadStream?.AsTemporaryFileStreamAsync(cancellationToken.Value);
                }

                return await rawUploadStream?.AsTemporaryFileStreamAsync();
            }
            finally
            {
                rawUploadStream?.Dispose();
            }
        }

        // https://github.com/loic-sharma/BaGet/blob/e60e91618d8654727c93df60db346d3c3cb2fa07/src/BaGet.Core/Extensions/StreamExtensions.cs#L22
        public static async Task<FileStream> AsTemporaryFileStreamAsync(
            this Stream original,
            CancellationToken cancellationToken = default)
        {
            // See: https://github.com/dotnet/corefx/blob/master/src/Common/src/CoreLib/System/IO/Stream.cs#L35
            const int DefaultCopyBufferSize = 81920;

            var result = new FileStream(
                                        Path.GetTempFileName(),
                                        FileMode.Create,
                                        FileAccess.ReadWrite,
                                        FileShare.None,
                                        DefaultCopyBufferSize,
                                        FileOptions.DeleteOnClose);

            try
            {
                await original.CopyToAsync(result, DefaultCopyBufferSize, cancellationToken);
                result.Position = 0;
            }
            catch (Exception)
            {
                result.Dispose();
                throw;
            }

            return result;
        }
    }
}