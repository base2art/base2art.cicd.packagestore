﻿namespace Base2art.Web.App.FirstFileHandling
{
    using System;
    using System.Collections.Generic;
    using Configuration;
    using Server.Registration;

    public class Module : RegistrationBase, IModule
    {
        public IReadOnlyList<IEndpointConfiguration> Endpoints { get; } = new List<IEndpointConfiguration>();
        public IReadOnlyList<ITypeInstanceConfiguration> GlobalEndpointAspects { get; } = new List<ITypeInstanceConfiguration>();
        public IReadOnlyList<ITaskConfiguration> Tasks { get; } = new List<ITaskConfiguration>();
        public IReadOnlyList<ITypeInstanceConfiguration> GlobalTaskAspects { get; } = new List<ITypeInstanceConfiguration>();
        public IHealthChecksConfiguration HealthChecks { get; } = new MyHc();

        public IApplicationConfiguration Application { get; } = new MyApp();

        public IReadOnlyList<IInjectionItemConfiguration> Injection { get; } = new List<IInjectionItemConfiguration>();
        public IReadOnlyList<ICorsItemConfiguration> Cors { get; } = new List<ICorsItemConfiguration>();

        private class MyHc : IHealthChecksConfiguration
        {
            public IReadOnlyList<IHealthCheckConfiguration> Items { get; } = new List<IHealthCheckConfiguration>();
            public IReadOnlyList<ITypeInstanceConfiguration> Aspects { get; } = new List<ITypeInstanceConfiguration>();
        }

        private class MyApp : IApplicationConfiguration
        {
            public IReadOnlyList<ITypeInstanceConfiguration> Filters { get; } = new List<ITypeInstanceConfiguration>();

            public IReadOnlyList<IModelBindingConfiguration> ModelBindings { get; }
                = new IModelBindingConfiguration[0];

            public IReadOnlyList<ITypeInstanceConfiguration> InputFormatters { get; } = new List<ITypeInstanceConfiguration>
                                                                                        {
                                                                                            new TypeInstanceConfiguration
                                                                                            {
                                                                                                Parameters = new Dictionary<string, object>(),
                                                                                                Properties = new Dictionary<string, object>(),
                                                                                                Type = typeof(FirstFormFileFilter)
                                                                                            }
                                                                                        };

            public IReadOnlyList<ITypeInstanceConfiguration> OutputFormatters { get; } = new List<ITypeInstanceConfiguration>();
            // {
            //     new ModelBindingConfiguration(typeof(FirstFormFileBinder), typeof(IFirstFormFile)),
            // };

            public IReadOnlyDictionary<string, string> MediaTypes { get; } = new Dictionary<string, string>();
            public IExceptionConfiguration Exceptions { get; } = new MyExConfig();

            private class MyExConfig : IExceptionConfiguration
            {
                public bool RegisterCommonHandlers { get; } = false;
                public bool AllowStackTraceInOutput { get; } = false;
                public IReadOnlyList<ITypeInstanceConfiguration> Loggers { get; } = new List<ITypeInstanceConfiguration>();
            }

            private class TypeInstanceConfiguration : ITypeInstanceConfiguration
            {
                public Type Type { get; set; }
                public IReadOnlyDictionary<string, object> Parameters { get; set; }
                public IReadOnlyDictionary<string, object> Properties { get; set; }
            }
        }
    }
}