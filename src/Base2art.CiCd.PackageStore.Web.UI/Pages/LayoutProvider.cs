namespace Base2art.CiCd.PackageStore.Web.UI.Pages
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Flows;
    using Base2art.Web.Http;
    using Base2art.Web.Pages;
    using Data;
    using ViewModels;

    public class LayoutProvider : ILayoutProvider
    {
        private readonly MembershipRepo userLookup;

        private readonly bool showCopyrightCompany;
        // private PersonLookup userLookup;

        public LayoutProvider(string showCopyrightCompany, MembershipRepo membershipRepo)
        {
            this.userLookup = membershipRepo;
            // const StringComparison comp = StringComparison.OrdinalIgnoreCase;

            this.showCopyrightCompany = ParseBool(showCopyrightCompany, () => true);
            // this.userLookup = new PersonLookup(x=> membershipRepo.GetUser(x));
        }

        public async Task<WebPage> BuildLayout(Task<WebPage> body, IHttpRequest request, ClaimsPrincipal user)
        {
            var page = WebPage.Create(new Layout(), new LayoutViewModel
                                                    {
                                                        Body = body,
                                                        Author = "Base2art",
                                                        Title = "Nuget Space List",
                                                        Url = request?.Uri?.AbsolutePath ?? string.Empty,
                                                        User = user,
                                                        UserData = user.Identity.IsAuthenticated
                                                                       ? await this.userLookup.GetUser(user)
                                                                       : null,
                                                        ShowCopyright = this.showCopyrightCompany
                                                    });

            return page;
        }

        private static bool ParseBool(string s, Func<bool> func)
        {
            // const StringComparison comp = StringComparison.OrdinalIgnoreCase;
            return s.StartsWith("#{") ? func() :
                   bool.TryParse(s, out var value) ? value : func();
        }
    }
}