namespace Base2art.Web.Flows
{
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Http;
    using Pages;

    public interface ILayoutProvider
    {
        Task<WebPage> BuildLayout(Task<WebPage> body, IHttpRequest request, ClaimsPrincipal user);
    }
}