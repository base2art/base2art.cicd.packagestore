namespace Base2art.CiCd.PackageStore.Web.UI.Public.Controllers
{
    using System;
    using System.Net.Mail;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Http;
    using Base2art.Web.Pages;
    using Converters;
    using Data;
    using Pages;
    using Pages.Spaces;
    using Resources;
    using ViewModels;
    using ViewModels.Spaces;

    public class HomeController
    {
        private readonly PackageRepo repo;
        private readonly ChannelRepo channelStorage;
        private readonly MembershipRepo users;

        public HomeController(PackageRepo packages, ChannelRepo channelStorage, MembershipRepo users)
        {
            this.repo = packages;
            this.channelStorage = channelStorage;
            this.users = users;
            // this.channelStorage = new ChannelRepo(provider);
            // this.users = new MembershipRepo(provider);
        }

        // public async Task<WebPage> Index(ClaimsPrincipal principal, IHttpRequest request)
        // {
        //     var user = await users.GetUser(principal);
        //     var address = new MailAddress(user.Email);
        //     var host = address.Host;
        //     // var channels = await this.channelStorage.GetChannelsByHost(host);
        //
        //     return WebPage.Create(new Layout(), new LayoutViewModel
        //                                         {
        //                                             Body = this.Pages(host),
        //                                             Author = "Base2art",
        //                                             Title = "Nuget Space Lookup",
        //                                             Url = request.Uri.AbsolutePath,
        //                                             User = principal
        //                                         });
        // }
        //
        // private async Task<WebPage> Pages(string host)
        // {
        //     var spaces = this.channelStorage.GetSpacesByHost(host);
        //     var converter = new SpaceConverter(new PersonLookup(x => this.users.GetUser(x)));
        //     return WebPage.Create(
        //                           new Index(),
        //                           new IndexViewModel
        //                           {
        //                               Spaces = await converter.ConvertToModel(spaces),
        //                           });
        // }
    }
}