namespace Base2art.CiCd.PackageStore.Web.Fixtures.TestServer
{
    using System;
    using Microsoft.AspNetCore.Mvc.ActionConstraints;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Microsoft.AspNetCore.Mvc.Internal;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.DependencyInjection;
    using WebApiRunner.Server;

    public class FrameworkShim : IFrameworkShim
    {
        public IRouter GetDefaultRouteHandler(IServiceProvider provider)
            => provider.GetRequiredService<MvcRouteHandler>();

        public IActionConstraintMetadata CreateHttpMethodActionConstraint(string[] methodNames)
            => new HttpMethodActionConstraint(methodNames);

        public IRouter CreateMegaRoute(IServiceProvider provider)
            => AttributeRouting.CreateAttributeMegaRoute(provider);

        public IFilterMetadata CreateControllerActionFilter()
            => new ControllerActionFilter();

        public IFilterMetadata CreateControllerResultFilter()
            => new ControllerResultFilter();
    }
}