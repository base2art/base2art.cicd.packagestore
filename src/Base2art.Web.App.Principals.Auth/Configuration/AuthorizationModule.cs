namespace Base2art.Web.App.Principals.Auth.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using App.Configuration;
    using ComponentModel.Composition;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization.Infrastructure;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.HttpOverrides;
    using Microsoft.Extensions.DependencyInjection;
    using Server.Registration;

    public class AuthorizationModule : RegistrationBase, IModule //, IAuthorizationModule
    {
        private readonly string requiredDomain;
        private readonly bool authorizeAll;

        public AuthorizationModule(string authorizeAll, string requiredDomain)
        {
            if (requiredDomain == "#{Auth_RequiredDomain}")
            {
                var googleAuthRequiredDomain = Path.Combine(
                                                            Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                                                            ".config",
                                                            "base2art",
                                                            "deployment",
                                                            "coordinator",
                                                            "Auth_RequiredDomain");

                this.requiredDomain = File.ReadAllText(googleAuthRequiredDomain).Trim();
            }
            else
            {
                this.requiredDomain = requiredDomain;
            }

            const StringComparison comp = StringComparison.OrdinalIgnoreCase;

            this.authorizeAll = !authorizeAll.StartsWith("#{") && string.Equals(authorizeAll, bool.TrueString, comp);
        }

        public IReadOnlyList<IEndpointConfiguration> Endpoints { get; } = new List<IEndpointConfiguration>();
        public IReadOnlyList<ITypeInstanceConfiguration> GlobalEndpointAspects { get; } = new List<ITypeInstanceConfiguration>();
        public IReadOnlyList<ITaskConfiguration> Tasks { get; } = new List<ITaskConfiguration>();
        public IReadOnlyList<ITypeInstanceConfiguration> GlobalTaskAspects { get; } = new List<ITypeInstanceConfiguration>();
        public IHealthChecksConfiguration HealthChecks { get; } = new MyHc();

        public IApplicationConfiguration Application { get; } = new MyApp();

        public IReadOnlyList<IInjectionItemConfiguration> Injection { get; } = new List<IInjectionItemConfiguration>();
        public IReadOnlyList<ICorsItemConfiguration> Cors { get; } = new List<ICorsItemConfiguration>();

        public override void RegisterSystemServices(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services, IServerConfiguration config)
        {
            base.RegisterSystemServices(builder, services, config);

            services.Bind<IAuthorizationModule>()
                    .To((x) => new AuthorizationModuleInstance
                               {
                                   RequiredDomain = this.requiredDomain
                               });

            var headers = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            builder.Services.Configure<ForwardedHeadersOptions>(options => options.ForwardedHeaders = headers);
            builder.AddAuthorization();
        }

        public override void RegisterFilters(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
            base.RegisterFilters(builder, services, config);

            if (this.authorizeAll)
            {
                var policy = new AuthorizationPolicyBuilder()
                             .AddRequirements(new DenyAnonymousAuthorizationRequirement())
                             .Build();

                builder.AddMvcOptions(x => x.Filters.Add(new CustomAuthFilter(policy)));
            }
        }

        public override void RegisterNativeMiddleware(IApplicationBuilder app, IServerConfiguration config)
        {
            base.RegisterNativeMiddleware(app, config);
            app.UseMiddleware<SimpleMiddleware>();
            app.UseForwardedHeaders();
        }

        public class AuthorizationModuleInstance : IAuthorizationModule
        {
            public string RequiredDomain { get; set; }
        }

        private class MyHc : IHealthChecksConfiguration
        {
            public IReadOnlyList<IHealthCheckConfiguration> Items { get; } = new List<IHealthCheckConfiguration>();
            public IReadOnlyList<ITypeInstanceConfiguration> Aspects { get; } = new List<ITypeInstanceConfiguration>();
        }

        private class MyApp : IApplicationConfiguration
        {
            public IReadOnlyList<ITypeInstanceConfiguration> Filters { get; } = new List<ITypeInstanceConfiguration>();

            public IReadOnlyList<IModelBindingConfiguration> ModelBindings { get; } = new IModelBindingConfiguration[0];
            public IReadOnlyList<ITypeInstanceConfiguration> InputFormatters { get; } = new List<ITypeInstanceConfiguration>();
            public IReadOnlyList<ITypeInstanceConfiguration> OutputFormatters { get; } = new List<ITypeInstanceConfiguration>();

            public IReadOnlyDictionary<string, string> MediaTypes { get; } = new Dictionary<string, string>();
            public IExceptionConfiguration Exceptions { get; } = new MyExConfig();

            private class MyExConfig : IExceptionConfiguration
            {
                public bool RegisterCommonHandlers { get; } = false;
                public bool AllowStackTraceInOutput { get; } = false;
                public IReadOnlyList<ITypeInstanceConfiguration> Loggers { get; } = new List<ITypeInstanceConfiguration>();
            }
        }
    }
}