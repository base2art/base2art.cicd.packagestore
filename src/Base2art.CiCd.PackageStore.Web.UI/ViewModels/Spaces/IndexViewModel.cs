namespace Base2art.CiCd.PackageStore.Web.UI.ViewModels.Spaces
{
    using System.Collections.Generic;
    using System.Net.Mail;
    using Data;
    using Data.Models;
    using Public.Resources;

    public class IndexViewModel
    {
        public Space[] Spaces { get; set; }

        // public KeyValuePair<string, string>[] UserKeys { get; set; }
        
        public MailAddress UserAddress { get; set; }

        // public IChannel[] Channels { get; set; }
    }
}