namespace Base2art.Web.App.Principals.FacebookAuth
{
    using System;
    using System.IO;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Auth.Configuration;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Authentication.OAuth;
    using Microsoft.Extensions.DependencyInjection;

    public class FacebookAuthenticationModule : SingleAuthenticationModule
    {
        private readonly string clientId;
        private readonly string clientSecret;

        public FacebookAuthenticationModule(
            string clientId,
            string clientSecret)
        {
            if (clientId == "#{FacebookAuth_ClientId}" && clientSecret == "#{FacebookAuth_ClientSecret}")
            {
                var googleAuthClientId = Path.Combine(
                                                      Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                                                      ".config",
                                                      "base2art",
                                                      "deployment",
                                                      "coordinator",
                                                      "FacebookAuth_ClientId");
                var googleAuthClientSecret = Path.Combine(
                                                          Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                                                          ".config",
                                                          "base2art",
                                                          "deployment",
                                                          "coordinator",
                                                          "FacebookAuth_ClientSecret");
                this.clientId = File.ReadAllText(googleAuthClientId).Trim();
                this.clientSecret = File.ReadAllText(googleAuthClientSecret).Trim();
            }
            else
            {
                this.clientId = clientId;
                this.clientSecret = clientSecret;
            }
        }

        protected override string AuthenticationScheme => "facebook";

        protected override void AddAuth(AuthenticationBuilder authBuilder)
        {
            authBuilder.AddFacebook(this.AuthenticationScheme, this.DisplayName, options =>
            {
                // options.AppId
                options.AppId = this.clientId;
                options.AppSecret = this.clientSecret;
                options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.CallbackPath = "/auth/facebook/callback";

#if NETCOREAPP3_1_OR_GREATER
                                                                   options.CorrelationCookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.Unspecified;
#endif

                options.Events.OnCreatingTicket += async arg =>
                {
                    var email = arg.Principal.GetValue(ClaimTypes.Email);

                    ValidateDomain(email, arg.HttpContext, arg.Properties.RedirectUri);
                    arg.Principal = await this.CreatePrincipal(
                                                               arg.Principal.GetValue(ClaimTypes.NameIdentifier),
                                                               email,
                                                               arg.Principal.GetValue(ClaimTypes.Name));
                };
            });
        }
    }
}