namespace Base2art.CiCd.PackageStore.Web.Fixtures.TestServer
{
    using Base2art.Web.App.Configuration;
    using WebApiRunner.Server;
    using WebApiRunner.Server.Runners;

    public class Startup : SharedStartup
    {
        public Startup(IServerConfiguration configuration, IFrameworkShim shim, IBoundTypeRegistrar registrar)
            : base(configuration, new NullApplication(), null, shim, registrar)
        {
        }
    }
}