namespace Base2art.CiCd.PackageStore.Web
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Mail;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Threading.Tasks;
    using Base2art.Web.App.Principals.Auth.Configuration;
    using Base2art.Web.App.Principals.AzureAdAuth;
    using Base2art.Web.App.Principals.FacebookAuth;
    using Base2art.Web.App.Principals.GoogleAuth;
    using Base2art.Web.Flows;
    using Data;
    using Data.Models;
    using Fixtures;
    using FluentAssertions;
    using Newtonsoft.Json.Linq;
    using Public.Resources;
    using UI.Pages;
    using UI.Public.Controllers;
    using Xunit;
    using Xunit.Abstractions;

    public class PackageServiceIntegrationFeature
    {
        private readonly TestServerClient client;
        private readonly ITestOutputHelper testOutputHelper;

        public PackageServiceIntegrationFeature(ITestOutputHelper testOutputHelper)
        {
            // RequireAuthProvider.OverridingValue = false;
            Console.WriteLine(typeof(SpaceController));
            Console.WriteLine(typeof(AuthorizationModule));
            Console.WriteLine(typeof(GoogleAuthenticationModule));
            Console.WriteLine(typeof(FacebookAuthenticationModule));
            Console.WriteLine(typeof(AzureAdAuthenticationModule));
            Console.WriteLine(typeof(Base2art.Web.App.FirstFileHandling.Module));
            Console.WriteLine(typeof(ILayoutProvider));
            Console.WriteLine(typeof(LayoutProvider));

            this.testOutputHelper = testOutputHelper;
            this.client = new TestServerClient(x => this.testOutputHelper.WriteLine(x));
            
            ILayoutProvider prov = new LayoutProvider("", new MembershipRepo(new DefinableDirectoryProvider("#{}")));
        }

        [Theory]
        [InlineData(true, true, 13, "http://localhost:29127/api/v3/{spaceId}/{channelId}/{readPassword}/query", "SearchQueryService")]
        [InlineData(true, false, 12, "http://localhost:29127/api/v3/{spaceId}/{channelId}/{readPassword}/query", "SearchQueryService")]
        [InlineData(false, true, 1, "http://localhost:29127/api/v3/{spaceId}/{channelId}/{readPassword}/package", "PackagePublish/2.0.0")]
        public async void ShouldGetFeatures(bool canRead, bool canWrite, int expectedCount, string expectedId, string expectedType)
        {
            var ids = await this.Setup(canRead, canWrite);
            var response = await this.client.SendRequestToGetJson<dynamic>(
                                                                           HttpMethod.Get,
                                                                           Explode("api/v3/{spaceId}/{channelId}/{readPassword}/index.json", ids));

            string version = response.version;
            JArray resources = response.resources;

            version.Should().Be(@"3.0.0");
            resources.Count.Should().Be(expectedCount);
            var obj = resources[0].ToObject<Dictionary<string, string>>();

            obj.Count.Should().Be(3);
            obj["@id"].Should().Be(Explode(expectedId, ids));
            obj["@type"].Should().Be(expectedType);
        }

        [Fact]
        public async void ShouldListVersions()
        {
            var ids = await this.Setup();
            var path1 = Explode("/api/v3/{spaceId}/{channelId}/{readPassword}/flatcontainer/Newtonsoft.Json/index.json", ids);
            var response = await this.client.SendRequestToGetJson<VersionModel>(HttpMethod.Get, Explode(path1, ids));
            response.Versions.Length.Should().Be(1);

            var path2 = "/api/v3/{spaceId}/{channelId}/{readPassword}/flatcontainer/Microsoft.Data.SqlClient/index.json";
            response = await this.client.SendRequestToGetJson<VersionModel>(HttpMethod.Get, Explode(path2, ids));
            response.Versions.Length.Should().Be(8);
        }

        [Theory]
        // api/v3/{spaceId}/{channelId}/{readPassword}/flatcontainer/{packageId}/{packageVersion}/{packageName}.nupkg
        [InlineData("api/v3/{spaceId}/{channelId}/{readPassword}/flatcontainer/Newtonsoft.Json/12.0.3/Newtonsoft.Json.12.0.3.nupkg", 2596051)]
        [InlineData("api/v3/{spaceId}/{channelId}/{readPassword}/flatcontainer/Microsoft.Data.SqlClient/2.0.0-preview4.20142.4/Microsoft.Data.SqlClient.2.0.0-preview4.20142.4.nupkg",
                    8992832)]
        public async void ShouldDownloadPackage(string path, int length)
        {
            var ids = await this.Setup();

            var bytes = await this.client.SendRequestToBytes(HttpMethod.Get, Explode(path, ids));
//            var bytes = await response.Content.ReadAsByteArrayAsync();
            bytes.Length.Should().Be(length);
        }

        [Theory]
        [InlineData("api/v3/{spaceId}/{channelId}/{readPassword}/flatcontainer/Newtonsoft.Json/12.0.3/Newtonsoft.Json.nuspec", "Newtonsoft.Json")]
        [InlineData("api/v3/{spaceId}/{channelId}/{readPassword}/flatcontainer/Microsoft.Data.SqlClient/2.0.0-preview4.20142.4/Microsoft.Data.SqlClient.nuspec",
                    "Microsoft.Data.SqlClient")]
        public async void ShouldDownloadPackageSpec(string path, string id)
        {
            var ids = await this.Setup();

            var bytes = await this.client.SendRequestToXml(HttpMethod.Get, Explode(path, ids));
            bytes.Should().NotBeNull();
            bytes.Should().NotBeNull();
            // bytes.Root.E
            bytes.Root
                 .Elements().First(x => x.Name.LocalName == "metadata")
                 .Elements().First(x => x.Name.LocalName == "id")
                 .Value.Should().Be(id);
        }

        private static string Explode(string path, (Guid spaceId, Guid channelId, Guid readPassword) ids)
            => path.Replace("{spaceId}", ids.spaceId.ToString("D"))
                   .Replace("{channelId}", ids.channelId.ToString("D"))
                   .Replace("{readPassword}", ids.readPassword.ToString("D"));

        //        [Fact]
//        public async void DownloadPackageSpec()
//        {
//            await this.Setup();
//            var response = await this.SendRequestToGetJson<VersionModel>(HttpMethod.Get, "v3-flatcontainer/Newtonsoft.Json/index.json");
//            response.Versions.Length.Should().Be(1);
//
//            response = await this.SendRequestToGetJson<VersionModel>(HttpMethod.Get, "v3-flatcontainer/Microsoft.Data.SqlClient/index.json");
//            response.Versions.Length.Should().Be(8);
//        }

        private async Task<(Guid, Guid, Guid)> Setup(bool canRead = true, bool canWrite = true)
        {
            var provider = new DefinableDirectoryProvider("#{}");
            var membership = new MembershipRepo(provider);
            var repo = new ChannelRepo(provider);
            var keys = new KeyRepo(provider);

            var testRunId = DateTime.UtcNow.Ticks;

            var user = await TestUser(membership, testRunId);
            var space = await TestSpace(repo, testRunId, user);
            var channel = await TestChannel(repo, space, testRunId);
            var key = await TestKey(keys, testRunId, user, canRead, canWrite);

            // provider.GetPackagesDir(spaceId, channelId).Delete(true);
            // provider = new DefinableDirectoryProvider("#{}");

            Guid spaceId = space.Id;
            Guid channelId = channel.Id;

            using (var webClient = new WebClient())
            {
                await this.DownloadFile(webClient, spaceId, channelId, provider, "Newtonsoft.Json", "12.0.3");
                await this.DownloadFile(webClient, spaceId, channelId, provider, "Microsoft.Data.SqlClient", "2.0.0");
                await this.DownloadFile(webClient, spaceId, channelId, provider, "Microsoft.Data.SqlClient", "1.1.3");
                await this.DownloadFile(webClient, spaceId, channelId, provider, "Microsoft.Data.SqlClient", "1.1.2");
                await this.DownloadFile(webClient, spaceId, channelId, provider, "Microsoft.Data.SqlClient", "1.1.1");
                await this.DownloadFile(webClient, spaceId, channelId, provider, "Microsoft.Data.SqlClient", "1.1.0");
                await this.DownloadFile(webClient, spaceId, channelId, provider, "Microsoft.Data.SqlClient", "1.0.19269.1");
                await this.DownloadFile(webClient, spaceId, channelId, provider, "Microsoft.Data.SqlClient", "2.0.0-preview4.20142.4");
                await this.DownloadFile(webClient, spaceId, channelId, provider, "Microsoft.Data.SqlClient", "2.0.0-preview3.20122.2");
            }

            return (spaceId, channelId, key.Password);
        }

        private static async Task<IKey> TestKey(KeyRepo keys, long testRunId, IUser user, bool canRead, bool canWrite) =>
            await keys.AddKey(
                              $"TEST: {testRunId}",
                              DateTime.UtcNow.AddHours(1),
                              canRead,
                              canWrite,
                              false,
                              user.Id);

        private static async Task<IChannel> TestChannel(ChannelRepo repo, ISpace space, long testRunId) =>
            await repo.AddChannel(space.Id, $"Test: {testRunId}");

        private static async Task<ISpace> TestSpace(ChannelRepo repo, long testRunId, IUser user)
        {
            return await repo.AddSpace(
                                       $"Test: {testRunId}",
                                       user.Id,
                                       new MailAddress[0],
                                       new string[] {"base2art.com"},
                                       new MailAddress[0],
                                       new string[] {"base2art.com"});
        }

        private static async Task<IUser> TestUser(MembershipRepo membership, long testRunId)
        {
            var claims = new Claim[]
                         {
                             new Claim(ClaimTypes.Name, "TEST_USER"),
                             new Claim(ClaimTypes.Email, "TEST_USER@base2art.com"),
                             new Claim(ClaimTypes.Role, "NONE"),
                         };
            var claimsIdentity = new ClaimsIdentity(claims, "TESTING", ClaimTypes.Name, ClaimTypes.Role);
            var user = await membership.GetUser(new ClaimsPrincipal(claimsIdentity));
            return user;
        }

        private async Task DownloadFile(WebClient webClient, Guid spaceId, Guid channelId, IDirectoryProvider provider, string package, string version)
        {
            var basePath = Path.Combine(
                                        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                        "base2art",
                                        "cicd",
                                        "packages-node",
                                        "packages",
                                        spaceId.ToString("N"),
                                        channelId.ToString("N"));

            var di = Directory.CreateDirectory(basePath);
            var cacheDir = Directory.CreateDirectory(Path.Combine(di.Parent.Parent.Parent.FullName, ".cache"));
            var name = $"{package}.{version}.nupkg";

            var cachePath = Path.Combine(cacheDir.FullName, name);

            if (!File.Exists(cachePath))
            {
                // this.testOutputHelper.WriteLine(cachePath);
                await webClient.DownloadFileTaskAsync($"https://www.nuget.org/api/v2/package/{package}/{version}", cachePath);
            }

            var path = Path.Combine(provider.GetPackagesDir(spaceId, channelId).FullName, name);
            File.Copy(cachePath, path);
        }

        //
        // private async Task WriteKeyFile(WebClient webClient, IDirectoryProvider provider, string package, string version)
        // {
        //     throw new NotImplementedException();
        //     var basePath = Path.Combine(
        //                                 Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
        //                                 "base2art/cicd/packages-node-test");
        //     var di = Directory.CreateDirectory(basePath);
        //     var name = $"{package}.{version}.nupkg";
        //
        //     var cachePath = Path.Combine(di.FullName, name);
        //
        //     if (!File.Exists(cachePath))
        //     {
        //         await webClient.DownloadFileTaskAsync($"https://www.nuget.org/api/v2/package/{package}/{version}", cachePath);
        //     }
        //
        //     var path = Path.Combine(provider.GetPackagesDir(Guid.NewGuid(), Guid.NewGuid()).FullName, name);
        //     File.Copy(cachePath, path);
        // }
    }
}
//
//        [Theory]
//        [InlineData("conf.yaml")]
//        [InlineData("confV2.yaml")]
//        public async void ShoulGetSourceBranches(string confFile)
//        {
//            var configPath = this.Config(confFile);
//
//            using (var fixture = IntegrationTests.CreateFixture(configPath))
//            {
//                var client = fixture.Client;
//
//                var taskUrl = $"http://localhost/tasks/provision-db";
//                using (var stringContent = new StringContent("{}", Encoding.UTF8, "application/json"))
//                {
//                    var response = await client.PostAsync(taskUrl, stringContent);
//                    var responseString = await response.Content.ReadAsStringAsync();
//                    testOutputHelper.WriteLine(responseString);
//                }
//
//                var url = $"http://localhost/api/v1/source/branches";
//                var gitRepositoryData = new GitRepositoryData
//                                        {
//                                            CloneLocation = this.CloneLocation
//                                        };
//                using (var stringContent = new StringContent(ToJson(gitRepositoryData), Encoding.UTF8, "application/json"))
//                {
//                    var response = await client.PutAsync(url, stringContent);
//                    var responseString = await response.Content.ReadAsStringAsync();
//                    testOutputHelper.WriteLine(responseString);
//
//                    response.StatusCode.Should().Be(HttpStatusCode.OK);
//                    response.Content.Headers.ContentType.MediaType.Should().Be("application/json");
//
//                    var features = JsonConvert.DeserializeObject<List<IsolatedBranchData>>(responseString);
//                    features.Count.Should().Be(3);
//
//                    features[0].Name.Should().Be("feature/test-branch");
//                    features[0].Hash.Should().Be("a2e771cb4de18e736bfbdf465c82e8af2731fb6c");
//
//                    features[1].Name.Should().Be("feature/test-branch-2");
//                    features[1].Hash.Should().Be("2d931a4b9a62f23b6719fa83bd9f6af6bc1f3c1d");
//
//                    features[2].Name.Should().Be("master");
//                    features[2].Hash.Should().Be("a2e771cb4de18e736bfbdf465c82e8af2731fb6c");
//                }
//            }
//        }
//
//        [Theory]
//        [InlineData("conf.yaml", true)]
//        [InlineData("confV2.yaml", false)]
//        public async void ShoulGetSourceLogs(string confFile, bool legacy)
//        {
//            var configPath = this.Config(confFile);
//            using (var fixture = IntegrationTests.CreateFixture(configPath))
//            {
//                var client = fixture.Client;
//
//                var taskUrl = $"http://localhost/tasks/provision-db";
//                using (var stringContent = new StringContent("{}", Encoding.UTF8, "application/json"))
//                {
//                    var response = await client.PostAsync(taskUrl, stringContent);
//                    var responseString = await response.Content.ReadAsStringAsync();
//                    testOutputHelper.WriteLine(responseString);
//                }
//
//                var url = $"http://localhost/api/v1/source/logs";
//                var gitRepositoryData = new GitRepositoryBranchData
//                                        {
//                                            Repository = new GitRepositoryData
//                                                         {
//                                                             CloneLocation = this.CloneLocation
//                                                         },
//                                            Branch = new IsolatedBranchData
//                                                     {
//                                                         Hash = "2d931a4b9a62f23b6719fa83bd9f6af6bc1f3c1d",
//                                                         Name = "feature/test-branch-2"
//                                                     }
//                                        };
//                using (var stringContent = new StringContent(ToJson(gitRepositoryData), Encoding.UTF8, "application/json"))
//                {
//                    var response = await client.PutAsync(url, stringContent);
//                    var responseString = await response.Content.ReadAsStringAsync();
//                    testOutputHelper.WriteLine(responseString);
//
//                    response.StatusCode.Should().Be(HttpStatusCode.OK);
//                    response.Content.Headers.ContentType.MediaType.Should().Be("application/json");
//
//                    var features = JsonConvert.DeserializeObject<IsolatedBranchLogData>(responseString);
//                    features.Name.Should().Be("feature/test-branch-2");
//                    features.Hash.Should().Be("2d931a4b9a62f23b6719fa83bd9f6af6bc1f3c1d");
//                    features.Logs.Length.Should().Be(5);
//                    features.Logs[0].Id.Should().Be("2d931a4b9a62f23b6719fa83bd9f6af6bc1f3c1d");
//                    features.Logs[0].When.Should().Be(DateTime.Parse("2020-07-29T23:08:48"));
//                    features.Logs[0].Author.Should().StartWith("Scott ");
//                    features.Logs[0].Message.Should().StartWith("Update 00-clean.ps1");
//                    features.Logs[0].FilesAdded.Should().BeEmpty();
//                    features.Logs[0].FilesRemoved.Should().BeEmpty();
//                    features.Logs[0].FilesChanged.Should().HaveCount(1);
//
//                    features.Logs[2].Id.Should().Be("220b97db83207dbe4aedefe7c61971e8369bab3c");
//                    if (legacy)
//                    {
//                        features.Logs[2].When.Should().Be(DateTime.Parse("2018-12-13T12:43:40"));
//                    }
//                    else
//                    {
//                        features.Logs[2].When.Should().Be(DateTime.Parse("2018-12-13T20:43:40"));
//                    }
//
//                    features.Logs[2].Author.Should().StartWith("Leat ");
//                    features.Logs[2].Message.Should().StartWith("Adding in provision script");
//                    features.Logs[2].FilesAdded.Should().HaveCount(2);
//                    features.Logs[2].FilesRemoved.Should().BeEmpty();
//                    features.Logs[2].FilesChanged.Should().HaveCount(1);
//                }
//            }
//        }
//
//        [Theory]
//        [InlineData("conf.yaml", true)]
//        [InlineData("confV2.yaml", false)]
//        public async void ShouldGetSource(string confFile, bool legacy)
//        {
//            var configPath = this.Config(confFile);
//            using (var fixture = IntegrationTests.CreateFixture(configPath))
//            {
//                var client = fixture.Client;
//
//                var taskUrl = $"http://localhost/tasks/provision-db";
//                using (var stringContent = new StringContent("{}", Encoding.UTF8, "application/json"))
//                {
//                    var response = await client.PostAsync(taskUrl, stringContent);
//                    var responseString = await response.Content.ReadAsStringAsync();
//                    testOutputHelper.WriteLine(responseString);
//                }
//
//                var url = $"http://localhost/api/v1/checkout";
//                var gitRepositoryData = new GitCloneData
//                                        {
//                                            Repository = new GitRepositoryData
//                                                         {
//                                                             CloneLocation = this.CloneLocation
//                                                         },
//                                            BranchOrTag = "feature/test-branch-2"
//                                        };
//
//                Guid id;
//                IsolatedCheckoutState state = IsolatedCheckoutState.Unknown;
//
//                using (var stringContent = new StringContent(ToJson(gitRepositoryData), Encoding.UTF8, "application/json"))
//                {
//                    var response = await client.PutAsync(url, stringContent);
//                    var responseString = await response.Content.ReadAsStringAsync();
//                    response.StatusCode.Should().Be(HttpStatusCode.OK);
//                    response.Content.Headers.ContentType.MediaType.Should().Be("application/json");
//                    var checkoutData = JsonConvert.DeserializeObject<IsolatedCheckout>(responseString);
//                    id = checkoutData.Id;
//                    state = checkoutData.State;
//                    state.Should().Be(IsolatedCheckoutState.Pending);
//                    id.Should().NotBeEmpty();
//                }
//
//                while (!(state == IsolatedCheckoutState.CompletedFail || state == IsolatedCheckoutState.CompletedSuccess))
//                {
//                    var getStateUrl = $"http://localhost/api/v1/checkout/" + id.ToString("N");
//
//                    var response = await client.GetAsync(getStateUrl);
//                    var responseString = await response.Content.ReadAsStringAsync();
//                    testOutputHelper.WriteLine(responseString);
//                    response.StatusCode.Should().Be(HttpStatusCode.OK);
//                    response.Content.Headers.ContentType.MediaType.Should().Be("application/json");
//                    var checkoutData = JsonConvert.DeserializeObject<IsolatedCheckout>(responseString);
//                    id = checkoutData.Id;
//                    state = checkoutData.State;
//
////                    state.Should().Be(IsolatedCheckoutState.Working);
////                    id.Should().NotBeEmpty();
//
//                    await Task.Delay(TimeSpan.FromSeconds(4));
//                }
//
//                state.Should().Be(IsolatedCheckoutState.CompletedSuccess);
//
//                var sourceUrl = $"http://localhost/api/v1/checkout/{id:N}/source";
//                var responseSource = await client.GetAsync(sourceUrl);
//                var responseSourceString = await responseSource.Content.ReadAsStringAsync();
//                testOutputHelper.WriteLine(responseSourceString);
//                responseSource.StatusCode.Should().Be(HttpStatusCode.OK);
//                responseSource.Content.Headers.ContentType.MediaType.Should().Be("application/json");
//                var checkoutDataBytes = JsonConvert.DeserializeObject<byte[]>(responseSourceString);
//
//                checkoutDataBytes.Length.Should().Be(6003);
//
//                var deleteStateUrl = $"http://localhost/api/v1/checkout/" + id.ToString("N");
//                var deleteResponseSource = await client.DeleteAsync(deleteStateUrl);
//                var deleteResponseSourceString = await deleteResponseSource.Content.ReadAsStringAsync();
//                deleteResponseSource.StatusCode.Should().Be(HttpStatusCode.OK);
//                deleteResponseSourceString.Should().BeNullOrWhiteSpace();
//
////                var sourceUrl = $"http://localhost/api/v1/checkout/{id:N}/source";
//                responseSource = await client.GetAsync(sourceUrl);
//                responseSourceString = await responseSource.Content.ReadAsStringAsync();
////                testOutputHelper.WriteLine(responseSourceString);
//                if (legacy)
//                {
//                    responseSource.StatusCode.Should().Be(HttpStatusCode.OK);
//                    responseSource.Content.Headers.ContentType.MediaType.Should().Be("application/json");
//                    checkoutDataBytes = JsonConvert.DeserializeObject<byte[]>(responseSourceString);
//
//                    checkoutDataBytes.Length.Should().Be(22);
////                    responseSourceString.Length.Should().Be(22)
//                }
//                else
//                {
//                    responseSource.StatusCode.Should().Be(HttpStatusCode.NotFound);
//                    responseSource.Content.Headers.ContentType.MediaType.Should().Be("application/json");
////                checkoutDataBytes = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseSourceString);
//                }
//
////                testOutputHelper.WriteLine(responseSourceString);
////                checkoutDataBytes.Length.Should().Be(0);
//            }
//        }
//
//        public string CloneLocation
//        {
//            get
//            {
//                var path = Path.Combine(
//                                        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
//                                        "base2art",
//                                        "cicd",
//                                        "git-repo.url");
//
//                return File.ReadAllText(path).Trim();
//            }
//        }
//
//
//        private string ToJson(object gitRepositoryData)
//        {
//            return JsonConvert.SerializeObject(gitRepositoryData);
//        }
//
//        private T ConvertTo<T>(object value)
//        {
//            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(value));
//        }
//    }
//}