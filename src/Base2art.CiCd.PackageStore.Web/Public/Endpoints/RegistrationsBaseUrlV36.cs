namespace Base2art.CiCd.PackageStore.Web.Public.Endpoints
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Data;
    using Hashing;
    using NuGet.Packaging;
    using NuGet.Packaging.Core;
    using NuGet.Versioning;
    using Resources;

    public class RegistrationsBaseUrlV36
    {
        private readonly IRequireAuthProvider authentication;
        private readonly PackageRepo repo;
        private readonly IUrlProvider urlProvider;

        public RegistrationsBaseUrlV36(
            IUrlProvider urlProvider,
            PackageRepo repo,
            IRequireAuthProvider authentication,
            KeyRepo keys)
        {
            this.urlProvider = urlProvider;
            this.authentication = authentication;
            this.repo = repo;
        }

        // GET {@id}/{LOWER_ID}/index.json
        // GET https://api.nuget.org/v3/registration3/nuget.server.core/index.json
        public async Task<RegistrationsBaseUrlPackageResult> GetPackagePages(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            string packageName)
        {
            await this.authentication.Api().RequireAccessLevel(FileAccess.Read, spaceId, channelId, readPassword);
            await this.repo.TriggerScanForChanges(spaceId, channelId);
            packageName = packageName?.ToLowerInvariant();
            // await this.repo.Setup(spaceId, channelId);
            var items1 = await this.repo.GetVersionsForPackage(spaceId, channelId, packageName, true);

            var results = new RegistrationsBaseUrlPackageResult();

            results.SetInternalId($"{this.urlProvider.RegistrationIndex(spaceId, channelId, readPassword, packageName)}");
            results.SetType(
                            "catalog:CatalogRoot",
                            "PackageRegistration",
                            "catalog:Permalink");

            var itemsWrapper = items1?.Select(x => this.Map(spaceId, channelId, readPassword, DateTime.UtcNow, packageName, x))
                                     ?.ToArray()
                               ?? new Task<CategoryItem>[0];

            var items = await Task.WhenAll(itemsWrapper);
            if (items.Length == 0)
            {
                Console.WriteLine("No Items Found");
                return new RegistrationsBaseUrlPackageResult
                       {
                           items = new CategoryPageTemp[0],
                           count = 0
                       };
            }

            results.commitId = items.First().commitId;
            results.commitTimeStamp = items.First().commitTimeStamp;
            results.items = items.OrderByDescending(x => new NuGetVersion(x.catalogEntry.version))
                                 .Select((x, i) => (i / 64, x))
                                 .GroupBy(x => x.Item1)
                                 .Select(x => new CategoryPageTemp(
                                                                   this.urlProvider,
                                                                   spaceId,
                                                                   channelId,
                                                                   readPassword,
                                                                   x.Select(y => y.x).ToArray()))
                                 .ToArray();

            return results;
        }

        public async Task<RegistrationsBaseUrlPackagePageResult> GetPackagePage(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            string packageName,
            string lower,
            string upper)
        {
            await this.authentication.Api().RequireAccessLevel(FileAccess.Read, spaceId, channelId, readPassword);
            await this.repo.TriggerScanForChanges(spaceId, channelId);
            packageName = packageName?.ToLowerInvariant();
            var lowerVersion = new NuGetVersion(lower);
            var upperVersion = new NuGetVersion(upper);

            // await this.repo.Setup(spaceId, channelId);

            var results = new RegistrationsBaseUrlPackagePageResult();

            results.SetInternalId(this.urlProvider.RegistrationPage(spaceId, channelId, readPassword, packageName, lower, upper));

            results.SetType("catalog:CatalogPage");

            var versions = await this.repo.GetVersionsForPackage(spaceId, channelId, packageName, true);

            var items2 = await Task.WhenAll(versions.Select(x => this.Map(spaceId, channelId, readPassword, DateTime.UtcNow, packageName, x)));
            var eligibleItems = items2?.ToArray() ?? new CategoryItem[0];

            var items = eligibleItems.Select(x => (new NuGetVersion(x.catalogEntry.version), x))
                                     .Where(x => lowerVersion <= x.Item1 && x.Item1 <= upperVersion)
                                     .OrderByDescending(x => x.Item1)
                                     .Select(x => x.x)
                                     .ToArray();

            results.commitId = items.First().commitId;
            results.commitTimeStamp = items.First().commitTimeStamp;
            results.lower = lower;
            results.upper = upper;
            results.parent = this.urlProvider.RegistrationIndex(spaceId, channelId, readPassword, packageName);

            results.items = items.ToArray();

            return results;
        }

        public async Task<RegistrationsBaseUrlPackageWithVersionResult> GetPackage(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            string packageName,
            string packageVersion)
        {
            await this.authentication.Api().RequireAccessLevel(FileAccess.Read, spaceId, channelId, readPassword);
            await this.repo.ScanForChangesAsync(spaceId, channelId);
            packageName = packageName?.ToLowerInvariant();
            // var lowerVersion = new NuGetVersion(packageVersion);

            // await this.repo.Setup(spaceId, channelId);

            var results = new RegistrationsBaseUrlPackageWithVersionResult();

            // var dir = this.directoryProvider.GetWorkingDir(spaceId, channelId);

            // var packageDir = dir.GetDirectories("*", SearchOption.TopDirectoryOnly)
            //                     .FirstOrDefault(x => string.Equals(x.Name, packageName, StringComparison.OrdinalIgnoreCase));

            // var item = packageDir?.GetDirectories()
            //                      ?.FirstOrDefault(x => string.Equals(x.Name, packageVersion, StringComparison.OrdinalIgnoreCase));

            var time = DateTime.UtcNow;
            results.SetInternalId(this.urlProvider.RegistrationPackageWithVersion(spaceId, channelId, readPassword, packageName, packageVersion));
            results.SetType("Package", "http://schema.nuget.org/catalog#Permalink");

            results.catalogEntry = this.urlProvider.CatalogEntry(spaceId, channelId, readPassword, time, packageName, packageVersion);
            results.listed = true;
            results.packageContent = this.urlProvider.DownloadPackageLink(spaceId, channelId, readPassword, packageName, packageVersion);
            results.published = time.ToString("O");
            results.registration = this.urlProvider.RegistrationIndex(spaceId, channelId, readPassword, packageName);

            return results;
        }

        private async Task<CategoryItem> Map(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            DateTime dateTime,
            string packageName,
            string versionName)
        {
            var packageMetaPath = await this.repo.GetSpecFile(spaceId, channelId, packageName, versionName);
            // var packageMetaPath = Path.Combine(directoryInfo.FullName, "nuspec.xml");
            var packageMeta = new NuspecReader(packageMetaPath);

            var pi = packageMeta.GetIdentity();

            var categoryItem = new CategoryItem();

            categoryItem.SetInternalId(this.urlProvider.RegistrationPackageWithVersion(spaceId, channelId, readPassword, pi.Id, pi.Version.ToString()));
            categoryItem.Add("@type", "Package");
            categoryItem.commitId = versionName.HashAsGuid(); //.CreationTimeUtc.ToString("O").HashAsGuid();
            categoryItem.commitTimeStamp = dateTime.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffffffzzz"); // // directoryInfo.CreationTimeUtc
            categoryItem.packageContent = this.urlProvider.DownloadPackageLink(spaceId, channelId, readPassword, pi.Id, pi.Version.ToString());
            categoryItem.registration = this.urlProvider.RegistrationIndex(spaceId, channelId, readPassword, pi.Id);
            var entry = ProduceCategoryEntry(this.urlProvider, spaceId, channelId, readPassword, dateTime, pi, packageMeta, false);

            categoryItem.catalogEntry = entry;

            return categoryItem;
        }

        internal static CategoryEntry ProduceCategoryEntry(
            IUrlProvider urlProvider,
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            DateTime producedDate,
            PackageIdentity pi,
            NuspecReader packageMeta,
            bool includeContext)
        {
            var entry = new CategoryEntry(includeContext);

            entry.SetInternalId(urlProvider.CatalogEntry(spaceId, channelId, readPassword, producedDate, pi.Id, pi.Version.ToString()));
            entry.version = pi.Version.ToString();
            entry.id = pi.Id;
            entry.authors = packageMeta.GetAuthors();
            entry.description = packageMeta.GetDescription();
            entry.iconUrl = packageMeta.GetIconUrl();
            entry.language = packageMeta.GetLanguage();

            entry.licenseExpression = packageMeta.GetLicenseMetadata()?.LicenseExpression?.ToString() ?? "";
            entry.licenseUrl = packageMeta.GetLicenseUrl();
            entry.listed = true;
            entry.packageContent = urlProvider.DownloadPackageLink(spaceId, channelId, readPassword, pi.Id, pi.Version.ToString());
            entry.projectUrl = packageMeta.GetProjectUrl();
            entry.published = producedDate.ToString("O");
            entry.requireLicenseAcceptance = false;
            entry.summary = packageMeta.GetSummary();
            entry.title = packageMeta.GetTitle();
            var tagContent = packageMeta.GetTags();
            entry.tags = new string[0];
            if (!string.IsNullOrWhiteSpace(tagContent))
            {
                entry.tags = tagContent.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            }

            return entry;
        }
    }
}