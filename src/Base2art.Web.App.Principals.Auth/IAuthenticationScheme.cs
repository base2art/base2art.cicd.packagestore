namespace Base2art.Web.App.Principals.Auth
{
    public interface IAuthenticationScheme
    {
        string Name { get; }
        string DisplayName { get; }
    }
}