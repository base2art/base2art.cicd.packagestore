namespace Base2art.CiCd.PackageStore.Web.UI.ViewModels.Keys
{
    using System;
    using System.Collections.Generic;
    using Public.Resources;

    public class PickUrlViewModel
    {
        public Guid SpaceId { get; set; }
        public Guid ChannelId { get; set; }
        // public KeyValuePair<string, Guid>[] Pairs { get; set; }
        public Key[] Keys { get; set; }
        public Guid? KeyId { get; }
        public string Url { get; set; }
        public string KeyType { get; set; }
    }
}