namespace Base2art.Web.Flows
{
    using System.Security.Claims;
    using System.Threading.Tasks;

    public interface IAuthenticatedUserLookup
    {
        Task<IAuthenticatedUser> GetUser(ClaimsPrincipal principal);
    }
}