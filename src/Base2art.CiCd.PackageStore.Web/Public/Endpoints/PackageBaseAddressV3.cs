namespace Base2art.CiCd.PackageStore.Web.Public.Endpoints
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Data;
    using Resources;

    public class PackageBaseAddressV3
    {
        private readonly IRequireAuthProvider authentication;

        private readonly PackageRepo repo;

        public PackageBaseAddressV3(
            PackageRepo repo,
            IRequireAuthProvider authentication)
        {
            this.authentication = authentication;
            this.repo = repo;
        }

        public async Task<VersionModel> ListVersions(Guid spaceId, Guid channelId, Guid readPassword, string packageId)
        {
            await this.authentication.Api().RequireAccessLevel(FileAccess.Read, spaceId, channelId, readPassword);
            await this.repo.TriggerScanForChanges(spaceId, channelId);
            packageId = packageId?.ToLowerInvariant();
            var dir = await this.repo.GetVersionsForPackage(spaceId, channelId, packageId, false);

            return new VersionModel {Versions = dir ?? new string[0]};
        }

        public async Task<FileInfo> DownloadPackage(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            string packageId,
            string packageVersion,
            string packageName)
        {
            await this.authentication.Api().RequireAccessLevel(FileAccess.Read, spaceId, channelId, readPassword);
            await this.repo.ScanForChangesAsync(spaceId, channelId);
            packageId = packageId?.ToLowerInvariant();
            return await this.repo.GetPackageFile(spaceId, channelId, packageId, packageVersion);
        }

        public async Task<FileInfo> DownloadPackageSpec(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            string packageId,
            string packageVersion,
            string packageName)
        {
            await this.authentication.Api().RequireAccessLevel(FileAccess.Read, spaceId, channelId, readPassword);
            await this.repo.ScanForChangesAsync(spaceId, channelId);
            packageId = packageId?.ToLowerInvariant();
            return await this.repo.GetSpecFileInfo(spaceId, channelId, packageId, packageVersion);
        }
    }
}