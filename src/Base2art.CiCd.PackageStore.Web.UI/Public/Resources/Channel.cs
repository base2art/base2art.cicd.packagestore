namespace Base2art.CiCd.PackageStore.Web.UI.Public.Resources
{
    using System;

    public class Channel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}