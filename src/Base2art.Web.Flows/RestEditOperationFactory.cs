namespace Base2art.Web.Flows
{
    public class RestEditOperationFactory
    {
        private readonly ILayoutProvider layoutProvider;
        private readonly IAuthenticatedUserLookup authenticatedUserLookup;

        public RestEditOperationFactory(ILayoutProvider layoutProvider, IAuthenticatedUserLookup authenticatedUserLookup)
        {
            this.layoutProvider = layoutProvider;
            this.authenticatedUserLookup = authenticatedUserLookup;
        }

        public RestEditOperationBuilder<T> Create<T>() where T : class
        {
            return new RestEditOperationBuilder<T>(this.layoutProvider, this.authenticatedUserLookup);
        }
    }
}