namespace Base2art.CiCd.PackageStore.Web.UI.Validation
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Net.Mail;
    using System.Text.RegularExpressions;
    using FluentValidation;
    using FluentValidation.Internal;
    using FluentValidation.Validators;
    using Public.Resources;

    public class AdminConfigurationValidator : Base2art.Web.ObjectQualityManagement.ValidatorBase<AdminConfiguration>
    {
        private readonly bool update;

        public AdminConfigurationValidator(bool update)
        {
            this.update = update;
        }

        public Regex HostPattern { get; } = new Regex(@"^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,6}(\.[a-zA-Z]{2,6})?$");

        protected override void Configure()
        {
            this.Backing.RuleForEach(x => x.ManagementDomains).Matches(this.HostPattern);
            this.Backing.RuleForEach(x => x.ManagementAddresses).Must((x) => this.IsAddress(x));
        }

        private bool IsAddress(string s)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return true;
            }

            try
            {
                var mailAddress = new MailAddress(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}