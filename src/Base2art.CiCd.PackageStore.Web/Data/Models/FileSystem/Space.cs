namespace Base2art.CiCd.PackageStore.Web.Data.Models.FileSystem
{
    using System;
    using System.Net.Mail;
    using Models;

    internal class Space : ISpace
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid OwnerId { get; set; }
        // public string ReadGroupName { get; set; }
        // public string WriteGroupName { get; set; }
        // public string Host { get; set; }
        
        public MailAddress[] PushAddresses { get; set; }
        public string[] PushDomains { get; set; }
        public MailAddress[] ManagementAddresses { get; set; }
        public string[] ManagementDomains { get; set; }
    }
}