namespace Base2art.CiCd.PackageStore.Web.Data
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using NuGet.Packaging;

    public static class FileSystemScanner
    {
        private static XDocument XDocument(ZipArchiveEntry entry)
        {
            using (var stream = entry.Open())
            {
                var doc = System.Xml.Linq.XDocument.Load(stream);
                return doc;
            }
        }

        private static Dictionary<string, Task> scanner = new Dictionary<string, Task>();

        public static Task TriggerScanForChanges(this IDirectoryProvider directoryProvider, Guid spaceId, Guid channelId)
        {
            var key = $"{spaceId:N}:{channelId:N}";
            scanner.TryGetValue(key, out var current);
            if (current?.IsCompleted != false)
            {
                // Console.WriteLine($"Trigger for Scan... {spaceId:N}.{channelId:N}");
                scanner[key] = Setup(directoryProvider, spaceId, channelId);
            }

            return Task.CompletedTask;
        }

        public static async Task ScanForChangesAsync(this IDirectoryProvider directoryProvider, Guid spaceId, Guid channelId)
        {
            var key = $"{spaceId:N}:{channelId:N}";
            scanner.TryGetValue(key, out var current);
            if (current != null)
            {
                await current;
            }

            // Console.WriteLine($"Scanning... {spaceId:N}.{channelId:N}");
            current = Setup(directoryProvider, spaceId, channelId);
            scanner[key] = current;
            await current;
        }

        private static Task Setup(this IDirectoryProvider directoryProvider, Guid spaceId, Guid channelId)
        {
            var dir = directoryProvider.GetPackagesDir(spaceId, channelId);

            // Console.WriteLine($"Scanning Dir: {dir.FullName}");
            var files = dir.GetFiles("*.nupkg");

            if (files.Length == 0)
            {
                return Task.CompletedTask;
            }

            foreach (var fileInfo in files)
            {
                try
                {
                    var extractionDir = directoryProvider.GetPackageExtractionDir(spaceId);
                    var doc = ZipArchiveEntry(fileInfo, extractionDir);

                    if (doc != null)
                    {
                        var refPath = Directory.CreateDirectory(Path.Combine(
                                                                             dir.FullName,
                                                                             doc.GetId().ToLowerInvariant(),
                                                                             doc.GetVersion().ToString().ToLowerInvariant()));
                        File.WriteAllText(Path.Combine(refPath.FullName, "nuspec.ref"),
                                          Path.Combine(
                                                       "..",
                                                       "..",
                                                       "..",
                                                       ".packages",
                                                       doc.GetId().ToLowerInvariant(),
                                                       doc.GetVersion().ToString().ToLowerInvariant(),
                                                       "nuspec.xml"));

                        File.WriteAllText(Path.Combine(refPath.FullName, "nupkg.ref"),
                                          Path.Combine(
                                                       "..",
                                                       "..",
                                                       "..",
                                                       ".packages",
                                                       doc.GetId().ToLowerInvariant(),
                                                       doc.GetVersion().ToString().ToLowerInvariant(),
                                                       "content.nupkg"));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            return Task.CompletedTask;
        }

        private static NuspecReader ZipArchiveEntry(FileInfo fileInfo, DirectoryInfo extractionDir)
        {
            bool alreadyExists;
            NuspecReader doc;
            DirectoryInfo specDir;
            using (var archive = ZipFile.OpenRead(fileInfo.FullName))
            {
                var comp = StringComparison.OrdinalIgnoreCase;
                var entry = archive.Entries.FirstOrDefault(x => !x.FullName.Contains('/') && !x.FullName.Contains('\\')
                                                                                          && x.Name.EndsWith(".nuspec", comp));
                var docWrapper = XDocument(entry);
                doc = new NuspecReader(docWrapper);
                var path = Path.Combine(
                                        extractionDir.FullName,
                                        doc.GetId().ToLowerInvariant(),
                                        doc.GetVersion().ToString().ToLowerInvariant());

                specDir = Directory.CreateDirectory(path);
                var destinationFileName = Path.Combine(specDir.FullName, "nuspec.xml");
                if (!File.Exists(destinationFileName))
                {
                    entry.ExtractToFile(destinationFileName);
                    alreadyExists = false;
                }
                else
                {
                    alreadyExists = true;
                }
            }

            if (alreadyExists)
            {
                TryDelete(fileInfo);
                return doc;
            }
            else
            {
                fileInfo.MoveTo(Path.Combine(specDir.FullName, "content.nupkg"));
                return doc;
            }
        }

        public static void TryDelete(string path)
        {
            if (File.Exists(path))
            {
                try
                {
                    File.Delete(path);
                }
                catch
                {
                }
            }
        }

        private static void TryDelete(FileInfo path)
        {
            if (path?.Exists == true)
            {
                try
                {
                    path.Delete();
                }
                catch
                {
                }
            }
        }
    }
}