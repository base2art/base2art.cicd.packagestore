namespace Base2art.CiCd.PackageStore.Web.Public.Endpoints
{
    using System;

    public static class PackageServices
    {
        public static string RegistrationBase(this IUrlProvider provider, Guid spaceId, Guid channelId, Guid readPassword) =>
            $"{provider.GetBaseUrl(spaceId, channelId, readPassword)}/registration/";

        public static string RegistrationIndex(this IUrlProvider provider, Guid spaceId, Guid channelId, Guid readPassword, string packageName) =>
            $"{provider.RegistrationBase(spaceId, channelId, readPassword)}{packageName}/index.json";

        public static string RegistrationPage(
            this IUrlProvider provider,
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            string packageId,
            string lower,
            string upper) =>
            // // /{categoryItem.catalogEntry.id.ToLowerInvariant()}/page/{lower}/{upper}.json
            $"{provider.RegistrationBase(spaceId, channelId, readPassword)}{packageId.ToLowerInvariant()}/page/{lower}/{upper}.json";

        public static string RegistrationPackageWithVersion(
            this IUrlProvider urlProvider,
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            string packageName,
            string packageVersion) =>
            $"{urlProvider.RegistrationBase(spaceId, channelId, readPassword)}{packageName}/{packageVersion}.json";

        public static string DownloadPackageLink(
            this IUrlProvider urlProvider,
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            string packageDirName,
            string versionDirName) =>
            $"{urlProvider.GetBaseUrl(spaceId, channelId, readPassword)}/flatcontainer/{packageDirName}/{versionDirName}/{packageDirName}.{versionDirName}.nupkg";

        public static string CategoryBase(
            this IUrlProvider urlProvider,
            Guid spaceId,
            Guid channelId,
            Guid readPassword) =>
            $"{urlProvider.GetBaseUrl(spaceId, channelId, readPassword)}/catalog0";

        // return $"{urlProvider.GetBaseUrl(spaceId, channelId, readPassword)}/flatcontainer/{packageDirName}/{versionDirName}/{packageDirName}.{versionDirName}.nupkg";
        public static string CatalogEntry(
            this IUrlProvider urlProvider,
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            DateTime? created,
            string packageDirName,
            string versionDirName) =>
            $"{urlProvider.CategoryBase(spaceId, channelId, readPassword)}/data/{created:yyyy.MM.dd.HH.mm.ss}/{packageDirName}.{versionDirName}.json";

        // return $"{urlProvider.GetBaseUrl(spaceId, channelId, readPassword)}/flatcontainer/{packageDirName}/{versionDirName}/{packageDirName}.{versionDirName}.nupkg";
    }
}