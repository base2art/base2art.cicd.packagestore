﻿// namespace Base2art.Web.App.Principals.Auth.Configuration
// {
//     using App.Configuration;
//     using ComponentModel.Composition;
//     using Microsoft.Extensions.DependencyInjection;
//
//     public class MultiAuthenticationModule : BaseAuthenticationModule
//     {
//         public MultiAuthenticationModule(string defaultAuthenticationScheme)
//             => this.DefaultAuthenticationScheme = defaultAuthenticationScheme;
//
//         public override string DefaultAuthenticationScheme { get; }
//
//         public override void RegisterUserServices(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services, IServerConfiguration config)
//         {
//             base.RegisterUserServices(builder, services, config);
//
//             var authBuilder = this.GetAuthBuilder(builder);
//
//             var modules = services.ResolveAll<IAuthenticationModule>() ?? new IAuthenticationModule[0];
//
//             foreach (var module in modules)
//             {
//                 module.AddAuthentication(authBuilder);
//             }
//         }
//     }
// }