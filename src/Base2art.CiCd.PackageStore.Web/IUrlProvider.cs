namespace Base2art.CiCd.PackageStore.Web
{
    using System;

    public interface IUrlProvider
    {
        Uri GetBaseUrl();
        string GetBaseUrl(Guid spaceId, Guid channelId, Guid readKey);
    }
}