namespace Base2art.CiCd.PackageStore.Web.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Converters;
    using Data;
    using Resources;

    public class SearchQueryServiceVersionV35
    {
        private readonly IRequireAuthProvider authentication;
        private readonly PackageRepo repo;
        private readonly IUrlProvider urlProvider;

        public SearchQueryServiceVersionV35(
            IUrlProvider urlProvider,
            PackageRepo repo,
            IRequireAuthProvider authentication)
        {
            this.urlProvider = urlProvider;
            this.authentication = authentication;
            this.repo = repo;
        }

        // q=NuGet.Versioning&prerelease=false&semVerLevel=2.0.0
        public async Task<SearchResults> Search(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            string q, bool? prerelease = null)
        {
            await this.authentication.Api().RequireAccessLevel(FileAccess.Read, spaceId, channelId, readPassword);
            await this.repo.TriggerScanForChanges(spaceId, channelId);

            var results = new SearchResults();

            var matchingPackageDirs = await this.repo.Search(spaceId, channelId, q, prerelease.GetValueOrDefault());

            results.data = new List<Dictionary<string, object>>();

            var itemsToAdd = matchingPackageDirs.Select(async x => await SearchResultsMapper.Map(
                                                                                                 spaceId,
                                                                                                 channelId,
                                                                                                 readPassword,
                                                                                                 this.repo,
                                                                                                 this.urlProvider,
                                                                                                 x.Item1,
                                                                                                 x.Item2,
                                                                                                 prerelease.GetValueOrDefault()))
                                                .ToArray();
            results.data.AddRange(await Task.WhenAll(itemsToAdd));

            results.totalHits = results.data.Count;

            return results;
        }
    }
}