namespace Base2art.Web.App.Principals.Auth.ViewModels
{
    using System.Collections.Generic;
    using Public.Controllers;

    public class SignInViewModel
    {
        public string ReturnUrl { get; set; }
        public FailureMessage? Message { get; set; }
        public IEnumerable<IAuthenticationScheme> Schemes { get; set; }
        public string RequiredDomain { get; set; }
    }
}