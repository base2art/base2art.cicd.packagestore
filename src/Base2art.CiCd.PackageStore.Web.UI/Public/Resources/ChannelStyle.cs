namespace Base2art.CiCd.PackageStore.Web.UI.Public.Resources
{
    public enum ChannelStyle
    {
        SingleChannel,
        DevStageProd,
        PrivatePublic
    }
}