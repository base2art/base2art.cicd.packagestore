﻿namespace Base2art.Web.Flows
{
    using System;
    using System.Net.Mail;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Http;
    using Pages;
    using Utilities.Hashing;

    public class RestListOperationBuilder
    {
        private readonly ILayoutProvider layoutProvider;
        private readonly IAuthenticatedUserLookup authenticatedUserLookup;
        private IHttpRequest request;
        private ClaimsPrincipal principal;

        public RestListOperationBuilder(ILayoutProvider layoutProvider, IAuthenticatedUserLookup authenticatedUserLookup)
        {
            this.layoutProvider = layoutProvider;
            this.authenticatedUserLookup = authenticatedUserLookup;
        }

        public RestListOperationBuilder WithRequest(IHttpRequest value)
        {
            this.request = value;
            return this;
        }

        public RestListOperationBuilder WithUser(ClaimsPrincipal value)
        {
            this.principal = value;
            return this;
        }
        //
        // public async Task<WebPage> Build<T1>(Func<IWebPage<T1>> func, Func<IAuthenticatedUser, T[], Task<T1>> func1)
        // {
        //     var user = this.Wrap(this.principal);
        //     var result = await this.lookup(user);
        //     var body = WebPage.Create(func(), await func1(user, result));
        //     return await this.layoutProvider.BuildLayout(body: Task.FromResult(body),
        //                                                  // author:Auth
        //                                                  request: this.request,
        //                                                  user: this.principal);
        // }

        public async Task<WebPage> Build<T1>(Func<IWebPage<T1>> func, Func<IAuthenticatedUser, Task<T1>> func1)
        {
            var user = await this.authenticatedUserLookup.GetUser(this.principal);
            var body = WebPage.Create(func(), await func1(user));
            return await this.layoutProvider.BuildLayout(body: Task.FromResult(body),
                                                         // author:Auth
                                                         request: this.request,
                                                         user: this.principal);
        }
    }
}