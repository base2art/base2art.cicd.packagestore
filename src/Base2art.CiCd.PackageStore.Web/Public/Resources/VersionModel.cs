namespace Base2art.CiCd.PackageStore.Web.Public.Resources
{
    public class VersionModel
    {
        public string[] Versions { get; set; }
    }
}