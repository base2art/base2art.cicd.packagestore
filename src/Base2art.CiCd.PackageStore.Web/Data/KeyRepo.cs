namespace Base2art.CiCd.PackageStore.Web.Data
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Models;
    using Newtonsoft.Json;

    public class KeyRepo
    {
        private readonly IDirectoryProvider directoryProvider;

        public KeyRepo(IDirectoryProvider directoryProvider) => this.directoryProvider = directoryProvider;

        public Task<IKey[]> GetKeysByUser(Guid userId)
        {
            return Task.FromResult(this.EnumerateKeysByUser(userId).ToArray());
        }

        public Task<IKey> GetKeyByPassword(Guid readPassword)
        {
            var kreRefDir = this.directoryProvider.GetKeyRefDataDir();
            var path = Path.Combine(kreRefDir.FullName, readPassword.ToString("N") + ".json-key-ref");
            if (File.Exists(path))
            {
                var keyRef =  JsonConvert.DeserializeObject<KeyRef>(File.ReadAllText(path));
                return this.GetKeyByUser(keyRef.UserId, keyRef.KeyId);
            }

            return Task.FromResult<IKey>(null);
        }

        private Task<IKey> GetKeyByUser(Guid userId, Guid keyId)
        {
            var userDir = this.directoryProvider.GetUserDataDir(userId);
            var file = keyId.ToString("N") + ".json-key";

            var key = JsonConvert.DeserializeObject<MyKey>(File.ReadAllText(Path.Combine(userDir.FullName, file)));
            return Task.FromResult<IKey>(key);
        }

        private IEnumerable<IKey> EnumerateKeysByUser(Guid userId)
        {
            var userDir = this.directoryProvider.GetUserDataDir(userId);
            var files = userDir.EnumerateFiles("*.json-key", SearchOption.TopDirectoryOnly);

            return files.Select(file => File.ReadAllText(file.FullName))
                        .Select(JsonConvert.DeserializeObject<MyKey>);
        }

        public Task<IKey> AddKey(string keyName, DateTime keyExpires, bool keyCanRead, bool keyCanWrite, bool keyCanDelete, Guid userUserId)
        {
            var key = new MyKey
                      {
                          Name = keyName,
                          Id = Guid.NewGuid(),
                          Expires = keyExpires,
                          CanRead = keyCanRead,
                          CanWrite = keyCanWrite,
                          CanDelete = keyCanDelete,
                          UserId = userUserId,
                          Password = Guid.NewGuid()
                      };

            return this.SaveKey(userUserId, key);
        }

        public Task<IKey> DeleteKey(Guid keyId, Guid userId)
        {
            var userDir = this.directoryProvider.GetUserDataDir(userId);
            var path = Path.Combine(userDir.FullName, keyId.ToString("N") + ".json-key");

            var content = File.ReadAllText(path);
            var key = JsonConvert.DeserializeObject<MyKey>(content);
            key.CanRead = false;
            key.CanWrite = false;
            key.CanDelete = false;

            return this.SaveKey(userId, key);
        }

        private Task<IKey> SaveKey(Guid userUserId, MyKey key)
        {
            {
                var userDir = this.directoryProvider.GetUserDataDir(userUserId);
                var path = Path.Combine(userDir.FullName, key.Id.ToString("N") + ".json-key");
                File.WriteAllText(path, JsonConvert.SerializeObject(key));
            }
            {
                var kreRefDir = this.directoryProvider.GetKeyRefDataDir();
                var path = Path.Combine(kreRefDir.FullName, key.Password.ToString("N") + ".json-key-ref");
                File.WriteAllText(path, JsonConvert.SerializeObject(new KeyRef
                                                                    {
                                                                        UserId = userUserId,
                                                                        KeyId = key.Id
                                                                    }));
            }
            return Task.FromResult<IKey>(key);
        }

        private class KeyRef
        {
            public Guid UserId { get; set; }
            public Guid KeyId { get; set; }
        }

        private class MyKey : IKey
        {
            public string Name { get; set; }
            public Guid Id { get; set; }
            public DateTime Expires { get; set; }
            public bool CanRead { get; set; }
            public bool CanWrite { get; set; }
            public bool CanDelete { get; set; }
            public Guid UserId { get; set; }
            public Guid Password { get; set; }
        }
    }
}