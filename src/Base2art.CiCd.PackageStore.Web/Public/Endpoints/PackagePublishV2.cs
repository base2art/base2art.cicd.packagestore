namespace Base2art.CiCd.PackageStore.Web.Public.Endpoints
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Base2art.Web.FirstFileHandling;
    using Data;

    public class PackagePublishV2
    {
        private readonly IRequireAuthProvider authentication;

        private readonly PackageRepo repo;

        public PackagePublishV2(
            PackageRepo repo,
            IRequireAuthProvider authentication)
        {
            this.authentication = authentication;
            this.repo = repo;
        }

        public async Task PutPackage(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            IFirstFormFile file)
        {
            await this.authentication.Api().RequireAccessLevel(FileAccess.Write, spaceId, channelId, readPassword);
            using (var ms = new MemoryStream())
            {
                using (var openReadStream = await file.OpenReadStream())
                {
                    await openReadStream.CopyToAsync(ms);
                    await ms.FlushAsync();
                    ms.Seek(0L, SeekOrigin.Begin);
                    await this.repo.Add(spaceId, channelId, ms.ToArray());
                }
            }
            
            await this.repo.TriggerScanForChanges(spaceId, channelId);
        }

        public async Task DeletePackage(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            string packageName,
            string packageVersion)
        {
            await this.authentication.Api().RequireAccessLevel(FileAccess.Write, spaceId, channelId, readPassword);
            await this.repo.TriggerScanForChanges(spaceId, channelId);
            await this.repo.UnlistPackage(spaceId, channelId, packageName, packageVersion);
        }
    }
}