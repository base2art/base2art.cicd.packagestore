namespace Base2art.CiCd.PackageStore.Web
{
    using System.Linq;
    using System.Xml.Linq;

    public static class Nuspec
    {
        //     private static readonly XNamespace xNamespace = "http://schemas.microsoft.com/packaging/2013/05/nuspec.xsd";
        //
        //     public static string Id(this XDocument doc)
        //         => doc.GetEl("id");
        //
        //     public static string Version(this XDocument doc)
        //         => doc.GetEl("version");
        //
        //     public static string Title(this XDocument doc)
        //         => doc.GetEl("title");
        //
        //     public static string Description(this XDocument doc)
        //         => doc.GetEl("description");
        //
        //     public static string Summary(this XDocument doc)
        //         => doc.GetEl("summary");
        //
        //     public static string LicenseUrl(this XDocument doc)
        //         => doc.GetEl("licenseUrl");
        //
        //     public static string ProjectUrl(this XDocument doc)
        //         => doc.GetEl("projectUrl");
        //
        //     public static string Tags(this XDocument doc)
        //         => doc.GetEl("tags");

        public static string GetEl(this XDocument doc, string name)
        {
            var package = doc.Elements().First();

            var nsl = package.Name.Namespace;
            // var nsl = package; 
            var metadata = package.Element(nsl + "metadata");
            var id = metadata.Element(nsl + name)?.Value;
            return id;
        }
    }
}