namespace Base2art.CiCd.PackageStore.Web.UI.Converters
{
    using System.Linq;
    using System.Threading.Tasks;
    using Data;
    using Data.Models;
    using Public.Resources;

    public class AdminConfigurationConverter
    {
        private readonly IPersonLookup lookup;

        public AdminConfigurationConverter(IPersonLookup lookup)
        {
            this.lookup = lookup;
        }

        public async Task<AdminConfiguration> ConvertToModel(Task<IAdminConfiguration> space)
        {
            var spaceValue = await space;

            return new AdminConfiguration
                   {
                       Id = spaceValue.Id,
                       ManagementAddresses = spaceValue.ManagementAddresses?.Select(x => x?.ToString())?.ToArray() ?? new string[0],
                       ManagementDomains = spaceValue.ManagementDomains,
                   };
        }
    }
}