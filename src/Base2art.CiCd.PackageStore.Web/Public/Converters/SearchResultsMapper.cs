namespace Base2art.CiCd.PackageStore.Web.Public.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Data;
    using Endpoints;
    using NuGet.Packaging;
    using NuGet.Versioning;

    public static class SearchResultsMapper
    {
        public static async Task<Dictionary<string, object>> Map(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            PackageRepo repo,
            IUrlProvider urlProvider,
            string packageName,
            string[] versionNumbers,
            bool includePrerelease)
        {
            var includedVersionDirs = versionNumbers.Select(x => new NuGetVersion(x))
                                                    .Where(x => includePrerelease || !x.HasMetadata)
                                                    .OrderByDescending(x => x)
                                                    .ToArray();

            var items = includedVersionDirs.FirstOrDefault();

            if (items == null)
            {
                return null;
            }

            var doc = new NuspecReader(await repo.GetSpecFile(spaceId, channelId, packageName, items.ToString()));

            var dict = new Dictionary<string, object>();
            dict["@id"] = urlProvider.RegistrationIndex(spaceId, channelId, readPassword, packageName);
            dict["@type"] = "Package";
            dict["registration"] = urlProvider.RegistrationIndex(spaceId, channelId, readPassword, packageName);
            ;
            dict["id"] = doc.GetIdentity().Id;
            dict["version"] = doc.GetIdentity().Version;
            dict["description"] = doc.GetDescription();
            dict["summary"] = doc.GetSummary();
            dict["title"] = doc.GetTitle();
            dict["licenseUrl"] = doc.GetLicenseUrl();
            dict["projectUrl"] = doc.GetProjectUrl();
            dict["tags"] = doc.GetTags();
            dict["authors"] = doc.GetAuthors();
            dict["totalDownloads"] = 1;
            dict["verified"] = true;

            dict["versions"] = includedVersionDirs.Select(versionDir => new Dictionary<string, object>
                                                                        {
                                                                            {"version", versionDir.ToString()},
                                                                            {"downloads", 0},
                                                                            {
                                                                                "@id",
                                                                                urlProvider.RegistrationPackageWithVersion(
                                                                                 spaceId,
                                                                                 channelId,
                                                                                 readPassword,
                                                                                 packageName,
                                                                                 versionDir.ToString())
                                                                            }
                                                                        })
                                                  .ToArray();
            return dict;
        }

        // private static (Version, string) GetVersion(DirectoryInfo arg)
        // {
        //     var parts = arg.Name.Split(new[] {'-'}, StringSplitOptions.RemoveEmptyEntries);
        //     var suffix = parts.Length == 1 ? null : parts[1];
        //     if (Version.TryParse(parts[0], out var value))
        //     {
        //         return (value, suffix);
        //     }
        //     else
        //     {
        //         return (new Version(0, 0, 0, 0), suffix);
        //     }
        // }
    }
}