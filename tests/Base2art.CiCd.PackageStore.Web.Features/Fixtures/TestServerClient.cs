namespace Base2art.CiCd.PackageStore.Web.Fixtures
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using FluentAssertions;
    using Newtonsoft.Json;
    using TestServer;

    public class TestServerClient
    {
        private readonly Action<string> logger;

        public TestServerClient(Action<string> logger = null) => this.logger = logger;

        private void Log(string message)
        {
            this.logger?.Invoke(message);
        }

        public Task<T> SendRequestToGetJson<T>(HttpMethod method, string path)
        {
            return this.SendRequestAction(method, path, async response =>
            {
                try
                {
                    response.StatusCode.Should().Be(HttpStatusCode.OK);
                    response.Content.Headers.ContentType.MediaType.Should().Be("application/json");

                    if (typeof(T) == typeof(HttpResponseMessage))
                    {
                        return (T) (object) response;
                    }

                    var responseString = await response.Content.ReadAsStringAsync();
                    if (typeof(T) == typeof(string))
                    {
                        return (T) (object) responseString;
                    }

                    var obj = JsonConvert.DeserializeObject<T>(responseString);
                    return obj;
                }
                catch (Exception)
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    this.Log(responseString);
                    throw;
                }
            });
        }

        public Task<byte[]> SendRequestToBytes(HttpMethod method, string path)
        {
            return this.SendRequestAction(method, path, async response =>
            {
                if (response.StatusCode == HttpStatusCode.InternalServerError || response.Content.Headers.ContentType.MediaType != "application/octet-stream")
                {
                    var contentBody = await response.Content.ReadAsStringAsync();
                    this.Log(contentBody);
                }

                response.StatusCode.Should().Be(HttpStatusCode.OK);
                response.Content.Headers.ContentType.MediaType.Should().Be("application/octet-stream");

                return await response.Content.ReadAsByteArrayAsync();
            });
        }

        public Task<string> SendRequestToJson(HttpMethod method, string path)
        {
            return this.SendRequestAction(method, path, async response =>
            {
                response.StatusCode.Should().Be(HttpStatusCode.OK);
                response.Content.Headers.ContentType.MediaType.Should().Be("application/json");

                return await response.Content.ReadAsStringAsync();
                // return await XDocument.LoadAsync(await response.Content.ReadAsStreamAsync(), LoadOptions.None, CancellationToken.None);
            });
        }
        
        public Task<XDocument> SendRequestToXml(HttpMethod method, string path)
        {
            return this.SendRequestAction(method, path, async response =>
            {
                response.StatusCode.Should().Be(HttpStatusCode.OK);
                response.Content.Headers.ContentType.MediaType.Should().Be("text/xml");

                // return await response.Content.ReadAsStringAsync();
                return await XDocument.LoadAsync(await response.Content.ReadAsStreamAsync(), LoadOptions.None, CancellationToken.None);
            });
        }

        private async Task<T> SendRequestAction<T>(HttpMethod method, string path, Func<HttpResponseMessage, Task<T>> action)
        {
            var configPath = this.Config("conf.yaml");
            using (var fixture = IntegrationTests.CreateFixture(configPath))
            {
                var client = fixture.Client;

                if (!path.StartsWith('/'))
                {
                    path = $"/{path}";
                }

                var url = $"http://localhost{path}";
                var message = new HttpRequestMessage(method, url);
                var response = await client.SendAsync(message);
                return await action(response);
            }
        }

        private string Config(string confFile) => "/home/tyoung/code/b2a/base2art.cicd.packagestore.web/config/" + confFile;
    }
}