namespace Base2art.CiCd.PackageStore.Web.Public.Tasks
{
    using System;
    using System.Threading.Tasks;
    using Data;

    public class ReScanTask
    {
        private readonly PackageRepo packages;
        private readonly ChannelRepo channels;
        private readonly IDirectoryProvider dataStorage;

        public ReScanTask(PackageRepo packages, ChannelRepo channels, IDirectoryProvider dataStorage)
        {
            this.packages = packages;
            this.channels = channels;
            this.dataStorage = dataStorage;
        }

        public async Task<string> ExecuteAsync()
        {
            this.dataStorage.WriteDebuggingInfo(Console.Out);
            var spaces = await this.channels.GetSpaces();
            foreach (var space in spaces)
            {
                var spaceChannels = await this.channels.GetChannelsBySpace(space.Id);
                foreach (var channel in spaceChannels)
                {
                    await this.packages.TriggerScanForChanges(space.Id, channel.Id);   
                }
            }
            
            return "OK";
        }
    }
}