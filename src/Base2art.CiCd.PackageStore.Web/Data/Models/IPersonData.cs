namespace Base2art.CiCd.PackageStore.Web.Data.Models
{
    using System;

    public interface IPersonData
    {
        Guid Id { get;  }
        string Name { get;  }
    }
}