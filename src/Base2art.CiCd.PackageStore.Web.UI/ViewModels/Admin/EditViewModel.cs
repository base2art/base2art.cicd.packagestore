namespace Base2art.CiCd.PackageStore.Web.UI.ViewModels.Admin
{
    using Public.Resources;
    using Validation;

    public class EditViewModel
    {
        public AdminConfiguration Data { get; set; }
        
        public Base2art.Web.ObjectQualityManagement.IQualityManagementResult Errors { get; set; }

        public AdminConfigurationValidator DataValidation { get; set; }

        public OperationType OperationType { get; set; }
    }
}