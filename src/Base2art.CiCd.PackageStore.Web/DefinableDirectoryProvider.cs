namespace Base2art.CiCd.PackageStore.Web
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public class DefinableDirectoryProvider : IDirectoryProvider
    {
        private readonly DirectoryInfo packages;

        // private readonly DirectoryInfo membership;
        private readonly DirectoryInfo users;
        private readonly DirectoryInfo userdata;
        private readonly DirectoryInfo keyRefData;

        public DefinableDirectoryProvider(string path)
        {
            if (string.IsNullOrWhiteSpace(path) || path.StartsWith("#"))
            {
                path = Path.Combine(
                                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                    "base2art/cicd/packages-node");
            }

            // "/packages"

            this.packages = Directory.CreateDirectory(Path.Combine(path, "packages"));
            // Console.WriteLine(this.packages.FullName);
            // this.membership = Directory.CreateDirectory(Path.Combine(path, "membership"));
            this.users = Directory.CreateDirectory(Path.Combine(path, "users"));
            this.userdata = Directory.CreateDirectory(Path.Combine(path, "user-data"));
            this.keyRefData = Directory.CreateDirectory(Path.Combine(path, "key-ref"));
        }

        public string ToName(Guid spaceId)
            => spaceId.ToString("N");

        public DirectoryInfo GetUsersDir() => this.users;

        public DirectoryInfo GetChannelsDir(Guid spaceId)
            => Directory.CreateDirectory(Path.Combine(this.packages.FullName, this.ToName(spaceId)));

        public DirectoryInfo GetSpacesDir()
            => Directory.CreateDirectory(Path.Combine(this.packages.FullName));

        public DirectoryInfo GetPackagesDir(Guid spaceId, Guid channelId)
            => Directory.CreateDirectory(Path.Combine(this.packages.FullName, this.ToName(spaceId), this.ToName(channelId)));

        public DirectoryInfo GetPackageExtractionDir(Guid spaceId)
            => Directory.CreateDirectory(Path.Combine(this.packages.FullName, this.ToName(spaceId), ".packages"));

        public DirectoryInfo GetUserDataDir(Guid userId)
            => Directory.CreateDirectory(Path.Combine(this.userdata.FullName, this.ToName(userId)));

        public DirectoryInfo GetKeyRefDataDir()
            => this.keyRefData;

        public Task WriteDebuggingInfo(TextWriter writer)
            => writer.WriteLineAsync($"SpacesDir: `{this.GetSpacesDir()}`");

        // public DirectoryInfo GetMembershipDir(Guid spaceId, Guid channelId) =>
        //     Directory.CreateDirectory(Path.Combine(this.membership.FullName, this.ToName(spaceId), this.ToName(channelId)));
    }
}