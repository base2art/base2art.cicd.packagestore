namespace Base2art.Web.App.Principals.Auth.Configuration
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Exceptions;
    using Exceptions.Models;
    using Filters;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;

    public class SimpleMiddleware
    {
        private readonly RequestDelegate _next;

        public SimpleMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            if (next == null)
            {
                throw new ArgumentNullException(nameof(next));
            }

            if (loggerFactory == null)
            {
                throw new ArgumentNullException(nameof(loggerFactory));
            }

            this._next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await this._next(context);
            }
            catch (Exception oe)
            {
                if (!(oe.InnerException is RedirectException re))
                {
                    throw;
                }

                re.SuppressLogging();

                var httpStatusCode = this.GetStatusCode(re);

                var error = new
                            {
                                Message = this.Message(re),
                                Code = this.Code(re),
                                Fields = this.Errors(re)
                            };

                await context.Response.WriteAsync($"Redirecting your browser to the URL: {re.Location}");
                await context.Response.WriteAsync(Environment.NewLine);
                await context.Response.WriteAsync(JsonConvert.SerializeObject(error));
                await context.Response.WriteAsync(Environment.NewLine);
                context.Response.StatusCode = (int) httpStatusCode;
                context.Response.Headers["Location"] = this.GetLocation(re);
            }
        }

        private string GetLocation(RedirectException redirectException) => redirectException.Location ?? "/";

        protected virtual HttpStatusCode GetStatusCode(RedirectException redirectException)
        {
            if (redirectException.IsPermanent)
            {
                return redirectException.PreserveMethod ? (HttpStatusCode) (308) : HttpStatusCode.MovedPermanently;
            }

            return redirectException.PreserveMethod ? HttpStatusCode.TemporaryRedirect : HttpStatusCode.SeeOther;
        }

        protected virtual ErrorField[] Errors(RedirectException exception) => new[]
                                                                              {
                                                                                  new ErrorField
                                                                                  {
                                                                                      Code = "LOCATION",
                                                                                      Path = this.GetLocation(exception),
                                                                                      Message = "See Path"
                                                                                  }
                                                                              };

        protected virtual string Message(RedirectException exception) => exception.Message;

        protected virtual string Code(RedirectException exception)
        {
            var httpStatusCode = this.GetStatusCode(exception);
            switch (httpStatusCode)
            {
                case HttpStatusCode.MovedPermanently:
                    return "MOVED_PERMANENTLY";
                case (HttpStatusCode) (308):
                    return "PERMANENT_REDIRECT";
                case HttpStatusCode.TemporaryRedirect:
                    return "TEMPORARY_REDIRECT";
                case HttpStatusCode.SeeOther:
                    return "SEE_OTHER";
                default:
                    return httpStatusCode.ToString("G");
            }
        }
    }
}