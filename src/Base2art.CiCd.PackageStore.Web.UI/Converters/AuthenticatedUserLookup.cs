namespace Base2art.CiCd.PackageStore.Web.UI.Converters
{
    using System;
    using System.Net.Mail;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Exceptions;
    using Base2art.Web.Flows;
    using Data;

    public class AuthenticatedUserLookup : IAuthenticatedUserLookup
    {
        private readonly MembershipRepo users;

        public AuthenticatedUserLookup(MembershipRepo users)
        {
            this.users = users;
        }

        public async Task<IAuthenticatedUser> GetUser(ClaimsPrincipal principal)
        {
            if (principal?.Identity.IsAuthenticated != true)
            {
                throw new NotAuthenticatedException();
            }
            
            var user = await this.users.GetUser(principal);

            var id = principal.Identity.AuthenticationType + ":" + principal.Identity.Name;
            return new AuthenticatedUser
                   {
                       Id = id,
                       Email = new MailAddress(user.Email),
                       UserId = user.Id
                   };
        }

        private class AuthenticatedUser : IAuthenticatedUser
        {
            public string Id { get; set; }
            public MailAddress Email { get; set; }
            public Guid UserId { get; set; }
        }
    }
}