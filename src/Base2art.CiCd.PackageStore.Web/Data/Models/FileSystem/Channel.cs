namespace Base2art.CiCd.PackageStore.Web.Data.Models.FileSystem
{
    using System;
    using Models;

    internal class Channel : IChannel
    {
        public Guid Id { get; set; }
        public Guid SpaceId { get; set; }
        public string Name { get; set; }
    }
}