namespace Base2art.CiCd.PackageStore.Web
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public interface IRequireApiAuthProvider
    {
        /// <summary>
        /// Require specific level of Access.
        /// </summary>
        /// <param name="demand">For Flags Must have all.</param>
        /// <param name="spaceId"></param>
        /// <param name="channelId"></param>
        /// <param name="readPassword"></param>
        /// <returns></returns>
        Task RequireAccessLevel(
            FileAccess demand,
            Guid spaceId,
            Guid channelId,
            Guid readPassword);

        /// <summary>
        /// Query to determine specific level of Access.
        /// </summary>
        /// <param name="demand">For Flags Must have all.</param>
        /// <param name="spaceId"></param>
        /// <param name="channelId"></param>
        /// <param name="readPassword"></param>
        /// <returns></returns>
        Task<bool> CanPerformForAccessLevel(
            FileAccess demand,
            Guid spaceId,
            Guid channelId,
            Guid readPassword);
    }
}