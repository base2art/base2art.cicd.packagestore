namespace Base2art.CiCd.PackageStore.Web.Data.Models
{
    using System;
    using System.Net.Mail;

    public interface IAdminConfiguration
    {
        Guid Id { get; set; }
        MailAddress[] ManagementAddresses { get; }
        string[] ManagementDomains { get; }
    }
}