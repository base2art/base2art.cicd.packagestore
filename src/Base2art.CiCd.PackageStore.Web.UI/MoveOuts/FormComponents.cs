namespace Base2art.CiCd.PackageStore.Web.UI.MoveOuts
{
    using Base2art.Web.Razor.Forms.Generation;
    using Base2art.Web.Razor.Forms.ViewModels.Specialize;

    public static class FormComponents
    {
        public static MultiAddInputBuilder<T> MultiAddInput<T>(this IFormComponent form, T modelData)
        {
            return new MultiAddInputBuilder<T>(modelData);
        }
    }
}