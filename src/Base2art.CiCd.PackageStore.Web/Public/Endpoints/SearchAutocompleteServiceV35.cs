namespace Base2art.CiCd.PackageStore.Web.Public.Endpoints
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Data;
    using Resources;

    public class SearchAutocompleteServiceV35
    {
        private readonly IRequireAuthProvider authentication;
        private readonly PackageRepo repo;
        private readonly IUrlProvider urlProvider;

        public SearchAutocompleteServiceV35(
            IUrlProvider urlProvider,
            PackageRepo repo,
            IRequireAuthProvider authentication)
        {
            this.urlProvider = urlProvider;
            this.authentication = authentication;
            this.repo = repo;
        }

        // #GET {@id}?q={QUERY}&skip={SKIP}&take={TAKE}&prerelease={PRERELEASE}&semVerLevel={SEMVERLEVEL}&packageType={PACKAGETYPE}
        // #GET {@id}?id={ID}&prerelease={PRERELEASE}&semVerLevel={SEMVERLEVEL}
        public async Task<object> Search(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            string id = "",
            string q = "",
            int skip = 0,
            int take = 200,
            bool? prerelease = null)
        {
            await this.authentication.Api().RequireAccessLevel(FileAccess.Read, spaceId, channelId, readPassword);
            await this.repo.TriggerScanForChanges(spaceId, channelId);
            id = id?.ToLowerInvariant();

            if (!string.IsNullOrWhiteSpace(id))
            {
                return await this.SearchVersions(spaceId, channelId, readPassword, id, prerelease);
            }

            return await this.SearchPackages(spaceId, channelId, readPassword, q ?? string.Empty, skip, take, prerelease);
        }

        public async Task<AutoCompleteVersionsResult> SearchVersions(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            string id,
            bool? prerelease)
        {
            await this.authentication.Api().RequireAccessLevel(FileAccess.Read, spaceId, channelId, readPassword);
            await this.repo.TriggerScanForChanges(spaceId, channelId);

            var packageDir = await this.repo.GetVersionsForPackage(spaceId, channelId, id, prerelease.GetValueOrDefault());

            return new AutoCompleteVersionsResult
                   {
                       data = packageDir ?? new string[0]
                   };
        }

        // {@id}?q={QUERY}&skip={SKIP}&take={TAKE}&prerelease={PRERELEASE}&semVerLevel={SEMVERLEVEL}&packageType={PACKAGETYPE}
        public async Task<AutoCompleteResult> SearchPackages(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            string q,
            int skip,
            int take,
            bool? prerelease)
        {
            await this.authentication.Api().RequireAccessLevel(FileAccess.Read, spaceId, channelId, readPassword);
            await this.repo.TriggerScanForChanges(spaceId, channelId);
            var items = await this.repo.Search(spaceId, channelId, q, prerelease.GetValueOrDefault());

            return new AutoCompleteResult
                   {
                       totalHits = items.Length,
                       data = items.Select(x => x.Item1).Skip(skip).Take(take).ToArray()
                   };
        }
    }
}
/*
{
    "@context":{"@vocab":"http://schema.nuget.org/schema#"},
    "totalHits":205623,
    "data":[
        "Newtonsoft.Json",
        "Microsoft.Extensions.Logging",
        "Microsoft.Extensions.DependencyInjection",
        "Castle.Core",
        "Serilog",
        "Microsoft.EntityFrameworkCore",
        "Moq",
        "AutoMapper",
        "Microsoft.ApplicationInsights",
        "xunit",
        "EntityFramework",
        "Microsoft.IdentityModel.Clients.ActiveDirectory",
        "AWSSDK.Core",
        "Microsoft.AspNet.Mvc",
        "NUnit",
        "jQuery",
        "Microsoft.Bcl.AsyncInterfaces",
        "Azure.Storage.Blobs",
        "Swashbuckle.AspNetCore.Swagger",
        "Swashbuckle.AspNetCore.SwaggerGen"
    ]
}
             
             */