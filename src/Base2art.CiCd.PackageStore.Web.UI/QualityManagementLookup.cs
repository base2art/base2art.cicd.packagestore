namespace Base2art.CiCd.PackageStore.Web.UI
{
    using System.Collections.Generic;
    using System.Linq;
    using Base2art.Web.ObjectQualityManagement;

    internal class QualityManagementLookup<T1> : IQualityManagementLookup
    {
        private readonly List<IValidator<T1>> validators = new List<IValidator<T1>>();
        private readonly List<IVerifier<T1>> verifiers = new List<IVerifier<T1>>();

        public IEnumerable<IValidator<T1>> Validators => this.validators;
        public IEnumerable<IVerifier<T1>> Verifiers => this.verifiers;

        public QualityManagementLookup<T1> AddValidator(IValidator<T1> item)
        {
            this.validators.Add(item);
            return this;
        }

        public QualityManagementLookup<T1> AddVerifier(IVerifier<T1> item)
        {
            this.verifiers.Add(item);
            return this;
        }

        IEnumerable<IValidator<T>> IQualityManagementLookup.GetValidators<T>()
        {
            return this.validators.Select(x => (IValidator<T>) x);
        }

        IEnumerable<IVerifier<T>> IQualityManagementLookup.GetVerifiers<T>()
        {
            return this.verifiers.Select(x => (IVerifier<T>) x);
        }
    }
}