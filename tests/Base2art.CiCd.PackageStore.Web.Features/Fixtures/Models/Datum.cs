namespace Base2art.CiCd.PackageStore.Web.Fixtures.Models
{
    using System.Collections.Generic;
    using System.Text.Json.Serialization;

    public class Datum
    {
        public string registration { get; set; }
        public string id { get; set; }
        public string version { get; set; }
        public string description { get; set; }
        public string summary { get; set; }
        public string title { get; set; }
        public string licenseUrl { get; set; }
        public List<string> tags { get; set; }
        public List<string> authors { get; set; }
        public int totalDownloads { get; set; }
        public bool verified { get; set; }
        public List<PackageType> packageTypes { get; set; }
        public List<Version> versions { get; set; }

        [JsonPropertyName("@id")]
        public string at_id { get; set; }

        [JsonPropertyName("@type")]
        public string at_type { get; set; }

        public string projectUrl { get; set; }
    }
}