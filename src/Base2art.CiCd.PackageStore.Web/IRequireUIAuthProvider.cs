namespace Base2art.CiCd.PackageStore.Web
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;

    public interface IRequireUIAuthProvider
    {
        Task RequireActionManageAdmin(ClaimsPrincipal principal);
        
        Task RequireActionListSpaces(ClaimsPrincipal principal);
        Task RequireActionCreateSpace(ClaimsPrincipal principal);
        Task RequireActionEditSpace(ClaimsPrincipal principal, Guid spaceId);
        Task RequireActionDeleteSpace(ClaimsPrincipal principal, Guid spaceId);

        // Keys
        Task RequireActionListKeys(ClaimsPrincipal principal);
        Task RequireActionCreateKey(ClaimsPrincipal principal);
        Task RequireActionDeleteKey(ClaimsPrincipal principal, Guid keyId);

        // Channels
        Task RequireActionCreateChannel(ClaimsPrincipal principal, Guid spaceId);
        Task RequireActionEditChannel(ClaimsPrincipal principal, Guid spaceId, Guid channelId);

        // Task RequireActionList(ObjectType operationType, ClaimsPrincipal principal);
        // Task RequireActionView(ObjectType operationType, Guid objectId, ClaimsPrincipal principal);
        // Task RequireActionCreate(ObjectType operationType, ClaimsPrincipal principal);
        // Task RequireActionEdit(ObjectType operationType, Guid objectId, ClaimsPrincipal principal);
        // Task RequireActionDelete(ObjectType operationType, Guid objectId, ClaimsPrincipal principal);

        // Task RequireActionListChild(ObjectType operationType, Guid parentId, ClaimsPrincipal principal);
        // Task RequireActionViewChild(ObjectType operationType, Guid parentId, Guid objectId, ClaimsPrincipal principal);
        // Task RequireActionCreateChild(ObjectType operationType, Guid parentId, ClaimsPrincipal principal);
        // Task RequireActionEditChild(ObjectType operationType, Guid parentId, Guid objectId, ClaimsPrincipal principal);
        // Task RequireActionDeleteChild(ObjectType operationType, Guid parentId, Guid objectId, ClaimsPrincipal principal);
    }
}