namespace Base2art.CiCd.PackageStore.Web.UI.Public.Controllers
{
    using System;
    using System.Linq;
    using System.Net.Mail;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Flows;
    using Base2art.Web.Http;
    using Base2art.Web.ObjectQualityManagement;
    using Base2art.Web.Pages;
    using Converters;
    using Data;
    using Data.Models;
    using Pages;
    using Pages.Admin;
    using Resources;
    using Threading.Tasks;
    using Validation;
    using ViewModels.Admin;

    public class AdminController
    {
        private readonly ChannelRepo spaces;
        private readonly IRequireAuthProvider auth;
        private readonly RestCreateOperationFactory createApi;
        private readonly AdminConfigurationConverter adminConverter;

        public AdminController(MembershipRepo users, ChannelRepo spaces, IRequireAuthProvider auth, ILayoutProvider layout)
        {
            this.spaces = spaces;
            this.auth = auth;
            var userLookup = new AuthenticatedUserLookup(users);
            this.createApi = new RestCreateOperationFactory(layout, userLookup);
            this.adminConverter = new AdminConfigurationConverter(new PersonLookup(users.GetUser));
        }

        public async Task<WebPage> GetAddItem(ClaimsPrincipal principal, IHttpRequest request)
        {
            await this.auth.UI(isAdminManagement: true).Then().ExecuteAsync(async (x) => await x.RequireActionManageAdmin(principal));
            return await this.createApi.Create<IAdminConfiguration>()
                             .WithRequest(request)
                             .WithUser(principal)
                             .Build(() => new Edit(),
                                    async (user, objectId, errors) =>
                                    {
                                        var item = await this.adminConverter.ConvertToModel(this.spaces.GetAdminInformation());
                                        return new EditViewModel
                                               {
                                                   Data = item,
                                                   Errors = errors,
                                                   OperationType = OperationType.Updating,
                                                   DataValidation = new AdminConfigurationValidator(false)
                                               };
                                    });
        }

        public async Task<WebPage> AddItem(
            ClaimsPrincipal principal,
            IHttpRequest request,
            AdminConfiguration space)
        {
            await this.auth.UI(isAdminManagement: true).Then().ExecuteAsync(x => x.RequireActionManageAdmin(principal));
            return await this.createApi.Create<IAdminConfiguration>()
                             .WithRequest(request)
                             .WithValidation(() => this.ValidateItem(space))
                             .WithPersistence(x => x.Id, async (user) =>
                             {
                                 await this.spaces.SetAdminInformation(
                                                                       space.ManagementAddresses?.Select(x => TryGetMailAddress(x))?.ToArray(),
                                                                       space.ManagementDomains);

                                 var config = await this.spaces.GetAdminInformation();
                                 return config;
                             })
                             .WithUser(principal)
                             // .WithEditUrl(objectId => $"//{objectId}")
                             .Build(() => new Edit(), async (user, objectId, errors) =>
                                        new EditViewModel
                                        {
                                            Data = space,
                                            Errors = errors,
                                            OperationType = OperationType.Creating,
                                            DataValidation = new AdminConfigurationValidator(false)
                                        });
        }

        private MailAddress TryGetMailAddress(string p0)
        {
            try
            {
                return new MailAddress(p0);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private Task<IQualityManagementResult> ValidateItem(AdminConfiguration space, bool update = false)
        {
            IQualityManagementLookup lookup = new QualityManagementLookup<AdminConfiguration>().AddValidator(new AdminConfigurationValidator(update));

            return lookup.ValidateAndVerifyResult(space);
        }
    }
}