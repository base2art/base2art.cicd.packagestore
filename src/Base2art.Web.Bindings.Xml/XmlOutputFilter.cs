namespace Base2art.Web.Bindings.Xml
{
    using System.IO;
    using System.Xml;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;

    public class XmlOutputFilter : IActionFilter
    {
        public virtual void OnActionExecuting(ActionExecutingContext context)
        {
        }

        public virtual void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.HttpContext.Response == null)
            {
                return;
            }

            if (!(context.Result is ObjectResult oc))
            {
                return;
            }

            if (!(oc.Value is XmlDocument fi))
            {
                return;
            }

            var content = GetBytes(fi);

            context.Result = new ContentResult
                             {
                                 Content = content,
                                 ContentType = "text/xml",
                                 StatusCode = 200
                             };
        }

        private static string GetBytes(XmlDocument fi)
        {
            using (var sw = new StringWriter())
            {
                fi.Save(sw);
                sw.Flush();
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}