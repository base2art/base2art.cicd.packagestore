namespace Base2art.CiCd.PackageStore.Web.UI.ViewModels.Channels
{
    using System.Collections.Generic;
    using Data.Models;
    using Public.Resources;
    using Validation;

    public class EditViewModel
    {
        public (Channel, Space) Data { get; set; }

        public Base2art.Web.ObjectQualityManagement.IQualityManagementResult Errors { get; set; }

        public SpaceValidator DataValidation { get; set; }

        public OperationType OperationType { get; set; }
    }
}