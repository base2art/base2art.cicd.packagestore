$items = Get-childItem "src" |Where-Object {$_.PsIsContainer -eq $True} 

foreach($dir in $items) {

  if (Test-Path "src/$($dir.Name)/bin") {
    Remove-Item "src/$($dir.Name)/bin" -Recurse -Force
  }
  
  if (Test-Path "src/$($dir.Name)/obj") {
    Remove-Item "src/$($dir.Name)/obj" -Recurse -Force
  }
}


#echo $items
