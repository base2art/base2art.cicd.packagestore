namespace Base2art.Web.Flows
{
    using System;
    using System.Net.Mail;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Exceptions;
    using Http;
    using ObjectQualityManagement;
    using Pages;

    public class RestCreateOperationBuilder<T>
    {
        private readonly ILayoutProvider layoutProvider;
        private readonly IAuthenticatedUserLookup authenticatedUserLookup;
        private IHttpRequest request;
        private ClaimsPrincipal principal;
        private Func<Task<IQualityManagementResult>> validation;
        private Func<IAuthenticatedUser, Task<T>> persistence;
        private Func<Guid, string> urlProvider;
        private Func<T, Guid> idLookup;

        public RestCreateOperationBuilder(ILayoutProvider layoutProvider, IAuthenticatedUserLookup authenticatedUserLookup)
        {
            this.layoutProvider = layoutProvider;
            this.authenticatedUserLookup = authenticatedUserLookup;
        }

        public RestCreateOperationBuilder<T> WithRequest(IHttpRequest value)
        {
            this.request = value;
            return this;
        }

        public RestCreateOperationBuilder<T> WithUser(ClaimsPrincipal value)
        {
            this.principal = value;
            return this;
        }

        public RestCreateOperationBuilder<T> WithPersistence(
            Func<T, Guid> idLookupFunction,
            Func<IAuthenticatedUser, Task<T>> action)
        {
            this.idLookup = idLookupFunction;
            this.persistence = action;
            return this;
        }

        public RestCreateOperationBuilder<T> WithValidation(Func<Task<Base2art.Web.ObjectQualityManagement.IQualityManagementResult>> value)
        {
            this.validation = value;
            return this;
        }

        public RestCreateOperationBuilder<T> WithEditUrl(Func<Guid, string> func)
        {
            this.urlProvider = func;
            return this;
        }

        public async Task<WebPage> Build<T1>(
            Func<IWebPage<T1>> func,
            Func<IAuthenticatedUser, T, IQualityManagementResult, Task<T1>> func1)
        {
            var user = await this.authenticatedUserLookup.GetUser(this.principal);

            IQualityManagementResult validation1 = new QualityManagementResult();
            if (this?.validation != null)
            {
                var result = await this.validation.Invoke();
                validation1 = result;
            }

            var fromResult = this.persistence?.Invoke(user);
            
            if (fromResult == null)
            {
                fromResult = Task.FromResult(default(T));
            }

            var objectToPersist = await fromResult;

            if (objectToPersist != null && this.urlProvider != null)
            {
                var objectId = this.idLookup(objectToPersist);
                throw new RedirectException(this.urlProvider(objectId), false, false);
            }

            var body = WebPage.Create(func(), await func1(user, objectToPersist, validation1));
            return await this.layoutProvider.BuildLayout(body: Task.FromResult(body),
                                                         // author:Auth
                                                         request: this.request,
                                                         user: this.principal);
        }

        private class QualityManagementResult : IQualityManagementResult
        {
            public bool IsValid => this.Errors.Length == 0;
            public IQualityManagementError[] Errors { get; } = new IQualityManagementError[0];
        }
    }
}