namespace Base2art.CiCd.PackageStore.Web.Public.Resources
{
    using System;
    using System.Collections.Generic;

    public class RegistrationsBaseUrlPackageResult : Dictionary<string, object>
    {
        public RegistrationsBaseUrlPackageResult()
        {
            this.Add("@context", new Dictionary<string, object>
                                 {
                                     {"@vocab", "http://schema.nuget.org/schema#"},
                                     {"catalog", "http://schema.nuget.org/catalog#"},
                                     {"xsd", "http://www.w3.org/2001/XMLSchema#"},
                                     {
                                         "items",
                                         new Dictionary<string, string>
                                         {
                                             {"@id", "catalog:item"},
                                             {"@container", "@set"}
                                         }
                                     },
                                     {
                                         "commitTimeStamp",
                                         new Dictionary<string, string>
                                         {
                                             {"@id", "catalog:commitTimeStamp"},
                                             {"@type", "xsd:dateTime"}
                                         }
                                     },
                                     {
                                         "commitId",
                                         new Dictionary<string, string>
                                         {
                                             {"@id", "catalog:commitId"}
                                         }
                                     },
                                     {
                                         "count",
                                         new Dictionary<string, string>
                                         {
                                             {"@id", "catalog:count"}
                                         }
                                     },
                                     {
                                         "parent",
                                         new Dictionary<string, string>
                                         {
                                             {"@id", "catalog:parent"},
                                             {"@type", "@id"}
                                         }
                                     },
                                     {
                                         "tags",
                                         new Dictionary<string, string>
                                         {
                                             {"@id", "tag"},
                                             {"@container", "@set"}
                                         }
                                     },
                                     {
                                         "reasons",
                                         new Dictionary<string, string>
                                         {
                                             {"@container", "@set"}
                                         }
                                     },
                                     {
                                         "packageTargetFrameworks",
                                         new Dictionary<string, string>
                                         {
                                             {"@container", "@set"},
                                             {"@id", "@packageTargetFramework"}
                                         }
                                     },
                                     {
                                         "dependencyGroups",
                                         new Dictionary<string, string>
                                         {
                                             {"@container", "@set"},
                                             {"@id", "@dependencyGroup"}
                                         }
                                     },
                                     {
                                         "dependencies",
                                         new Dictionary<string, string>
                                         {
                                             {"@container", "@set"},
                                             {"@id", "@dependency"}
                                         }
                                     },
                                     {
                                         "packageContent",
                                         new Dictionary<string, string>
                                         {
                                             {"@type", "@id"}
                                         }
                                     },
                                     {
                                         "published",
                                         new Dictionary<string, string>
                                         {
                                             {"@type", "xsd:dateTime"}
                                         }
                                     },
                                     {
                                         "registration",
                                         new Dictionary<string, string>
                                         {
                                             {"@type", "@id"}
                                         }
                                     }
                                 });
        }

        public Guid commitId
        {
            get => (Guid) this[nameof(this.commitId)];
            set => this[nameof(this.commitId)] = value;
        }

        public string commitTimeStamp
        {
            get => (string) this[nameof(this.commitTimeStamp)];
            set => this[nameof(this.commitTimeStamp)] = value;
        }

        public int count
        {
            get => (int) this[nameof(this.count)];
            set => this[nameof(this.count)] = value;
        }

        public CategoryPageTemp[] items
        {
            get => (CategoryPageTemp[]) this[nameof(this.items)];
            set
            {
                this.count = value.Length;
                this[nameof(this.items)] = value;
            }
        }

        public void SetInternalId(string id)
        {
            this.Add("@id", id);
        }

        public void SetType(params string[] types)
        {
            this.Add("@type", types);
        }
    }
}