namespace Base2art.CiCd.PackageStore.Web.UI.Public.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Flows;
    using Base2art.Web.Http;
    using Base2art.Web.ObjectQualityManagement;
    using Base2art.Web.Pages;
    using Converters;
    using Data;
    using Data.Models;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Pages;
    using Pages.Keys;
    using Resources;
    using Threading.Tasks;
    using Validation;
    using ViewModels;
    using ViewModels.Keys;

    public class KeyController
    {
        private readonly IUrlProvider urlProvider;
        private readonly RestListOperationFactory listApi;
        private readonly RestCreateOperationFactory createApi;
        private readonly RestDeleteOperationFactory deleteApi;
        private readonly KeyConverter keyConverter;
        private readonly KeyRepo keys;
        private readonly IRequireAuthProvider auth;
        private readonly IAuthenticatedUserLookup userLookup;

        public KeyController(IUrlProvider urlProvider, KeyRepo keys, MembershipRepo users, IRequireAuthProvider auth, ILayoutProvider layout)
        {
            this.urlProvider = urlProvider;
            this.keys = keys;
            this.auth = auth;
            this.userLookup = new AuthenticatedUserLookup(users);
            this.listApi = new RestListOperationFactory(layout, this.userLookup);
            this.createApi = new RestCreateOperationFactory(layout, this.userLookup);
            this.deleteApi = new RestDeleteOperationFactory(this.userLookup);
            this.keyConverter = new KeyConverter(new PersonLookup(x => users.GetUser(x)));
        }

        public async Task<WebPage> PickUrl(ClaimsPrincipal principal, IHttpRequest request, Guid spaceId, Guid channelId)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionListKeys(principal));
            var user = await this.userLookup.GetUser(principal);

            var items = await this.keys.GetKeysByUser(user.UserId);
            var innerPage = WebPage.Create(new PickUrl(), new PickUrlViewModel
                                                          {
                                                              SpaceId = spaceId,
                                                              ChannelId = channelId,
                                                              Keys = items.Where(x => x.Expires > DateTime.UtcNow)
                                                                          .Select(x => this.keyConverter.ConvertToModel(x, true))
                                                                          .ToArray(),
                                                              Url = this.ResolveToBase(this.urlProvider.GetBaseUrl())
                                                          });

            return WebPage.Create(new Pages.NoChromeLayout(), new LayoutViewModel
                                                              {
                                                                  Body = Task.FromResult(innerPage)
                                                              });
        }

        public async Task<WebPage> List(ClaimsPrincipal principal, IHttpRequest request)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionListKeys(principal));
            return await this.listApi.Create()
                             .WithRequest(request)
                             .WithUser(principal)
                             .Build(
                                    () => new Index(),
                                    async delegate(IAuthenticatedUser user)
                                    {
                                        var spacesLocal = this.keys.GetKeysByUser(user.UserId);
                                        return new IndexViewModel
                                               {
                                                   Keys = await this.keyConverter.ConvertToModel(spacesLocal, false)
                                               };
                                    });
        }

        public async Task<WebPage> GetAddItem(ClaimsPrincipal principal, IHttpRequest request)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionCreateKey(principal));
            // await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionCreate(ObjectType.Key, principal);
            // await this.auth.UI().Then().ExecuteAsync(x => x.RequireAction(Operations.AddKey, principal);
            return await this.createApi.Create<IKey>()
                             .WithRequest(request)
                             .WithUser(principal)
                             .Build(() => new Edit(), async (user, objectId, errors) => new EditViewModel
                                                                                        {
                                                                                            Data = null,
                                                                                            Errors = errors,
                                                                                            OperationType = OperationType.Creating,
                                                                                            DataValidation = new SpaceValidator(false)
                                                                                        });
        }

        public async Task<WebPage> AddItem(
            ClaimsPrincipal principal,
            IHttpRequest request,
            Key key)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionCreateKey(principal));
            return await this.createApi.Create<IKey>()
                             .WithRequest(request)
                             .WithValidation(() => this.ValidateItem(key))
                             .WithPersistence(keyValue => keyValue.Id,
                                              user => this.keys.AddKey(key.Name, key.Expires, key.CanRead, key.CanWrite, true, user.UserId))
                             .WithUser(principal)
                             .WithEditUrl(objectId => $"/keys")
                             .Build(() => new Edit(), async (user, objectValue, errors) =>
                             {
                                 if (objectValue != null)
                                 {
                                     key = this.keyConverter.ConvertToModel(objectValue, true);
                                 }

                                 return new EditViewModel
                                        {
                                            Data = key,
                                            Errors = errors,
                                            OperationType = OperationType.Creating,
                                            DataValidation = new SpaceValidator(false)
                                        };
                             });
        }

        public async Task DeleteItem(
            ClaimsPrincipal principal,
            IHttpRequest request,
            Guid keyId)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionDeleteKey(principal, keyId));
            await this.deleteApi.Create()
                      .WithRequest(request)
                      // .WithValidation(() => this.ValidateItem(key))
                      .WithPersistence(user => this.keys.DeleteKey(keyId, user.UserId))
                      .WithUser(principal)
                      .WithEditUrl(objectId => $"/keys/edit/{objectId}")
                      .WithListUrl(() => $"/keys")
                      .Build(keyId);
        }

        private string ResolveToBase(Uri requestUri)
        {
            var builder = new UriBuilder(requestUri);
            builder.Path = "/";
            builder.Query = null;

            return builder.ToString();
        }

        private Task<IQualityManagementResult> ValidateItem(Key space, bool update = false)
        {
            IQualityManagementLookup lookup = new QualityManagementLookup<Key>().AddValidator(new KeyValidator());

            return lookup.ValidateAndVerifyResult(space);
        }
    }
}