namespace Base2art.CiCd.PackageStore.Web.Data
{
    using System;
    using System.IO;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Newtonsoft.Json;

    public class MembershipRepo
    {
        private readonly IDirectoryProvider directoryProvider;

        public MembershipRepo(IDirectoryProvider directoryProvider) => this.directoryProvider = directoryProvider;

        public Task<IUser> GetUser(ClaimsPrincipal principal)
        {
            var email = principal.FindFirst(ClaimTypes.Email)?.Value;
            var name = principal.FindFirst(ClaimTypes.Name)?.Value;

            return this.GetUser(principal.Identity.AuthenticationType, email, name);
        }

        internal async Task<IUser> GetUser(string authType, string email, string name)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new ArgumentNullException(nameof(email));
            }

            var aliasPath = Path.Combine(this.directoryProvider.GetUsersDir().FullName, authType);
            var pathLookup = Directory.CreateDirectory(aliasPath);

            var outputFile = Path.Combine(pathLookup.FullName, email);

            var id = Guid.NewGuid();
            if (!File.Exists(outputFile))
            {
                File.WriteAllText(outputFile, id.ToString("N"));

                var path = Path.Combine(this.directoryProvider.GetUsersDir().FullName, id.ToString("N"));
                var userData = new User
                               {
                                   Id = id,
                                   Name = name,
                                   Email = email,
                               };
                File.WriteAllText(path, JsonConvert.SerializeObject(userData));
            }
            else
            {
                id = Guid.Parse(File.ReadAllText(outputFile)?.Trim());
            }

            return await this.GetUser(id);
        }

        public async Task<IUser> GetUser(Guid id)
        {
            var path = Path.Combine(this.directoryProvider.GetUsersDir().FullName, id.ToString("N"));
            if (!File.Exists(path))
            {
                return null;
            }

            return JsonConvert.DeserializeObject<User>(File.ReadAllText(path));
        }

        private class User : IUser
        {
            public string Email { get; set; }
            public Guid Id { get; set; }
            public string Name { get; set; }
        }
    }

    public interface IUser
    {
        string Email { get; }
        Guid Id { get; }
        string Name { get; }
    }
}