namespace Base2art.CiCd.PackageStore.Web.Data.Models
{
    using System;

    public interface IChannel
    {
        Guid Id { get; }
        string Name { get; }
        Guid SpaceId { get; }
    }
}