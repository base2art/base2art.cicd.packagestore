namespace Base2art.CiCd.PackageStore.Web
{
    using System;
    using System.Linq;
    using System.Net.Mail;

    public static class RequireAuthProviders
    {
        public static bool IsAllowed(string[] allowedAddresses, string[] allowedDomains, MailAddress address)
        {
            allowedAddresses = allowedAddresses ?? new string[0];
            allowedDomains = allowedDomains ?? new string[0];

            if (allowedAddresses.Length + allowedDomains.Length == 0)
            {
                return true;
            }

            if (allowedDomains.Any(y => IsMatch(y, address)))
            {
                return true;
            }

            return allowedAddresses.Any(y => IsMatchAddress(y, address));
        }

        public static bool IsAllowed(MailAddress[] allowedAddresses, string[] allowedDomains, MailAddress address)
        {
            allowedAddresses = allowedAddresses ?? new MailAddress[0];
            allowedDomains = allowedDomains ?? new string[0];

            return IsAllowed(allowedAddresses.Select(x => x?.Address).ToArray(), allowedDomains, address);
        }

        private static bool IsMatch(string allowedAddress, MailAddress userAddress)
        {
            if (string.Equals(userAddress.Host, allowedAddress.TrimStart('@'), StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }

        private static bool IsMatchAddress(string allowedAddress, MailAddress userAddress)
        {
            if (string.Equals(allowedAddress ?? string.Empty, userAddress.Address, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }
    }
}

// internal static MailAddress MakeAddress(string arg)
// {
//     try
//     {
//         return new MailAddress(arg);
//     }
//     catch
//     {
//     }
//
//     try
//     {
//         return new MailAddress(PrefixUser + (arg.Contains("@") ? arg : $"@{arg}"));
//     }
//     catch
//     {
//     }
//
//     return null;
// }
// private const string PrefixUser = "FD05E7527ECE44E3985F290EFE8A832D";

// public static bool IsAllowed(IEnumerable<MailAddress> allowedAddresses, MailAddress address)
// {
//     return allowedAddresses.Any(y => IsMatch(y, address));
// }