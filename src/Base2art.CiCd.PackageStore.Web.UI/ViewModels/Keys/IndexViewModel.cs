namespace Base2art.CiCd.PackageStore.Web.UI.ViewModels.Keys
{
    using Data;
    using Data.Models;
    using Public.Resources;

    public class IndexViewModel
    {
        public Key[] Keys { get; set; }
        // public IChannel[] Channels { get; set; }
    }
}