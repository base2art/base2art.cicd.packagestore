namespace Base2art.Web.App.Principals.Auth.ViewModels
{
    using System.Threading.Tasks;
    using Web.Pages;

    public class LayoutViewModel
    {
        // public string Title { get; set; }
        // public string Author { get; set; }
        public Task<WebPage> Body { get; set; }
        public bool SmallBody { get; set; }
    }
}