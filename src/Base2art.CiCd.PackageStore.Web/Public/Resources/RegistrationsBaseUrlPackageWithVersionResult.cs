namespace Base2art.CiCd.PackageStore.Web.Public.Resources
{
    using System.Collections.Generic;

    public class RegistrationsBaseUrlPackageWithVersionResult : Dictionary<string, object>
    {
        public RegistrationsBaseUrlPackageWithVersionResult()
        {
            this.Add("@context", new Dictionary<string, object>
                                 {
                                     {"@vocab", "http://schema.nuget.org/schema#"},
                                     {"xsd", "http://www.w3.org/2001/XMLSchema#"},
                                     {
                                         "catalogEntry",
                                         new Dictionary<string, string>
                                         {
                                             {"@type", "@id"}
                                         }
                                     },
                                     {
                                         "registration",
                                         new Dictionary<string, string>
                                         {
                                             {"@type", "@id"}
                                         }
                                     },
                                     {
                                         "packageContent",
                                         new Dictionary<string, string>
                                         {
                                             {"@type", "@id"}
                                         }
                                     },
                                     {
                                         "published",
                                         new Dictionary<string, string>
                                         {
                                             {"@type", "xsd:dateTime"}
                                         }
                                     }
                                 });
        }

        public string catalogEntry
        {
            get => (string) this[nameof(this.catalogEntry)];
            set => this[nameof(this.catalogEntry)] = value;
        }

        public string published
        {
            get => (string) this[nameof(this.published)];
            set => this[nameof(this.published)] = value;
        }

        public string registration
        {
            get => (string) this[nameof(this.registration)];
            set => this[nameof(this.registration)] = value;
        }

        public string packageContent
        {
            get => (string) this[nameof(this.packageContent)];
            set => this[nameof(this.packageContent)] = value;
        }

        public bool listed
        {
            get => (bool) this[nameof(this.listed)];
            set => this[nameof(this.listed)] = value;
        }

        public void SetInternalId(string id)
        {
            this.Add("@id", id);
        }

        public void SetType(params string[] types)
        {
            this.Add("@type", types);
        }
    }
}