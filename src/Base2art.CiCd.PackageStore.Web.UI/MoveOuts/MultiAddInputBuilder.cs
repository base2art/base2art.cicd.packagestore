namespace Base2art.CiCd.PackageStore.Web.UI.MoveOuts
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Text.RegularExpressions;
    using Base2art.Web.Pages;
    using Base2art.Web.Razor.Forms.Generation;
    using Base2art.Web.Razor.Forms.Generation.PropertyBuilders;

    public class MultiAddInputBuilder<T> : BasicControlBuilderBase<T, MultiAddInputModel>
    {
        private readonly ForBuilder<MultiAddInputBuilder<T>, T> fors;

        public MultiAddInputBuilder(T value) : base(value)
        {
            this.fors = new ForBuilder<MultiAddInputBuilder<T>, T>(this, this.Model);
            //}this.WrapMany(new ForBuilder<MultiAddInputBuilder<T>, T>(this, this.Model));
        }

        public MultiAddInputBuilder<T> WithType(string type)
        {
            this.Model.Type = type;
            return this;
        }

        public MultiAddInputBuilder<T, TProp> For<TProp>(Expression<Func<T, TProp[]>> lookup, Func<TProp, string> map)
            where TProp : class
            => this.WrapMany(this.fors.For(lookup, map));

        public MultiAddInputBuilder<T, Guid> For(Expression<Func<T, Guid[]>> lookup)
            => this.WrapMany(this.fors.For(lookup, x => x.ToString("D")));

        public MultiAddInputBuilder<T, Guid?> For(Expression<Func<T, Guid?[]>> lookup)
            => this.WrapMany(this.fors.For(lookup, x => x?.ToString("D")));

        // public MultiAddInputBuilder<T, MailAddress> For(Expression<Func<T, MailAddress[]>> lookup)
        //     => this.WrapMany(this.fors.For(lookup, (x) => x.ToString()));

        public MultiAddInputBuilder<T, string> For(Expression<Func<T, string[]>> lookup)
            => this.WrapMany(this.fors.For(lookup, x => x));

        
        
        // public MultiAddInputBuilder<T, string> For(Expression<Func<T, string[]>> lookup)
        //     => this.WrapMany<string>(this.fors.For(lookup), null);

        // public MultiAddInputBuilder<T, Uri> For(Expression<Func<T, Uri[]>> lookup)
        //     => this.WrapMany(this.fors.For(lookup, x => x.ToString()));

        // public MultiAddInputBuilder<T, Uri> For(Expression<Func<T, Uri[]>> lookup)
        //     => this.WrapMany(this.fors.For(lookup, x => x.ToString()));

        // public MultiAddInputBuilder<T, DateTime?> ForDate(Expression<Func<T, DateTime?>> lookup)
        //     => this.Wrap(this.fors.ForDate(lookup));

        // public MultiAddInputBuilder<T, DateTimeOffset?> ForLocalizedDateTime(Expression<Func<T, DateTimeOffset?>> lookup)
        //     => this.Wrap(this.fors.ForLocalizedDateTime(lookup));

        // public MultiAddInputBuilder<T, TimeSpan?> ForTime(Expression<Func<T, TimeSpan?>> lookup)
        //     => this.Wrap(this.fors.ForTime(lookup));

        // public MultiAddInputBuilder<T, Guid?[]> For(Expression<Func<T, Guid?[]>> lookup)
        //     => this.WrapMany(this.fors.For(lookup, x => x.HasValue ? x.Value.ToString("D") : ""));
        // //
        // public MultiAddInputBuilder<T, TEnum[]> For<TEnum>(Expression<Func<T, TEnum[]>> lookup)
        //     where TEnum : Enum => this.WrapMany(this.fors.For(lookup));

        // public MultiAddInputBuilder<T, IDictionary<string, string>> ForJson(Expression<Func<T, IDictionary<string, string>>> lookup)
        //     => this.Wrap(this.fors.ForJson(lookup));

        // public MultiChooserInputBuilder<T, Guid> For(Expression<Func<T, Guid[]>> lookup)
        //     => this.WrapMany(this.fors.For(lookup, prop => prop.ToString("N")));

        private MultiAddInputBuilder<T, TOther> WrapMany<TOther>(
            (MultiAddInputBuilder<T>, Func<T, TOther[]>, Func<TOther, string>) @for,
            KeyValuePair<string, string>[] options = null)
        {
            var builder = new MultiAddInputBuilder<T, TOther>(new MultiAddInputModel(),
                                                              this.Value,
                                                              this.fors.Expression,
                                                              @for.Item2,
                                                              @for.Item3,
                                                              this.Model.Type);
            // builder.WithOptions(options);
            return builder;
        }

        // private MultiAddInputBuilder<T, TOther> Wrap<TOther>((MultiAddInputBuilder<T>, Func<T, TOther>, Func<TOther, string>) @for)
        // {
        //     var builder = new MultiAddInputBuilder<T, TOther>(
        //                                                   new TextInputModel {Type = this.Model.Type},
        //                                                   this.Value,
        //                                                   this.fors.Expression,
        //                                                   @for.Item2,
        //                                                   null,
        //                                                   @for.Item3);
        //
        //     return builder;
        // }
    }

    public class MultiAddInputBuilder<T, TProp> : HtmlControlBuilderBase<T, MultiAddInputModel>
    {
        private readonly LabelBuilder<MultiAddInputBuilder<T, TProp>, T> label;
        private readonly ValuesBuilder<MultiAddInputBuilder<T, TProp>, T, TProp> value;
        private readonly ReadOnlyBuilder<MultiAddInputBuilder<T, TProp>> readOnly;

        // private readonly RequiredBuilder<MultiAddInputBuilder<T, TProp>> required;

        // private readonly TextAreaRowsBuilder<MultiAddInputBuilder<T, TProp>> rows;
        // private readonly PlaceholderBuilder<MultiAddInputBuilder<T, TProp>> placeholder;
        private readonly PatternBuilder<MultiAddInputBuilder<T, TProp>> pattern;
        private readonly NameAndIdBuilder<MultiAddInputBuilder<T, TProp>, T> name;

        public MultiAddInputBuilder(MultiAddInputModel model,
                                    T value,
                                    Expression expression,
                                    Func<T, TProp[]> lookups,
                                    Func<TProp, string> stringifier, 
                                    string modelType)
            : base(model, value)
        {
            this.value = new ValuesBuilder<MultiAddInputBuilder<T, TProp>, T, TProp>(this, this.Model, lookups, stringifier);
            this.label = new LabelBuilder<MultiAddInputBuilder<T, TProp>, T>(this, this.Model, expression);
            this.name = new NameAndIdBuilder<MultiAddInputBuilder<T, TProp>, T>(this, this.Model, expression);

            this.readOnly = new ReadOnlyBuilder<MultiAddInputBuilder<T, TProp>>(this, this.Model);
            // this.required = new RequiredBuilder<MultiAddInputBuilder<T, TProp>>(this, this.Model);
            // this.rows = new TextAreaRowsBuilder<MultiAddInputBuilder<T, TProp>>(this, this.Model);
            // this.placeholder = new PlaceholderBuilder<MultiAddInputBuilder<T, TProp>>(this, this.Model);
            this.pattern = new PatternBuilder<MultiAddInputBuilder<T, TProp>>(this, this.Model);
            this.Model.Type = modelType;
        }

        public MultiAddInputBuilder<T, TProp> WithValues(TProp[] data) => this.value.WithValues(data);

        // public MultiAddInputBuilder<T, TProp> WithDefaultValue(TProp data) => this.value.WithDefaultValue(data);

        public MultiAddInputBuilder<T, TProp> ReadOnly(bool isReadonly = true) => this.readOnly.Readonly(isReadonly);

        public MultiAddInputBuilder<T, TProp> WithLabel(string labelValue) => this.label.WithLabel(labelValue);

        // public MultiAddInputBuilder<T, TProp> Required(bool isRequired = true) => this.required.Required(isRequired);

        public MultiAddInputBuilder<T, TProp> WithPattern(Regex patternRegex) => this.pattern.WithPattern(patternRegex);

        // public MultiAddInputBuilder<T, TProp> Regex(bool isRequired = true) => this.required.Required(isRequired);

        // public MultiAddInputBuilder<T, TProp> MultiLine(int rowCount = 2) => this.rows.MultiLine(rowCount);

        // public MultiAddInputBuilder<T, TProp> WithPlaceholder(string placeholderValue) => this.placeholder.WithPlaceholder(placeholderValue);

        protected override WebPage BuildControl() =>
            WebPage.Create(new MultiAdd(), this.Model);
    }
}