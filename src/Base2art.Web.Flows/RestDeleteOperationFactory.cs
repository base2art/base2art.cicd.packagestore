namespace Base2art.Web.Flows
{
    public class RestDeleteOperationFactory
    {
        private readonly IAuthenticatedUserLookup authenticatedUserLookup;

        public RestDeleteOperationFactory(IAuthenticatedUserLookup authenticatedUserLookup)
        {
            this.authenticatedUserLookup = authenticatedUserLookup;
        }

        public RestDeleteOperationBuilder Create()
        {
            return new RestDeleteOperationBuilder(this.authenticatedUserLookup);
        }
    }
}