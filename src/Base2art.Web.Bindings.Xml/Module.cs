﻿namespace Base2art.Web.Bindings.Xml
{
    using System;
    using System.Collections.Generic;
    using App.Configuration;
    using Microsoft.AspNetCore.Builder;
    using Server.Registration;

    public class Module : RegistrationBase, IModule
    {
        public IReadOnlyList<IEndpointConfiguration> Endpoints { get; } = new List<IEndpointConfiguration>();
        public IReadOnlyList<ITypeInstanceConfiguration> GlobalEndpointAspects { get; } = new List<ITypeInstanceConfiguration>();
        public IReadOnlyList<ITaskConfiguration> Tasks { get; } = new List<ITaskConfiguration>();
        public IReadOnlyList<ITypeInstanceConfiguration> GlobalTaskAspects { get; } = new List<ITypeInstanceConfiguration>();
        public IHealthChecksConfiguration HealthChecks { get; } = new MyHc();

        public IApplicationConfiguration Application { get; } = new MyApp();

        public IReadOnlyList<IInjectionItemConfiguration> Injection { get; } = new List<IInjectionItemConfiguration>();
        public IReadOnlyList<ICorsItemConfiguration> Cors { get; } = new List<ICorsItemConfiguration>();

        public override void RegisterNativeMiddleware(IApplicationBuilder app, IServerConfiguration config)
        {
            base.RegisterNativeMiddleware(app, config);
            app.Use(async (context, next) =>
            {
                // var cultureQuery = context.Request.Query["culture"];
                // if (!string.IsNullOrWhiteSpace(cultureQuery))
                // {
                // var culture = new CultureInfo(cultureQuery);

                // CultureInfo.CurrentCulture = culture;
                // CultureInfo.CurrentUICulture = culture;
                // }

                Console.WriteLine(context.Request.Path.ToString());

                // Call the next delegate/middleware in the pipeline
                try
                {
                    await next();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Console.WriteLine(context.Request.Path.ToString());
                }
            });
        }

        private class MyHc : IHealthChecksConfiguration
        {
            public IReadOnlyList<IHealthCheckConfiguration> Items { get; } = new List<IHealthCheckConfiguration>();
            public IReadOnlyList<ITypeInstanceConfiguration> Aspects { get; } = new List<ITypeInstanceConfiguration>();
        }

        private class MyApp : IApplicationConfiguration
        {
            public IReadOnlyList<ITypeInstanceConfiguration> Filters { get; } = new List<ITypeInstanceConfiguration>
                                                                                {
                                                                                    new TypeInstanceConfiguration
                                                                                    {
                                                                                        Type = typeof(XmlOutputFilter),
                                                                                        Parameters = new Dictionary<string, object>(),
                                                                                        Properties = new Dictionary<string, object>()
                                                                                    }
                                                                                };

            public IReadOnlyList<IModelBindingConfiguration> ModelBindings { get; } = new IModelBindingConfiguration[0];

            public IReadOnlyDictionary<string, string> MediaTypes { get; } = new Dictionary<string, string>();
            public IExceptionConfiguration Exceptions { get; } = new MyExConfig();

            private class MyExConfig : IExceptionConfiguration
            {
                public bool RegisterCommonHandlers { get; } = false;
                public bool AllowStackTraceInOutput { get; } = false;
                public IReadOnlyList<ITypeInstanceConfiguration> Loggers { get; } = new List<ITypeInstanceConfiguration>();
            }

            private class TypeInstanceConfiguration : ITypeInstanceConfiguration
            {
                public Type Type { get; set; }
                public IReadOnlyDictionary<string, object> Parameters { get; set; }
                public IReadOnlyDictionary<string, object> Properties { get; set; }
            }
        }
    }

    public class MyMiddleWare
    {
    }
}