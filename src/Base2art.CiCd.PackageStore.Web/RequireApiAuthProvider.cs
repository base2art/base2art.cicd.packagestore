namespace Base2art.CiCd.PackageStore.Web
{
    using System;
    using System.IO;
    using System.Net.Mail;
    using System.Threading.Tasks;
    using Data;
    using Threading.Tasks;

    public class RequireApiAuthProvider : IRequireApiAuthProvider
    {
        private readonly KeyRepo keys;
        private readonly MembershipRepo users;
        private readonly ChannelRepo areas;

        public RequireApiAuthProvider(
            KeyRepo keys,
            MembershipRepo users,
            ChannelRepo areas)
        {
            this.keys = keys;
            this.users = users;
            this.areas = areas;
        }

        public async Task RequireAccessLevel(
            FileAccess demand,
            Guid spaceId,
            Guid channelId,
            Guid accessKey)
        {
            var (canPerform, message) = await this.CanPerformActionInternal(demand, spaceId, channelId, accessKey);

            if (!canPerform)
            {
                throw new AccessViolationException(message);
            }
        }

        public Task<bool> CanPerformForAccessLevel(FileAccess demand, Guid spaceId, Guid channelId, Guid accessKey)
            => this.CanPerformActionInternal(demand, spaceId, channelId, accessKey).Then().Return(x => x.canPerform);

        private async Task<(bool canPerform, string message)> CanPerformActionInternal(FileAccess demand, Guid spaceId, Guid channelId, Guid accessKey)
        {
            var key = await this.keys.GetKeyByPassword(accessKey);
            if (key == null)
            {
                return (false, $"Not Found: `{nameof(accessKey)}`.`*************************`");
            }

            var user = await this.users.GetUser(key.UserId);

            if (user == null)
            {
                return (false, $"User Not Found: `{key.UserId}`.`{key.UserId}`");
            }

            if (key.Expires < DateTime.UtcNow)
            {
                return (false, $"Key Expired: `{nameof(accessKey)}`.`{key.Name}`");
            }

            var space = await this.areas.GetSpaceById(spaceId);
            if (space == null)
            {
                return (false, $"Not Found: `{nameof(spaceId)}`.`{spaceId}`");
            }

            var channel = await this.areas.GetChannelBySpace(spaceId, channelId);
            if (channel == null)
            {
                return (false, $"Not Found: `{nameof(spaceId)}`.`{spaceId}`.`{nameof(channelId)}`.`{channelId}`");
            }

            if (demand.HasFlag(FileAccess.Read) && !key.CanRead)
            {
                return (false, $"Insufficient Permissions (Read): `{nameof(accessKey)}`.`{key.Name}`");
            }

            if (demand.HasFlag(FileAccess.Write) && !key.CanWrite)
            {
                return (false, $"Insufficient Permissions (Write): `{nameof(accessKey)}`.`{key.Name}`");
            }

            var userEmail = new MailAddress(user.Email);

            if (!RequireAuthProviders.IsAllowed(space.PushAddresses, space.PushDomains, userEmail))
            {
                return (false,
                           $"Space Host MisMatch: `{nameof(space)}`.`{nameof(space.PushDomains)}`.`*******` - `{nameof(user)}`.`{nameof(user.Id)}`.`{user.Id}`");
            }

            // if (!string.IsNullOrWhiteSpace(space.Host) && !string.Equals(space.Host, host, StringComparison.OrdinalIgnoreCase))
            // {
            //     
            // }

            return (true, "OK");
        }
    }
}