namespace Base2art.CiCd.PackageStore.Web.UI.Public.Resources
{
    using System;

    public class Key
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid Password { get; set; }
        public bool CanRead { get; set; }
        public bool CanWrite { get; set; }
        public bool CanDelete { get; set; }
        public DateTime Expires { get; set; }
    }
}