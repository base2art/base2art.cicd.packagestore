namespace Base2art.CiCd.PackageStore.Web.Fixtures.Models
{
    using System.Text.Json.Serialization;

    public class Version
    {
        public string version { get; set; }

        public int downloads { get; set; }

        [JsonPropertyName("@id")]
        public string at_id { get; set; }
    }
}