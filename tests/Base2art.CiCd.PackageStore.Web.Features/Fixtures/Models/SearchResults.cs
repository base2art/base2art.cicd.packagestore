namespace Base2art.CiCd.PackageStore.Web.Fixtures.Models
{
    using System.Collections.Generic;

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 

    public class SearchResults
    {
        public int totalHits { get; set; }
        public List<Datum> data { get; set; }
    }
}