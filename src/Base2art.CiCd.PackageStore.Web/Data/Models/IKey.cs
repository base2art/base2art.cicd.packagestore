namespace Base2art.CiCd.PackageStore.Web.Data.Models
{
    using System;

    public interface IKey
    {
        string Name { get; }
        Guid Id { get; }
        DateTime Expires { get; }
        bool CanRead { get; }

        bool CanWrite { get; }

        bool CanDelete { get;  }
        Guid UserId { get; }
        Guid Password { get; }
    }
}