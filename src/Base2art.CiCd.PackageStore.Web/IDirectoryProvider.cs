namespace Base2art.CiCd.PackageStore.Web
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public interface IDirectoryProvider
    {
//        DirectoryInfo GetCheckoutRoot();

        // DirectoryInfo GetWorkingDir();

//        DirectoryInfo OriginsRoot();
//
//        DirectoryInfo DataDirectory();
        // DirectoryInfo GetMembershipDir(Guid spaceId, Guid channelId);
        DirectoryInfo GetUsersDir();
        DirectoryInfo GetSpacesDir();
        DirectoryInfo GetChannelsDir(Guid spaceId);
        DirectoryInfo GetPackagesDir(Guid spaceId, Guid channelId);
        DirectoryInfo GetPackageExtractionDir(Guid spaceId);

        string ToName(Guid id);
        DirectoryInfo GetUserDataDir(Guid userId);
        DirectoryInfo GetKeyRefDataDir();

        Task WriteDebuggingInfo(TextWriter writer);
    }
}