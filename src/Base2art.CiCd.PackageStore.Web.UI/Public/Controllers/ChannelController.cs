
namespace Base2art.CiCd.PackageStore.Web.UI.Public.Controllers
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Flows;
    using Base2art.Web.Http;
    using Base2art.Web.ObjectQualityManagement;
    using Base2art.Web.Pages;
    using Converters;
    using Data;
    using Data.Models;
    using Pages;
    using Pages.Channels;
    using Resources;
    using Threading.Tasks;
    using Validation;
    using ViewModels.Channels;

    public class ChannelController
    {
        private readonly MembershipRepo users;
        private readonly IRequireAuthProvider auth;
        private readonly ChannelRepo spaces;
        private readonly RestListOperationFactory listApi;
        private readonly RestCreateOperationFactory createApi;
        private readonly RestEditOperationFactory editApi;
        private readonly SpaceConverter spaceConverter;

        public ChannelController(ChannelRepo spaces, MembershipRepo users, IRequireAuthProvider auth, ILayoutProvider layout)
        {
            this.spaces = spaces;
            this.users = users;
            this.auth = auth;
            var userLookup = new AuthenticatedUserLookup(this.users);
            this.listApi = new RestListOperationFactory(layout, userLookup);
            this.createApi = new RestCreateOperationFactory(layout, userLookup);
            this.editApi = new RestEditOperationFactory(layout, userLookup);
            this.spaceConverter = new SpaceConverter(new PersonLookup(x => this.users.GetUser(x)));
        }

        public async Task<WebPage> GetNewItem(ClaimsPrincipal principal, IHttpRequest request, Guid spaceId)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionCreateChannel(principal, spaceId));
            return await this.createApi.Create<IChannel>()
                             .WithRequest(request)
                             .WithUser(principal)
                             .Build(() => new Edit(), async (user, objectId, errors) => new EditViewModel
                                                                                        {
                                                                                            Data = (null, null),
                                                                                            Errors = errors,
                                                                                            OperationType = OperationType.Creating,
                                                                                            DataValidation = new SpaceValidator(false)
                                                                                        });
        }

        public async Task<WebPage> NewItem(
            ClaimsPrincipal principal,
            IHttpRequest request,
            Guid spaceId,
            Channel channel)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionCreateChannel(principal, spaceId));
            return await this.createApi.Create<IChannel>()
                             .WithRequest(request)
                             .WithValidation(() => this.ValidateItem(channel))
                             .WithPersistence(x => x.Id, user => this.spaces.AddChannel(spaceId, channel.Name))
                             .WithUser(principal)
                             .WithEditUrl(objectId => $"/channels/edit/{spaceId}/{objectId}")
                             .Build(() => new Edit(), async (user, objectId, errors) =>
                             {
                                 var space = await this.spaceConverter.ConvertToModel(this.spaces.GetSpaceById(spaceId),
                                                                                      this.spaces.GetChannelsBySpace(spaceId));

                                 return new EditViewModel
                                        {
                                            Data = (null, space),
                                            Errors = errors,
                                            OperationType = OperationType.Creating,
                                            DataValidation = new SpaceValidator(false)
                                        };
                             });
        }

        public async Task<WebPage> GetEditItem(ClaimsPrincipal principal, IHttpRequest request, Guid spaceId, Guid channelId)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionEditChannel(principal, spaceId, channelId));
            return await this.editApi.Create<Space>()
                             .WithRequest(request)
                             .WithUser(principal)
                             .BuildForGet(
                                          () => new Edit(),
                                          async (user, errors) =>
                                          {
                                              var space = await this.spaceConverter.ConvertToModel(this.spaces.GetSpaceById(spaceId),
                                                                                                   this.spaces.GetChannelsBySpace(spaceId));

                                              return new EditViewModel
                                                     {
                                                         Data = (space.Channels.FirstOrDefault(x => x.Id == channelId), space),
                                                         Errors = errors,
                                                         OperationType = OperationType.Updating,
                                                         DataValidation = new SpaceValidator(true)
                                                     };
                                          });
        }

        public async Task<WebPage> EditItem(ClaimsPrincipal principal, IHttpRequest request, Guid spaceId, Guid channelId, Channel channel)
        {
            await this.auth.UI().Then().ExecuteAsync(x => x.RequireActionEditChannel(principal, spaceId, channelId));
            return await this.editApi.Create<Channel>()
                             .WithRequest(request)
                             .WithValidation(() => this.ValidateItem(channel, true))
                             .WithPersistence((user) => this.spaces.UpdateChannel(spaceId, channel.Id, channel.Name).Then().Return(x => x.Id))
                             .WithUser(principal)
                             // .WithLookup(() => this.spaceConverter.ConvertToModel(this.spaces.GetSpaceById(spaceId), this.spaces.GetChannelsBySpace(spaceId)))
                             .WithEditUrl(objectId => $"/channels/edit/{spaceId}/{objectId}")
                             .BuildForSubmit(() => new Edit(), async (user, errors) =>
                             {
                                 var space = await this.spaceConverter.ConvertToModel(this.spaces.GetSpaceById(spaceId),
                                                                                      this.spaces.GetChannelsBySpace(spaceId));
                                 return new EditViewModel
                                        {
                                            Data = (channel, space),
                                            Errors = errors,
                                            OperationType = OperationType.Updating,
                                            DataValidation = new SpaceValidator(true)
                                        };
                             });
        }

        private Task<IQualityManagementResult> ValidateItem(Channel space, bool update = false)
        {
            IQualityManagementLookup lookup = new QualityManagementLookup<Channel>().AddValidator(new ChannelValidator(update));

            return lookup.ValidateAndVerifyResult(space);
        }
    }
}