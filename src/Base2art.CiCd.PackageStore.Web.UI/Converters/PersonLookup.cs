namespace Base2art.CiCd.PackageStore.Web.UI.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Data;
    using Data.Models;

    public class PersonLookup : IPersonLookup
    {
        private readonly Func<Guid, Task<IUser>> func;

        public PersonLookup(Func<Guid, Task<IUser>> func)
            => this.func = func;

        public async Task<IPersonData> GetPerson(Guid personId)
        {
            var person = await this.func.Invoke(personId);

            if (person == null)
            {
                return null;
            }

            return new FakePersonData
                   {
                       Id = person.Id,
                       Name = person.Name,
                   };
        }

        public async Task<IReadOnlyDictionary<Guid, IPersonData>> GetPersons(IEnumerable<Guid> items)
        {
            var lookup = new Dictionary<Guid, IPersonData>();

            foreach (var item in items)
            {
                lookup[item] = await GetPerson(item);
            }

            return lookup;
        }

        private class FakePersonData : IPersonData
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }
    }
}