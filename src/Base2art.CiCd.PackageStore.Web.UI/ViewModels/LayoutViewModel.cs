namespace Base2art.CiCd.PackageStore.Web.UI.ViewModels
{
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Web.Pages;
    using Data;

    public class LayoutViewModel
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public Task<WebPage> Body { get; set; }
        public string Url { get; set; }
        public ClaimsPrincipal User { get; set; }
        public bool ShowCopyright { get; set; }
        public IUser UserData { get; set; }
    }
}