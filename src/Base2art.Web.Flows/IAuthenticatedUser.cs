namespace Base2art.Web.Flows
{
    using System;
    using System.Net.Mail;

    public interface IAuthenticatedUser
    {
        string Id { get; }
        MailAddress Email { get; }
        Guid UserId { get; }
    }
}