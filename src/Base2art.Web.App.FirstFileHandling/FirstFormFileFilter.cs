namespace Base2art.Web.App.FirstFileHandling
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Web.FirstFileHandling;

    public class FirstFormFileFilter : InputFormatter
    {
        public FirstFormFileFilter()
        {
            this.SupportedMediaTypes.Add("application/octet-stream");
            this.SupportedMediaTypes.Add("application/zip");
            this.SupportedMediaTypes.Add("multipart/form-data");
        }

        public override Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context)
            => InputFormatterResult.SuccessAsync(new FirstFormFile(context.HttpContext.Request));

        protected override bool CanReadType(Type type)
            => type == typeof(IFirstFormFile);
    }
}