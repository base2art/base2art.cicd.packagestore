﻿namespace Base2art.Web.FirstFileHandling
{
    using System.IO;
    using System.Threading.Tasks;

    public interface IFirstFormFile
    {
        Task<Stream> OpenReadStream();
    }
}