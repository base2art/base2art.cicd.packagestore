namespace Base2art.CiCd.PackageStore.Web.UI.Validation
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Net.Mail;
    using System.Text.RegularExpressions;
    using FluentValidation;
    using FluentValidation.Internal;
    using FluentValidation.Validators;
    using Public.Resources;

    public class SpaceValidator : Base2art.Web.ObjectQualityManagement.ValidatorBase<Space>
    {
        private readonly bool update;

        public SpaceValidator(bool update)
        {
            this.update = update;
        }

        public Regex HostPattern { get; } = new Regex(@"^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,6}(\.[a-zA-Z]{2,6})?$");

        protected override void Configure()
        {
            if (this.update)
            {
                this.Backing.RuleFor(x => x.Id).NotEmpty();
            }

            this.Backing.RuleFor(x => x.Name).NotEmpty();
            // this.Backing.RuleForEach(x=>x.ManagementAddresses)
            this.Backing.RuleForEach(x => x.ManagementDomains).Matches(this.HostPattern);
            this.Backing.RuleForEach(x => x.PushDomains).Matches(this.HostPattern);

            this.Backing.RuleForEach(x => x.ManagementAddresses).Must((x) => IsAddress(x));
            this.Backing.RuleForEach(x => x.PushAddresses).Must((x) => IsAddress(x));

            this.Backing
                .RuleFor(x => x)
                .Must(space => this.GetValue(space?.ManagementAddresses?.Length) + this.GetValue(space?.ManagementDomains?.Length) > 0)
                .WithMessage("Must specify a management domain or address");

            this.Backing
                .RuleFor(x => x)
                .Must(space => GetValue(space?.PushAddresses?.Length) + GetValue(space?.PushDomains?.Length) > 0)
                .WithMessage("Must specify a push domain or address");
        }

        private T GetValue<T>(T? valueItem)
            where T : struct
        {
            return valueItem.GetValueOrDefault();
        }

        private bool IsAddress(string s)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return true;
            }

            try
            {
                var mailAddress = new MailAddress(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsRequired(Expression<Func<Space, object>> expr)
        {
            // HACK
            Validate(new Space());
            foreach (var variable in this.Backing.OfType<PropertyRule>())
            {
                if (variable.Member == expr.GetMember())
                {
                    // Console.WriteLine(variable.Member);
                    return variable.Validators.Any(x => x is NotEmptyValidator);
                }
            }

            return false;
        }
    }
}
// public Regex Pattern(Expression<Func<Space, object>> expr)
// {
//     // HACK
//     Validate(new Space());
//     foreach (var variable in this.Backing.OfType<PropertyRule>())
//     {
//         if (variable.Member == expr.GetMember())
//         {
//             // Console.WriteLine(variable.Member);
//             var item = variable.Validators.OfType<RegularExpressionValidator>().FirstOrDefault()?.Expression;
//             return item == null ? null : new Regex(item);
//         }
//     }
//
//     return null;
// }