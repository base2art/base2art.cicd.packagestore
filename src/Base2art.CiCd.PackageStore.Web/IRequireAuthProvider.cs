namespace Base2art.CiCd.PackageStore.Web
{
    using System.Threading.Tasks;

    public interface IRequireAuthProvider
    {
        IRequireApiAuthProvider Api();
        Task<IRequireUIAuthProvider> UI(bool isAdminManagement = false);
    }
}