namespace Base2art.CiCd.PackageStore.Web.UI.Validation
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using FluentValidation;
    using FluentValidation.Internal;
    using FluentValidation.Validators;
    using Public.Resources;

    public class ChannelValidator : Base2art.Web.ObjectQualityManagement.ValidatorBase<Channel>
    {
        private readonly bool update;

        public ChannelValidator(bool update)
        {
            this.update = update;
        }

        protected override void Configure()
        {
            if (this.update)
            {
                this.Backing.RuleFor(x => x.Id).NotEmpty();
            }

            this.Backing.RuleFor(x => x.Name).NotEmpty();
        }

        public bool IsRequired(Expression<Func<Space, object>> expr)
        {
            // HACK
            Validate(new Channel());
            foreach (var variable in this.Backing.OfType<PropertyRule>())
            {
                if (variable.Member == expr.GetMember())
                {
                    // Console.WriteLine(variable.Member);
                    return variable.Validators.Any(x => x is NotEmptyValidator);
                }
            }

            return false;
        }
    }
}