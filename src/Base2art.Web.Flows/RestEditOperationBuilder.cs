namespace Base2art.Web.Flows
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Exceptions;
    using Http;
    using ObjectQualityManagement;
    using Pages;

    public class RestEditOperationBuilder<T>
        where T : class
    {
        private readonly ILayoutProvider layoutProvider;
        private readonly IAuthenticatedUserLookup authenticatedUserLookup;
        private IHttpRequest request;

        private ClaimsPrincipal principal;

        // private Func<Task<IQualityManagementResult>> validation;
        // private Func<IAuthenticatedUser, Task<Guid>> persistence;
        // private Func<Task<T>> lookup;
        private Func<Task<IQualityManagementResult>> validation;
        private Func<IAuthenticatedUser, Task<Guid>> persistence;
        private Func<Guid, string> urlProvider;

        public RestEditOperationBuilder(ILayoutProvider layoutProvider, IAuthenticatedUserLookup authenticatedUserLookup)
        {
            this.layoutProvider = layoutProvider;
            this.authenticatedUserLookup = authenticatedUserLookup;
        }

        public RestEditOperationBuilder<T> WithRequest(IHttpRequest value)
        {
            this.request = value;
            return this;
        }

        public RestEditOperationBuilder<T> WithUser(ClaimsPrincipal value)
        {
            this.principal = value;
            return this;
        }

        // public RestEditOperationBuilder<T> WithLookup(Func<Task<T>> value)
        // {
        //     this.lookup = value;
        //     return this;
        // }

        public RestEditOperationBuilder<T> WithPersistence(Func<IAuthenticatedUser, Task<Guid>> action)
        {
            this.persistence = action;
            return this;
        }

        public RestEditOperationBuilder<T> WithValidation(Func<Task<IQualityManagementResult>> value)
        {
            this.validation = value;
            return this;
        }

        public RestEditOperationBuilder<T> WithEditUrl(Func<Guid, string> value)
        {
            this.urlProvider = value;
            return this;
        }

        public async Task<WebPage> BuildForGet<T1>(
            Func<IWebPage<T1>> func,
            Func<IAuthenticatedUser, IQualityManagementResult, Task<T1>> func1)
        {
            var user = await this.authenticatedUserLookup.GetUser(this.principal);

            var body = WebPage.Create(func(), await func1(user, new QualityManagementResult()));
            return await this.layoutProvider.BuildLayout(body: Task.FromResult(body),
                                                         // author:Auth
                                                         request: this.request,
                                                         user: this.principal);
        }

        public async Task<WebPage> BuildForSubmit<T1>(
            // T data,
            Func<IWebPage<T1>> func,
            Func<IAuthenticatedUser, IQualityManagementResult, Task<T1>> func1)
        {
            var user = await this.authenticatedUserLookup.GetUser(this.principal);

            IQualityManagementResult validation1 = new QualityManagementResult();
            if (this?.validation != null)
            {
                var result = await this.validation.Invoke();
                validation1 = result;
            }

            if (!validation1.IsValid)
            {
                var body = WebPage.Create(func(), await func1(user, validation1));
                return await this.layoutProvider.BuildLayout(body: Task.FromResult(body),
                                                             // author:Auth
                                                             request: this.request,
                                                             user: this.principal);
            }
            else
            {
                var objectId = await (this.persistence?.Invoke(user) ?? Task.FromResult(Guid.Empty));

                if (objectId != Guid.Empty && this.urlProvider != null)
                {
                    throw new RedirectException(this.urlProvider(objectId), false, false);
                }

                var body = WebPage.Create(func(), await func1(user, validation1));
                return await this.layoutProvider.BuildLayout(body: Task.FromResult(body),
                                                             // author:Auth
                                                             request: this.request,
                                                             user: this.principal);
            }
        }

        private class QualityManagementResult : IQualityManagementResult
        {
            public bool IsValid => this.Errors.Length == 0;
            public IQualityManagementError[] Errors { get; } = new IQualityManagementError[0];
        }
    }
}