namespace Base2art.CiCd.PackageStore.Web.Public.Resources
{
    using System.Collections.Generic;

    public class CategoryEntry : Dictionary<string, object>
    {
        public CategoryEntry(bool includeContext)
        {
            this.Add("@type", "PackageDetails");
            // this.Add("listed", true);
            this.listed = true;
            this.Add("dependencyGroups", new object[0]);
            // this.Add("dependencyGroups", new object[0]);
            this.Add("minClientVersion", "");

            if (includeContext)
            {
                this.Add("@context", new Dictionary<string, object>
                                     {
                                         {"@vocab", "http://schema.nuget.org/schema#"},
                                         {"catalog", "http://schema.nuget.org/catalog#"},
                                         {"xsd", "http://www.w3.org/2001/XMLSchema#"},

                                         {
                                             "tags",
                                             new Dictionary<string, string>
                                             {
                                                 {"@id", "tag"},
                                                 {"@container", "set"}
                                             }
                                         },
                                         {
                                             "published",
                                             new Dictionary<string, string>
                                             {
                                                 {"@type", "xsd:dateTime"}
                                             }
                                         },
                                         {
                                             "created",
                                             new Dictionary<string, string>
                                             {
                                                 {"@type", "xsd:dateTime"}
                                             }
                                         },
                                         {
                                             "lastEdited",
                                             new Dictionary<string, string>
                                             {
                                                 {"@type", "xsd:dateTime"}
                                             }
                                         },
                                         {
                                             "catalog:commitTimeStamp",
                                             new Dictionary<string, string>
                                             {
                                                 {"@type", "xsd:dateTime"}
                                             }
                                         }
                                     });
            }
        }

        public string version
        {
            get => (string) this[nameof(this.version)];
            set => this[nameof(this.version)] = value;
        }

        public string[] tags
        {
            get => (string[]) this[nameof(this.tags)];
            set => this[nameof(this.tags)] = value;
        }

        public string id
        {
            get => (string) this[nameof(this.id)];
            set => this[nameof(this.id)] = value;
        }

        public string authors
        {
            get => (string) this[nameof(this.authors)];
            set => this[nameof(this.authors)] = value;
        }

        public string description
        {
            get => (string) this[nameof(this.description)];
            set => this[nameof(this.description)] = value;
        }

        public string iconUrl
        {
            get => (string) this[nameof(this.iconUrl)];
            set => this[nameof(this.iconUrl)] = value;
        }

        public string licenseUrl
        {
            get => (string) this[nameof(this.licenseUrl)];
            set => this[nameof(this.licenseUrl)] = value;
        }

        public string licenseExpression
        {
            get => (string) this[nameof(this.licenseExpression)];
            set => this[nameof(this.licenseExpression)] = value;
        }

        public string language
        {
            get => (string) this[nameof(this.language)];
            set => this[nameof(this.language)] = value;
        }

        public bool listed
        {
            get => (bool) this[nameof(this.listed)];
            set => this[nameof(this.listed)] = value;
        }

        public string packageContent
        {
            get => (string) this[nameof(this.packageContent)];
            set => this[nameof(this.packageContent)] = value;
        }

        public string projectUrl
        {
            get => (string) this[nameof(this.projectUrl)];
            set => this[nameof(this.projectUrl)] = value;
        }

        public string published
        {
            get => (string) this[nameof(this.published)];
            set => this[nameof(this.published)] = value;
        }

        public bool requireLicenseAcceptance
        {
            get => (bool) this[nameof(this.requireLicenseAcceptance)];
            set => this[nameof(this.requireLicenseAcceptance)] = value;
        }

        public string summary
        {
            get => (string) this[nameof(this.summary)];
            set => this[nameof(this.summary)] = value;
        }

        public string title
        {
            get => (string) this[nameof(this.title)];
            set => this[nameof(this.title)] = value;
        }

        public void SetInternalId(string value)
        {
            this.Add("@id", value);
        }
    }
}