namespace Base2art.Web.App.Principals.Auth.Configuration
{
    using Microsoft.AspNetCore.Authentication;

    public interface IAuthenticationModule
    {
         void AddAuthentication(AuthenticationBuilder authBuilder);
    }
}