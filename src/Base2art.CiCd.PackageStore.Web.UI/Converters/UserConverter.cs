namespace Base2art.CiCd.PackageStore.Web.UI.Converters
{
    using Data.Models;
    using Public.Resources;

    public static class UserConverter
    {
        public static PersonAudit ConvertToModel(this IPersonData user)
        {
            if (user == null)
            {
                return null;
            }

            return new PersonAudit
                   {
                       PersonId = user.Id,
                       PersonName = user.Name
                   };
        }
    }
}