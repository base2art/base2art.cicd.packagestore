namespace Base2art.CiCd.PackageStore.Web.Fixtures.TestServer
{
    using Base2art.Web.App.Configuration;
    using WebApiRunner.Server;
    using WebApiRunner.Server.Testability.Configuration;

    public class TestAspectLookup : IAspectLookup
    {
        public ITypeInstanceConfiguration GetByName(string name) => new AspectConfiguration();
    }
}