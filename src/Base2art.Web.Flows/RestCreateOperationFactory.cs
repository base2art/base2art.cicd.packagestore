namespace Base2art.Web.Flows
{
    public class RestCreateOperationFactory
    {
        private readonly ILayoutProvider layoutProvider;
        private readonly IAuthenticatedUserLookup authenticatedUserLookup;

        public RestCreateOperationFactory(ILayoutProvider layoutProvider, IAuthenticatedUserLookup authenticatedUserLookup)
        {
            this.layoutProvider = layoutProvider;
            this.authenticatedUserLookup = authenticatedUserLookup;
        }

        public RestCreateOperationBuilder<T> Create<T>()
        {
            return new RestCreateOperationBuilder<T>(this.layoutProvider, this.authenticatedUserLookup);
        }
    }
}