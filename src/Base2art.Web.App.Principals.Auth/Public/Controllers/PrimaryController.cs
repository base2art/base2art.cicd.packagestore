namespace Base2art.Web.App.Principals.Auth.Public.Controllers
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Auth;
    using Configuration;
    using Exceptions;
    using Pages;
    using ViewModels;
    using Web.Pages;

    public class PrimaryController
    {
        private readonly IAuthenticationScheme[] modules;
        private readonly IAuthorizationModule module;

        public PrimaryController(IAuthenticationScheme[] modules, IAuthorizationModule module)
        {
            this.modules = modules;
            this.module = module;
        }

        public Task<WebPage> GetSignIn(string returnUrl, FailureMessage? message, ClaimsPrincipal principal)
        {
            if (principal.Identity.IsAuthenticated)
            {
                throw new RedirectException("/" + returnUrl?.TrimStart('/'), false, false);
            }

            if (!message.HasValue && this.modules.Length == 1)
            {
                throw new RedirectException($"/auth/sign-in/{this.modules[0].Name}?returnUrl={Uri.EscapeUriString(returnUrl)}", false, false);
            }

            return Task.FromResult(WebPage.Create(
                                                  new Layout(),
                                                  new LayoutViewModel
                                                  {
                                                      SmallBody = true,
                                                      Body = Task.FromResult(WebPage.Create(new SignInChooser(), new SignInViewModel
                                                                                                {
                                                                                                    Schemes = this.modules ?? new IAuthenticationScheme[0],
                                                                                                    ReturnUrl = returnUrl,
                                                                                                    Message = message,
                                                                                                    RequiredDomain = this.module?.RequiredDomain ?? ""
                                                                                                })),
                                                  }));
        }

        public Task<WebPage> Debug(string returnUrl, ClaimsPrincipal principal)
        {
            if (!principal.Identity.IsAuthenticated)
            {
                throw new RedirectException("/" + returnUrl?.TrimStart('/'), false, false);
            }

            return Task.FromResult(WebPage.Create(
                                                  new Layout(),
                                                  new LayoutViewModel
                                                  {
                                                      SmallBody = false,
                                                      Body = Task.FromResult(WebPage.Create(new PrincipalDebugger(), principal)),
                                                  }));
        }

        public Task<WebPage> GetSignOut(string returnUrl, ClaimsPrincipal principal)
        {
            if (!principal.Identity.IsAuthenticated)
            {
                throw new RedirectException("/" + returnUrl?.TrimStart('/'), false, false);
            }

            throw new SignOutException();
        }

        public Task<WebPage> GetSignInByType(string authProvider, string returnUrl, ClaimsPrincipal principal)
        {
            if (principal.Identity.IsAuthenticated)
            {
                throw new RedirectException("/" + returnUrl?.TrimStart('/'), false, false);
            }

            throw new NotAuthenticatedException("Not Authenticated")
                  {
                      Data = {{"AuthenticationType", authProvider}}
                  };
            // return WebPage.Create(new Layout(), new LayoutViewModel
            //                                     {
            //                                         Body = Task.FromResult(WebPage.Create(new SignInChooser()))
            //                                     });
        }
    }

    public enum FailureMessage
    {
        BadDomain
    }
}
//
// public async Task<WebPage> GetSignInGoogle()
// {
//     return WebPage.Create(new Layout(), new LayoutViewModel
//                                         {
//                                             Body = Task.FromResult(WebPage.Create(new SignInChooser()))
//                                         });
// }
//
// public async Task<WebPage> GetSignInActiveDirectory()
// {
//     return WebPage.Create(new Layout(), new LayoutViewModel
//                                         {
//                                             Body = Task.FromResult(WebPage.Create(new SignInChooser()))
//                                         });
// }
//
// public async Task<WebPage> GetSignInAmazon()
// {
//     return WebPage.Create(new Layout(), new LayoutViewModel
//                                         {
//                                             Body = Task.FromResult(WebPage.Create(new FormSignIn()))
//                                         });
// }
//
// public async Task<WebPage> GetSignInFacebook()
// {
//     return WebPage.Create(new Layout(), new LayoutViewModel
//                                         {
//                                             Body = Task.FromResult(WebPage.Create(new FormSignIn()))
//                                         });
// }
//
// public async Task<WebPage> GetSignInForms()
// {
//     return WebPage.Create(new Layout(), new LayoutViewModel
//                                         {
//                                             Body = Task.FromResult(WebPage.Create(new FormSignIn()))
//                                         });
// }