namespace Base2art.Web.Flows.Utilities.Hashing
{
    using System.Globalization;
    using System.Text;

    internal class HashResult
    {
        public HashResult(byte[] hashedBytes) => this.HashedBytes = hashedBytes;

        protected byte[] HashedBytes { get; }

        public byte[] AsByteArray() => this.HashedBytes;

        public string AsString()
        {
            var hash = this.HashedBytes;
            // step 2, convert byte array to hex string
            var sb = new StringBuilder();
            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2", CultureInfo.InvariantCulture));
            }

            return sb.ToString();
        }
    }
}