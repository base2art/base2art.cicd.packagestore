namespace Base2art.CiCd.PackageStore.Web.Fixtures.TestServer
{
    using Base2art.Web.App.Configuration;
    using Base2art.Web.Server.Registration;
    using WebApiRunner.Server;
    using WebApiRunner.Server.Configuration.CommandLine;
    using WebApiRunner.Server.Configuration.Loader;
    using WebApiRunner.Server.Runners.Configuration;
    using WebApiRunner.Server.Testability;
    using WebApiRunner.Server.Testability.Configuration;

    public static class IntegrationTests
    {
        public static TestFixture<Startup> CreateFixture(this IServerConfiguration serverConfiguration, params IRegistration[] registrations)
            => new TestFixture<Startup>(serverConfiguration, new FrameworkShim(), registrations);

        public static TestFixture<Startup> CreateFixture(string configPath)
        {
            var assemblyLoader = new CurrentAppDomainAssemblyLoader(
                                                                    new TestAssemblyFactory(),
                                                                    new[]
                                                                    {
                                                                        "netcoreapp2.2",
                                                                        "netcoreapp2.1",
                                                                        "netcoreapp2.0",
                                                                        "netstandard2.0",
                                                                        "netstandard1.6",
                                                                        "netstandard1.5",
                                                                        "netstandard1.4",
                                                                        "netstandard1.3",
                                                                        "netstandard1.2",
                                                                        "netstandard1.1",
                                                                        "netstandard1.0"
                                                                    });
            var config = new ConfigurationProvider<ConsoleServerConfiguration>(configPath);

            var configWrapper = new ServerConfigurationWrapper(
                                                               assemblyLoader,
                                                               new TypeLoader(assemblyLoader),
                                                               new TestAspectLookup(),
                                                               config);

            return CreateFixture(configWrapper);
        }
    }
}