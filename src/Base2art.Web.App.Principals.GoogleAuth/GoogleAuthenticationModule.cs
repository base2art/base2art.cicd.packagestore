namespace Base2art.Web.App.Principals.GoogleAuth
{
    using System;
    using System.IO;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Auth.Configuration;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Authentication.Google;
    using Microsoft.AspNetCore.Authentication.OAuth;
    using Microsoft.Extensions.DependencyInjection;

    public class GoogleAuthenticationModule : SingleAuthenticationModule
    {
        private readonly string clientId;

        private readonly string clientSecret;

        public GoogleAuthenticationModule(string clientId, string clientSecret)
        {
            if (clientId == "#{GoogleAuth_ClientId}" && clientSecret == "#{GoogleAuth_ClientSecret}")
            {
                var googleAuthClientId = Path.Combine(
                                                      Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                                                      ".config",
                                                      "base2art",
                                                      "deployment",
                                                      "coordinator",
                                                      "GoogleAuth_ClientId");
                var googleAuthClientSecret = Path.Combine(
                                                          Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                                                          ".config",
                                                          "base2art",
                                                          "deployment",
                                                          "coordinator",
                                                          "GoogleAuth_ClientSecret");

                this.clientId = File.ReadAllText(googleAuthClientId).Trim();
                this.clientSecret = File.ReadAllText(googleAuthClientSecret).Trim();
            }
            else
            {
                this.clientId = clientId;
                this.clientSecret = clientSecret;
            }
        }

        protected override string AuthenticationScheme => "google";

        protected override void AddAuth(AuthenticationBuilder authBuilder)
        {
            authBuilder.AddOAuth<GoogleOptions, GoogleHandler>(
                                                               this.AuthenticationScheme,
                                                               this.DisplayName,
                                                               // "Google Authentication",
                                                               options =>
                                                               {
                                                                   options.ClientId = this.clientId;
                                                                   options.ClientSecret = this.clientSecret;
                                                                   options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                                                                   options.CallbackPath = "/auth/google/callback";

#if NETCOREAPP3_1_OR_GREATER
                                                                   options.CorrelationCookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.Unspecified;
#endif

                                                                   options.Events.OnCreatingTicket += this.OnCreatingTicket;
                                                                   options.Events.OnRedirectToAuthorizationEndpoint += this.OnRedirectToAuthorizationEndpoint;
                                                               });
        }

        private Task OnRedirectToAuthorizationEndpoint(RedirectContext<OAuthOptions> arg)
        {
            var myRequiredDomain = this.AuthorizationModule.RequiredDomain;
            if (string.IsNullOrWhiteSpace(myRequiredDomain))
            {
                return Task.CompletedTask;
            }
        
            var result = arg.RedirectUri;
            if (result.Contains("&hd=") || result.Contains("?hd="))
            {
                return Task.CompletedTask;
            }
        
            arg.RedirectUri = result + "&hd=" + Uri.EscapeUriString(myRequiredDomain);
            
            arg.Response.Redirect(arg.RedirectUri);
            return Task.CompletedTask;
        }

        private async Task OnCreatingTicket(OAuthCreatingTicketContext arg)
        {
            var email = arg.Principal.GetValue(ClaimTypes.Email);

            var claimsPrincipal = await this.CreatePrincipal(
                                                             arg.Principal.GetValue(ClaimTypes.NameIdentifier),
                                                             email,
                                                             arg.Principal.GetValue(ClaimTypes.Name));

            this.ValidateDomain(email, arg.HttpContext, arg.Properties?.RedirectUri ?? string.Empty);

            arg.Principal = claimsPrincipal;
        }
    }
}