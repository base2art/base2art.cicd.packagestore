namespace Base2art.CiCd.PackageStore.Web
{
    using System.Threading.Tasks;
    using Data;

    public class RequireAuthProvider : IRequireAuthProvider
    {
        private readonly KeyRepo keys;
        private readonly MembershipRepo users;
        private readonly ChannelRepo areas;
        // private readonly MailAddress[] adminAddresses;
        // private readonly string[] adminDomains;

        public RequireAuthProvider(
                KeyRepo keys,
                MembershipRepo users,
                ChannelRepo areas) //,
            // string[] adminDomainsAndAddresses)
        {
            this.keys = keys;
            this.users = users;
            this.areas = areas;

            // this.adminAddresses = adminDomainsAndAddresses.Where(x => !string.IsNullOrWhiteSpace(x))
            //                                               .Where(x => x.Contains("@") && !x.StartsWith("@"))
            //                                               .Select(this.CreateAddress)
            //                                               .Where(x => x != null)
            //                                               .ToArray();
            //
            // this.adminDomains = adminDomainsAndAddresses.Where(x => !string.IsNullOrWhiteSpace(x))
            //                                             .Where(x => !x.Contains("@") || x.StartsWith("@"))
            //                                             .Select(x => x.TrimStart('@'))
            //                                             .Where(x => !string.IsNullOrWhiteSpace(x))
            //                                             .ToArray();
        }

        // private MailAddress CreateAddress(string value)
        // {
        //     try
        //     {
        //         return new MailAddress(value);
        //     }
        //     catch
        //     {
        //     }
        //
        //     return null;
        // }

        public IRequireApiAuthProvider Api() => new RequireApiAuthProvider(this.keys, this.users, this.areas);

        public async Task<IRequireUIAuthProvider> UI(bool isAdminManagement)
        {
            var adminSpace = await this.areas.GetAdminInformation();

            return new RequireUIAuthProvider(this.keys, this.users, this.areas, adminSpace.ManagementAddresses, adminSpace.ManagementDomains, isAdminManagement);
        }
    }
}

// internal static MailAddress MakeAddress(string arg)
// {
//     try
//     {
//         return new MailAddress(arg);
//     }
//     catch
//     {
//     }
//
//     try
//     {
//         return new MailAddress(PrefixUser + (arg.Contains("@") ? arg : $"@{arg}"));
//     }
//     catch
//     {
//     }
//
//     return null;
// }