namespace Base2art.Web.Flows
{
    public class RestListOperationFactory
    {
        private readonly ILayoutProvider layoutProvider;
        private readonly IAuthenticatedUserLookup userLookup;

        public RestListOperationFactory(ILayoutProvider layoutProvider, IAuthenticatedUserLookup userLookup)
        {
            this.layoutProvider = layoutProvider;
            this.userLookup = userLookup;
        }

        public RestListOperationBuilder Create()
        {
            return new RestListOperationBuilder(this.layoutProvider, this.userLookup);
        }
    }
}