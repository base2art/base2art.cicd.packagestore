namespace Base2art.Web.App.FirstFileHandling
{
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Web.FirstFileHandling;

    internal class FirstFormFile : IFirstFormFile
    {
        private readonly HttpRequest httpContextRequest;

        public FirstFormFile(HttpRequest httpContextRequest)
        {
            this.httpContextRequest = httpContextRequest;
        }

        public Task<Stream> OpenReadStream() => this.httpContextRequest.GetUploadStreamOrNullAsync();
    }
}