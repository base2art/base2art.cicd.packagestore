namespace Base2art.CiCd.PackageStore.Web.UI
{
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.Web.ObjectQualityManagement;

    public static class QualityManagersUI
    {
        public static async Task<IQualityManagementResult> ValidateAndVerifyResult<T>(this IQualityManagementLookup lookup, T data)
        {
            var items = lookup.ValidateResult(data);
            return (items?.Length).GetValueOrDefault() != 0 
                       ? new QualityManagementResult(items) 
                       : new QualityManagementResult(await lookup.VerifyResult(data));
        }

        public static IQualityManagementError[] ValidateResult<T>(this IQualityManagementLookup lookup, T data)
        {
            var validators = lookup.GetValidators<T>();

            var resultOf = validators.Select(x => ValidateItem(x, data)).ToArray();

            if (!resultOf.All(x => x.Item1))
            {
                return resultOf.SelectMany(x => x.Item2).ToArray();
            }

            return new IQualityManagementError[0];
        }

        public static async Task<IQualityManagementError[]> VerifyResult<T>(this IQualityManagementLookup lookup, T data)
        {
            var validators = lookup.GetVerifiers<T>();

            var resultOf = await Task.WhenAll(validators.Select(x => VerifyItem(x, data)).ToArray());

            if (!resultOf.All(x => x.Item1))
            {
                return resultOf.SelectMany(x => x.Item2).ToArray();
            }

            return new IQualityManagementError[0];
        }

        private static async Task<(bool, IQualityManagementError[])> VerifyItem<T>(IVerifier<T> verifier, T data)
            => MapToResult<T>(await verifier.Verify(data));

        private static (bool, IQualityManagementError[]) ValidateItem<T>(IValidator<T> validator, T data)
            => MapToResult<T>(validator.Validate(data));

        private static (bool, IQualityManagementError[]) MapToResult<T>(IQualityManagementResult result)
        {
            if (result == null)
            {
                return (true, new IQualityManagementError[0]);
            }

            return !result.IsValid
                       ? (false, (result.Errors ?? new IQualityManagementError[0]).Where(x => x != null).Select(Map).ToArray())
                       : (true, new IQualityManagementError[0]);
        }

        private static IQualityManagementError Map(IQualityManagementError x)
            => x; //new ErrorField {Code = x.ErrorCode, Message = x.ErrorMessage, Path = x.PropertyName};

        private class QualityManagementResult : IQualityManagementResult
        {
            public QualityManagementResult(IQualityManagementError[] errors) => this.Errors = errors;

            public bool IsValid => this.Errors.Length == 0;
            public IQualityManagementError[] Errors { get; }
        }
    }
}