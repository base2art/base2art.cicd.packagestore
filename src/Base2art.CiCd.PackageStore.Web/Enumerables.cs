﻿namespace Base2art.CiCd.PackageStore.Web
{
    using System;
    using System.Linq;

    public static class Enumerables
    {
        public static bool In<T>(this T item, params T[] items)
            where T : struct, IComparable
        {
            return (items ?? new T[0]).Any(y => item.CompareTo(y) == 0);
        }
    }
}