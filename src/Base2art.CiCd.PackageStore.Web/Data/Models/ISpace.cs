namespace Base2art.CiCd.PackageStore.Web.Data.Models
{
    using System;
    using System.Net.Mail;

    public interface ISpace
    {
        Guid Id { get; }
        string Name { get; }
        Guid OwnerId { get; }

        MailAddress[] PushAddresses { get; }
        string[] PushDomains { get; }

        MailAddress[] ManagementAddresses { get; }
        string[] ManagementDomains { get; }

        // Channel[] Channels { get;  }
        // Channel[] Channels { get;  }
    }
}