namespace Base2art.Web.Flows
{
    using System;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Exceptions;
    using Http;
    using ObjectQualityManagement;

    public class RestDeleteOperationBuilder
    {
        private readonly IAuthenticatedUserLookup authenticatedUserLookup;

        private IHttpRequest request;
        private ClaimsPrincipal principal;

        // private Func<T, Guid> idLookup;
        private Func<IAuthenticatedUser, Task> persistence;
        private Func<Task<IQualityManagementResult>> validation;
        private Func<string> listUrlProvider;

        private List<IQualityManagementResult> flash = new List<IQualityManagementResult>();
        private Func<Guid, string> editUrlProvider;

        public RestDeleteOperationBuilder(IAuthenticatedUserLookup authenticatedUserLookup)
        {
            this.authenticatedUserLookup = authenticatedUserLookup;
        }

        //
        public RestDeleteOperationBuilder WithRequest(IHttpRequest value)
        {
            this.request = value;
            return this;
        }

        public RestDeleteOperationBuilder WithUser(ClaimsPrincipal value)
        {
            this.principal = value;
            return this;
        }

        public RestDeleteOperationBuilder WithPersistence(
            // Func<T, Guid> idLookupFunction,
            Func<IAuthenticatedUser, Task> action)
        {
            // this.idLookup = idLookupFunction;
            this.persistence = action;
            return this;
        }

        public RestDeleteOperationBuilder WithValidation(Func<Task<Base2art.Web.ObjectQualityManagement.IQualityManagementResult>> value)
        {
            this.validation = value;
            return this;
        }

        public RestDeleteOperationBuilder WithListUrl(Func<string> func)
        {
            this.listUrlProvider = func;
            return this;
        }

        public RestDeleteOperationBuilder WithEditUrl(Func<Guid, string> func)
        {
            this.editUrlProvider = func;
            return this;
        }

        // Func<IWebPage<T1>> func,
        //                    Func<IAuthenticatedUser, T, IQualityManagementResult, Task<T1>> func1
        public async Task Build(Guid objectId)
        {
            var user = await this.authenticatedUserLookup.GetUser(this.principal);

            IQualityManagementResult validation1 = new QualityManagementResult();
            if (this?.validation != null)
            {
                var result = await this.validation.Invoke();
                validation1 = result;
            }

            var fromResult = this.persistence?.Invoke(user);

            if (fromResult == null)
            {
                fromResult = Task.CompletedTask;
            }

            await fromResult;

            if (!validation1.IsValid)
            {
                this.flash.Add(validation1);

                if (this.editUrlProvider != null)
                {
                    throw new RedirectException(this.editUrlProvider(objectId), false, false);
                }

                if (this.listUrlProvider != null)
                {
                    // var objectId = this.idLookup(objectToPersist);
                    throw new RedirectException(this.listUrlProvider(), false, false);
                }

                throw new NotSupportedException();
            }

            if (this.listUrlProvider != null)
            {
                // var objectId = this.idLookup(objectToPersist);
                throw new RedirectException(this.listUrlProvider(), false, false);
            }

            throw new NotSupportedException();
        }

        private class QualityManagementResult : IQualityManagementResult
        {
            public bool IsValid => this.Errors.Length == 0;
            public IQualityManagementError[] Errors { get; } = new IQualityManagementError[0];
        }
    }
}