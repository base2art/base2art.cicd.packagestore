namespace Base2art.CiCd.PackageStore.Web.Public.Endpoints
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Data;
    using NuGet.Packaging;
    using Resources;

    public class CatalogService
    {
        private readonly IRequireAuthProvider authentication;
        private readonly PackageRepo repo;
        private readonly IUrlProvider urlProvider;

        public CatalogService(
            IUrlProvider urlProvider,
            IRequireAuthProvider authentication,
            PackageRepo repo)
        {
            this.urlProvider = urlProvider;
            this.authentication = authentication;
            this.repo = repo;
        }

        public async Task<CategoryEntry> GetPackage(
            Guid spaceId,
            Guid channelId,
            Guid readPassword,
            DateTime dateTime,
            string packageNameAndVersion)
        {
            await this.authentication.Api().RequireAccessLevel(FileAccess.Read, spaceId, channelId, readPassword);
            await this.repo.ScanForChangesAsync(spaceId, channelId);
            var doc = await this.repo.GetSpecFile(spaceId, channelId, packageNameAndVersion);

            var packageMeta = new NuspecReader(doc);

            var pi = packageMeta.GetIdentity();

            var entry = RegistrationsBaseUrlV36.ProduceCategoryEntry(
                                                                     this.urlProvider,
                                                                     spaceId,
                                                                     channelId,
                                                                     readPassword,
                                                                     dateTime,
                                                                     pi,
                                                                     packageMeta,
                                                                     true);

            return entry;
        }
    }
}

/*
             {
  "@id":"https://api.nuget.org/v3/catalog0/data/2018.10.15.07.26.47/awssdk.core.3.3.5.1.json",
  "@type":[
    "PackageDetails",
    "catalog:Permalink"
  ],
  "authors":"Amazon Web Services",
  "catalog:commitId":"5d2cc3c1-6983-46f5-89a7-13f305582e5f",
  "catalog:commitTimeStamp":"2018-10-15T07:26:47.3563504Z",
  "created":"2016-12-07T15:38:48.713Z",
  "description":"The Amazon Web Services SDK for .NET - Core Runtime",
  "iconUrl":"http://media.amazonwebservices.com/aws_singlebox_01.png",
  "id":"AWSSDK.Core",
  "isPrerelease":false,
  "language":"en-US",
  "lastEdited":"2018-10-15T07:26:40.9Z",
  "licenseUrl":"http://aws.amazon.com/apache2.0/",
  "listed":true,
  "packageHash":"bHtSIlyArMCh7G2Xt+SD/69xQcZP0DoKLwLgcI3l1ucQtnjU05En9mjrpVKqC8df+mSaEzAwbmO4E/wwzkNR3g==",
  "packageHashAlgorithm":"SHA512",
  "packageSize":4855484,
  "projectUrl":"https://github.com/aws/aws-sdk-net/",
  "published":"2016-12-07T15:38:48.713Z",
  "requireLicenseAcceptance":false,
  "title":"AWSSDK - Core Runtime",
  "verbatimVersion":"3.3.5.1",
  "version":"3.3.5.1",
  "dependencyGroups":[
  ],
  "packageEntries":[
  ],
  "tags":[
    "AWS",
    "Amazon",
    "cloud",
    "aws-sdk-v3"
  ],
  "@context":{
    "@vocab":"http://schema.nuget.org/schema#",
    "catalog":"http://schema.nuget.org/catalog#",
    "xsd":"http://www.w3.org/2001/XMLSchema#",
    "dependencies":{
      "@id":"dependency",
      "@container":"@set"
    },
    "dependencyGroups":{
      "@id":"dependencyGroup",
      "@container":"@set"
    },
    "packageEntries":{
      "@id":"packageEntry",
      "@container":"@set"
    },
    "supportedFrameworks":{
      "@id":"supportedFramework",
      "@container":"@set"
    },
    "tags":{
      "@id":"tag",
      "@container":"@set"
    },
    "published":{
      "@type":"xsd:dateTime"
    },
    "created":{
      "@type":"xsd:dateTime"
    },
    "lastEdited":{
      "@type":"xsd:dateTime"
    },
    "catalog:commitTimeStamp":{
      "@type":"xsd:dateTime"
    }
  }
}
             */