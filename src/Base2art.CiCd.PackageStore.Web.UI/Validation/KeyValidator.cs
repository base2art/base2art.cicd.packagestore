namespace Base2art.CiCd.PackageStore.Web.UI.Validation
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using FluentValidation;
    using FluentValidation.Internal;
    using FluentValidation.Validators;
    using Public.Resources;

    public class KeyValidator : Base2art.Web.ObjectQualityManagement.ValidatorBase<Key>
    {
        // private readonly bool update;
        //
        // public KeyValidator(bool update)
        // {
        //     this.update = update;
        // }

        // public Regex HostPattern { get; } = new Regex(@"^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,6}(\.[a-zA-Z]{2,6})?$");

        protected override void Configure()
        {
            // if (this.update)
            // {
            //     this.Backing.RuleFor(x => x.Id).NotEmpty();
            // }

            this.Backing.RuleFor(x => x.Name).NotEmpty();
        }

        public bool IsRequired(Expression<Func<Space, object>> expr)
        {
            // HACK
            Validate(new Key());
            foreach (var variable in this.Backing.OfType<PropertyRule>())
            {
                if (variable.Member == expr.GetMember())
                {
                    // Console.WriteLine(variable.Member);
                    return variable.Validators.Any(x => x is NotEmptyValidator);
                }
            }

            return false;
        }
    }
}